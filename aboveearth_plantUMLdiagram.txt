@startuml
' avoid problems with angled crows feet
skinparam linetype ortho

entity "Agency" as Agency {
    *Agency Table Attributes
    --
    *id = Integer
    *name= String
    *type= String
    *country = String
    *abbrev= String
    *description= String
    *administrator= String
    *founding_year= String
    *total_launch_count= Integer
    *successful_launches= Integer
    *failed_launches= Integer
    *pending_launches= Integer
    *info_url= String
    *wiki_url= String
    *logo_url= String
    *image_url= String
}

entity "Story" as Story {
    *Story Table Attributes
    --
    *id= String
    *title= String
    *url= String
    *image_url= String
    *site= String
    *summary= String
    *publish_date= String
    *site_url= String
    *site_logo_url= String
}


entity "Expedition" as Expedition {
    *Expedition Table Attributes
    --
    *id= String
    *name= String
    *date= String
    *agency_id= Integer
    *rocket_name= String
    *pad_id= Integer
    *image= String
    *author= String
    *tags= String
    *word_count= Integer
    *site_twitter= String
}

entity "StoryExpedition" as StoryExpedition {
    *StoryExpedition Table Attributes
    --
    *id= Integer
    *story_id= String
    *expedition_id= String
}

entity "Pad" as Pad {
    *Pad Table Attributes
    --
    *id= Integer
    *name= String
    *latitude= String
    *longitude= String
    *image= String
}

entity "AstronomyPicture" as AstronomyPicture {
    *AstronomyPicture Table Attributes
    --
    *id= Integer
    *title= String
    *explanation= String
    *month= Integer
    *day= Integer
    *url= String
    *hdurl= String
}

Expedition }|..|| Pad
Expedition }o..|| Agency
Story ||..o{ StoryExpedition
StoryExpedition }o..|| Expedition
@enduml