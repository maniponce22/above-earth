.DEFAULT_GOAL := all
MAKEFLAGS     += --no-builtin-rules
SHELL         := bash


# build and run docker
docker:
	sudo docker build -t aboveearth-backend ./backend
	sudo docker run -p 8080:8080 aboveearth-backend

# start website on localhost:3000/
start:
	cd frontend && \
	export REACT_APP_API_URL=http://localhost:8080/api && \
	npm start

# auto format the code
format:
	black ./backend/*.py

# install dependencies
install:
	pip3 install -r ./backend/requirements.txt

# check files, check their existence with make check
CFILES :=                                 \
    .gitignore                            \
    .gitlab-ci.yml

# check the existence of check files
check: $(CFILES)

# remove temporary files
clean:
	rm -f  *.tmp
	rm -rf __pycache__
