# Above Earth

Link to GitLab pipelines: https://gitlab.com/maniponce22/above-earth/-/pipelines

Link to website: https://www.aboveearth.me/

Link to Postman: https://documenter.getpostman.com/view/14771894/Tz5jfLrW

Link to Presentation: https://www.youtube.com/watch?v=dQL-ctzBIt4

| Name | EID | GitLabID |
| ------ | ------ | ------ |
| Cindy Pham | cp33499 | @phamcindy619 |
| Bruce Luo | byl238 | @Feezy15 |
| Ethan Lao | el26609 | @ethan-lao |
| Manuel Ponce | mip445 | @maniponce22 |
| Joshua Skadberg | jss4626 | @skadbergjosh |

## Phase I

Git SHA: 3556469e6fefe10d012e3db2e54ab16c26d14850

Project leader: Cindy Pham

| Name | Estimated Completion Time | Actual Completion Time |
| ------ | ------ | ------ |
| Cindy Pham | 15 hrs | 35 hrs |
| Bruce Luo | 20 hrs | 20 hrs |
| Ethan Lao | 20 hrs | 30 hrs |
| Manuel Ponce | 25 hrs | 25 hrs |
| Joshua Skadberg | 15 hrs | 15 hrs |

Comments: 

## Phase II

Git SHA: a5446f8123eca5f1846f1d77e3638086b72fe1d1

Project leader: Ethan Lao

| Name | Estimated Completion Time | Actual Completion Time |
| ------ | ------ | ------ |
| Cindy Pham | 40 hrs | 30 hrs |
| Bruce Luo | 35 hrs | 25 hrs |
| Ethan Lao | 35 hrs | 60 hrs |
| Manuel Ponce | 40 hrs | 12 hrs |
| Joshua Skadberg | 40 hrs | 20 hrs |

Comments: 

## Phase III

Git SHA: 8d3d3dd87963c05940815d49ae509e50f3a16427

Project leader: Manuel Ponce

| Name | Estimated Completion Time | Actual Completion Time |
| ------ | ------ | ------ |
| Cindy Pham | 25 hrs | 35 hrs |
| Bruce Luo | 20 hrs | 20 hrs |
| Ethan Lao | 30 hrs | 35 hrs |
| Manuel Ponce | 10 hrs | 11 hrs |
| Joshua Skadberg | 20 hrs | 15 hrs |

Comments: 

## Phase IV

Git SHA: 1d220e390c77a8db8d0c1060c7c273c129677f0d

Project leader: Bruce Luo

| Name | Estimated Completion Time | Actual Completion Time |
| ------ | ------ | ------ |
| Cindy Pham | 20 hrs | 15 hrs |
| Bruce Luo | 10 hrs | 5 hrs |
| Ethan Lao | 20 hrs | 20 hrs |
| Manuel Ponce | 10 hrs | 16 hrs |
| Joshua Skadberg | 15 hrs | 15 hrs |

Comments: 
