echo "Deploying Backend..."
aws ecr get-login-password --region us-east-1 | sudo docker login --username AWS --password-stdin 483327250881.dkr.ecr.us-east-1.amazonaws.com
sudo docker build -t aboveearth-backend .
sudo docker tag aboveearth-backend:latest 483327250881.dkr.ecr.us-east-1.amazonaws.com/aboveearth-backend:latest
sudo docker push 483327250881.dkr.ecr.us-east-1.amazonaws.com/aboveearth-backend:latest
cd aws_deploy
eb deploy
