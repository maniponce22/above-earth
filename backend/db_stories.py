import json
from flask import Flask, request, jsonify
from models import *
from sqlalchemy import *
from sqlalchemy.orm import relationship, sessionmaker
import requests
import json
from os import path
import time
import sys

# connect to db
engine = create_engine(
    "mysql+pymysql://admin:totheM00n@aboveearth-database.cfuv0laclydz.us-east-1.rds.amazonaws.com:3306/aboveearth"
)
factory = sessionmaker(bind=engine)
session = factory()

limit = 100

story_data = {"done": False, "limit": limit, "current_offset": 0, "ids": {}}
filename = "populate_database/stories.json"

if not path.exists("populate_database"):
    os.mkdir("populate_database")

# load from filename
def load_story_data():
    global story_data
    with open(filename) as infile:
        story_data = json.load(infile)

# print to filename
def dump_story_data():
    global story_data
    with open(filename, "w") as outfile:
        json.dump(story_data, outfile)


# setup json file
if not path.exists(filename):
    dump_story_data()
load_story_data()

# check if already done
if story_data["done"]:
    sys.exit("already done")

while True:
    load_story_data()
    current_offset = story_data["current_offset"]

    # send a request
    response = requests.get(
        "https://spaceflightnewsapi.net/api/v2/articles?_limit="
        + str(limit)
        + "&_start="
        + str(current_offset)
    )
    if not response:
        minutes = int(response.json()["detail"].split(" ")[-2]) / 60
        print("Error: no response, try again in " + str(minutes) + " minutes")
        print("Sleeping for " + str((minutes * 50)) + " seconds")
        time.sleep((minutes * 50))
        break

    # check if done
    if len(response.json()) == 0:
        story_data["done"] = True
        dump_story_data()
        break

    current_total_stories = len(response.json())
    current_story = 0
    # loop through results
    for s_info in response.json():
        current_story += 1
        print(
            str(current_story) + "/" + str(current_total_stories), end="\r", flush=True
        )

        s_id = s_info["id"]

        # skip if we've already evaluated it
        if s_id in story_data["ids"] and story_data["ids"][s_id]:
            continue

        story_data["ids"][s_id] = False
        dump_story_data()

        if session.query(Story.id).filter_by(id=s_id).first() is None:
            # add story instance
            new_story = Story(
                id=s_id,
                title=s_info["title"],
                url=s_info["url"],
                image_url=s_info["imageUrl"],
                site=s_info["newsSite"],
                summary=s_info["summary"],
                publish_date=s_info["publishedAt"],
            )

            # try to add story to db
            try:
                session.add(new_story)
                session.commit()
                session.close()
            except:
                session.rollback()
                print("fail")
                continue

        if len(s_info["launches"]) == 0:
            story_data["ids"][s_id] = True
            dump_story_data()
        else:
            # add storyExpedition instance
            success = True
            for l_info in s_info["launches"]:
                if l_info["provider"] != "Launch Library 2":
                    continue

                # skip if launch not in db
                if (
                    session.query(Expedition.id).filter_by(id=l_info["id"]).first()
                    is None
                ):
                    print("Expedition not found")
                    continue

                new_storyexpedition = StoryExpedition(
                    story_id=s_id, expedition_id=l_info["id"]
                )

                # try to add story expedition connection to db
                try:
                    session.add(new_storyexpedition)
                    session.commit()
                    session.close()
                except:
                    session.rollback()
                    success = False

            if success:
                story_data["ids"][s_id] = True
                dump_story_data()

    # make sure success
    success = True
    for s_info in response.json():
        s_id = s_info["id"]
        if not story_data["ids"][s_id]:
            success = False
            break
    if success:
        # update offset
        page = (story_data["current_offset"] // 100) + 1
        story_data["current_offset"] += limit
        story_data["ids"] = {}
        dump_story_data()
        print("Success with page: " + str(page))
    else:
        print("Stopped")
        break
