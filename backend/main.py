import json
from flask import Flask, request, jsonify
from models import *
from marshmallow import Schema, fields
from flask_marshmallow import Marshmallow
import flask_marshmallow as fma
from datetime import date

from flask_cors import CORS

# Create Flask App, config with our AWS database
app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = (
    "mysql+pymysql://admin:totheM00n@aboveearth-database."
    "cfuv0laclydz.us-east-1.rds.amazonaws.com:3306/aboveearth"
)
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.debug = True
CORS(app)
ma = Marshmallow(app)


# const status codes
status_ok = 200
status_notfound = 404
status_badrequest = 400


###########
# SCHEMAS #
###########

# Agency schema (specific agency information for agency instance page)
class AgencySchema(ma.Schema):
    id = fields.Int(required=True)
    name = fields.Str(required=False)
    type = fields.Str(required=False)
    country = fields.Str(required=False)
    abbrev = fields.Str(required=False)
    description = fields.Str(required=False)
    administrator = fields.Str(required=False)
    founding_year = fields.Str(required=False)
    total_launch_count = fields.Int(required=False)
    failed_launches = fields.Int(required=False)
    pending_launches = fields.Int(required=False)
    info_url = fields.Str(required=False)
    wiki_url = fields.Str(required=False)
    logo_url = fields.Str(required=False)
    image_url = fields.Str(required=False)
    most_recent_launch = fields.Str(required=False)
    most_recent_news_story = fields.Str(required=False)
    total_story_count = fields.Int(required=False)


# Agency list (list of agencies for agency model page)
class AgencyListSchema(ma.Schema):
    id = fields.Int(required=True)
    name = fields.Str(required=False)
    abbrev = fields.Str(required=False)
    founding_year = fields.Str(required=False)
    country = fields.Str(required=False)
    type = fields.Str(required=False)
    logo_url = fields.Str(required=False)
    most_recent_launch = fields.Str(required=False)
    most_recent_news_story = fields.Str(required=False)
    total_launch_count = fields.Int(required=False)
    total_story_count = fields.Int(required=False)


# Expedition schema (specific expedition information for instance page)
class ExpeditionSchema(ma.Schema):
    id = fields.Str(required=True)
    name = fields.Str(required=False)
    date = fields.Str(required=False)
    agency_id = fields.Int(required=False)
    rocket_name = fields.Str(required=False)
    image = fields.Str(required=False)
    pad_name = fields.Str(required=False)
    pad_latitude = fields.Str(required=False)
    pad_longitude = fields.Str(required=False)
    pad_image = fields.Str(required=False)
    agency_name = fields.Str(required=False)
    agency_type = fields.Str(required=False)
    agency_country = fields.Str(required=False)
    agency_abbrev = fields.Str(required=False)
    agency_description = fields.Str(required=False)
    agency_administrator = fields.Str(required=False)
    agency_founding_year = fields.Str(required=False)
    agency_logo_url = fields.Str(required=False)
    total_story_count = fields.Int(required=False)
    most_recent_news_story = fields.Str(required=False)


# Agency list (list of expeditions for expedition model page)
class ExpeditionListSchema(ma.Schema):
    id = fields.Str(required=True)
    name = fields.Str(required=False)
    date = fields.Str(required=False)
    agency_id = fields.Int(required=False)
    rocket_name = fields.Str(required=False)
    image = fields.Str(required=False)
    pad_name = fields.Str(required=False)
    agency_name = fields.Str(required=False)
    agency_country = fields.Str(required=False)
    agency_abbrev = fields.Str(required=False)
    total_story_count = fields.Int(required=False)
    most_recent_news_story = fields.Str(required=False)


# News Story schema (specific story information for instance page)
class StorySchema(ma.Schema):
    id = fields.Str(required=True)
    title = fields.Str(required=False)
    url = fields.Str(required=False)
    image_url = fields.Str(required=False)
    site = fields.Str(required=False)
    summary = fields.Str(required=False)
    publish_date = fields.Str(required=False)
    site_url = fields.Str(required=False)
    site_logo_url = fields.Str(required=False)
    author = fields.Str(required=False)
    tags = fields.Str(required=False)
    word_count = fields.Int(required=False)
    site_twitter = fields.Str(required=False)
    related_agency_count = fields.Int(required=False)
    related_expedition_count = fields.Int(required=False)


# News Story list (list of stories for story model page)
class StoryListSchema(ma.Schema):
    id = fields.Str(required=True)
    title = fields.Str(required=False)
    url = fields.Str(required=False)
    image_url = fields.Str(required=False)
    site = fields.Str(required=False)
    summary = fields.Str(required=False)
    publish_date = fields.Str(required=False)
    site_url = fields.Str(required=False)
    site_logo_url = fields.Str(required=False)
    related_agency_count = fields.Int(required=False)
    related_expedition_count = fields.Int(required=False)


# Picture schema (for getting a picture of the day)
class PictureSchema(ma.Schema):
    month = fields.Int(required=False)
    day = fields.Int(required=False)
    title = fields.Str(required=False)
    url = fields.Str(required=False)
    hdurl = fields.Str(required=False)
    explanation = fields.Str(required=False)


# Pad schema (for getting a list of pads)
class PadSchema(ma.Schema):
    name = fields.Str(required=False)
    count = fields.Int(required=False)
    latitude = fields.Str(required=False)
    longitude = fields.Str(required=False)
    image = fields.Str(required=False)


# Schema for getting the count of a query
class CountSchema(ma.Schema):
    count = fields.Int(required=False)


# Initialize Schemas
agency_schema = AgencySchema()
agencies_schema = AgencyListSchema(many=True)
expedition_schema = ExpeditionSchema()
expeditions_schema = ExpeditionListSchema(many=True)
story_schema = StorySchema()
stories_schema = StoryListSchema(many=True)
stories_schema_2 = StorySchema(many=True)
picture_schema = PictureSchema()
pictures_schema = PictureSchema(many=True)
pads_schema = PadSchema(many=True)
count_schema = CountSchema()


#################
# API ENDPOINTS #
#################

# ---------------------
# Query Builder Helpers
# ---------------------

# create a string checks if values are in a set of columns
def build_filter(keywords, columns):
    conditions = [f"({c} LIKE '%{k}%')" for k in keywords for c in columns]
    if len(conditions) > 0:
        return "(" + " or ".join(conditions) + ")"
    else:
        return "(true)"

# create a string that makes sure values are in a specific column
def build_col_filter(values, column):
    return build_filter(values, [column])

# string to calculate the number of hits keywords have in a set of columns
def build_hits(keywords, columns, default):
    cases = [
        f"ROUND((LENGTH({c})-LENGTH(REPLACE({c},'{k}','')))/LENGTH('{k}'))"
        for k in keywords
        for c in columns
    ]
    if len(cases) > 0:
        return "(" + " + ".join(cases) + ")"
    else:
        return default


# NOTE: This route is needed for the default EB health check route
@app.route("/")
def home():
    return "ok"


# ----------------------------
# Picture of the Day Endpoints
# ----------------------------

# Retrive picture of the day
@app.route("/api/apod", methods=["GET"])
def get_apod():
    # get URL args
    month = request.args.get("month", default=-1, type=int)
    day = request.args.get("day", default=-1, type=int)
    random = request.args.get("random", default="false", type=str).lower()

    # make sure parameters are valid
    if month == -1 and day != -1:
        return jsonify({"error": f"month was not provided"}), status_badrequest
    if month != -1 and day == -1:
        return jsonify({"error": f"day was not provided"}), status_badrequest
    if random != "true" and random != "false":
        return jsonify({"error": f"{random} is not a bool"}), status_badrequest

    no_date_provided = month == -1 and day == -1

    # make sure not random if date is provided
    if random == "true" and not no_date_provided:
        return (
            jsonify({"error": f"random cannot be used in conjunction with date"}),
            status_badrequest,
        )

    # default query: return picture for date specified
    query = f"""
        SELECT * FROM astronomypicture
        WHERE month={month} and day={day}
        LIMIT 1"""

    # return a random picture
    if random == "true":
        query = "SELECT * FROM astronomypicture ORDER BY RAND() LIMIT 1"

    # return the picture for today
    elif no_date_provided:
        today = date.today()
        month = int(today.strftime("%m"))
        day = int(today.strftime("%d"))
        query = f"""
            SELECT * FROM astronomypicture
            WHERE month={month} and day={day}
            LIMIT 1"""

    # execute query
    picture = db.session.execute(query).first()
    result = picture_schema.dump(picture)
    return jsonify({"picture": result}), status_ok

# Retrieve list of pictures (from picture of the day)
@app.route("/api/pictures", methods=["GET"])
def get_aps():
    # get URL args
    start_month = request.args.get("start_month", default=-1, type=int)
    start_day = request.args.get("start_day", default=-1, type=int)
    end_month = request.args.get("end_month", default=-1, type=int)
    end_day = request.args.get("end_day", default=-1, type=int)
    limit = request.args.get("limit", default=10, type=int)

    query = f"SELECT * FROM astronomypicture ORDER BY RAND() LIMIT {limit}"

    # check if using a range
    if start_month != -1 or start_day != -1 or end_month != -1 or end_day != -1:
        if start_month == -1 or start_day == -1 or end_month == -1 or end_day == -1:
            return (
                jsonify({"error": "all arguments must be provided"}),
                status_badrequest,
            )

        query = f"""
            SELECT * FROM astronomypicture
            WHERE (month*31 + day)>={start_month*31 + start_day}
            AND (month*31 + day)<={end_month*31 + end_day}
            LIMIT {limit}
        """

    # execute query
    picture = db.session.execute(query)
    result = pictures_schema.dump(picture)
    return jsonify({"pictures": result}), status_ok


# ----------------
# Agency Endpoints
# ----------------

# Retrieve agencies
@app.route("/api/agencies", methods=["GET"])
def get_agencies():
    # get URL args
    keywords = request.args.getlist("keyword", type=str)
    founding_year_start = request.args.get(
        "founding_year_start", default="-1", type=str
    )
    founding_year_end = request.args.get("founding_year_end", default="-1", type=str)
    countries = request.args.getlist("country", type=str)
    types = request.args.getlist("type", type=str)
    sort_by = request.args.get("sort_by", default="name", type=str)
    order_by = request.args.get("order_by", default="desc", type=str)
    limit = request.args.get("limit", default=10, type=str)
    offset = request.args.get("offset", default=0, type=str)

    # make sure sort_by is a valid string
    if sort_by.lower() not in {
        "name",
        "founding_year",
        "country",
        "type",
        "most_recent_launch",
        "most_recent_news_story",
        "total_launch_count",
        "total_story_count",
        "hits",
    }:
        return jsonify({"error": f"invalid sort string"}), status_badrequest

    # make sure order_by is desc or asc
    if order_by.lower() not in {"desc", "asc"}:
        return jsonify({"error": f"invalid order string"}), status_badrequest

    # make sure offset and limit are ints
    try:
        int(limit)
    except ValueError:
        return jsonify({"error": f"{limit} is not an integer"}), status_badrequest
    try:
        int(offset)
    except ValueError:
        return jsonify({"error": f"{offset} is not an integer"}), status_badrequest
    limit = int(limit)
    offset = int(offset)

    default_query = "SELECT * FROM agency_view AS a"

    filter_req = []

    # founding year start filter
    if founding_year_start != "-1":
        filter_req.append("a.founding_year>=" + founding_year_start)

    # founding year end filter
    if founding_year_end != "-1":
        filter_req.append("a.founding_year<=" + founding_year_end)

    # keyword filter
    keyword_cols = [
        "a.name",
        "a.abbrev",
        "a.description",
        "a.country",
        "a.type",
        "a.administrator",
        "a.founding_year",
        "a.total_launch_count",
        "a.pending_launches",
        "a.most_recent_launch",
        "a.most_recent_news_story",
        "a.total_story_count",
    ]
    filter_req.append(build_filter(keywords, keyword_cols))
    if sort_by == "hits":
        sort_by = build_hits(keywords, keyword_cols, "name")

    # country filter
    filter_req.append(build_col_filter(countries, "a.country"))

    # type filter
    filter_req.append(build_col_filter(types, "a.type"))

    # build filter req string
    filter_req_string = " and ".join(filter_req)
    if filter_req_string != "":
        filter_req_string = "WHERE " + filter_req_string

    # order, limit, offset
    additional_req = ["ORDER BY " + sort_by, order_by, "LIMIT 99999"]

    # execute query
    final_query = (
        default_query + " " + filter_req_string + " " + " ".join(additional_req)
    )
    agencies = db.session.execute(final_query)

    # return as json
    result = agencies_schema.dump(agencies)
    return (
        jsonify({"count": len(result), "agencies": result[offset : offset + limit]}),
        status_ok,
    )

# Retrieve single agency
@app.route("/api/agencies/<agency_id>", methods=["GET"])
def get_agency(agency_id):
    assert agency_id == request.view_args["agency_id"]

    # get URL arguments
    related_agency_limit = request.args.get(
        "related_agency_limit", default="10", type=int
    )
    linked_expedition_limit = request.args.get(
        "linked_expedition_limit", default="10", type=int
    )
    linked_story_limit = request.args.get("linked_story_limit", default="10", type=int)

    # make sure agency id is an integer
    try:
        int(agency_id)
    except ValueError:
        return jsonify({"error": f"{agency_id} is not an integer"}), status_badrequest

    # execute agency query and get object
    agency_query = f"SELECT * FROM agency_view as a WHERE a.id={agency_id}"
    agency_response = db.session.execute(agency_query).first()
    agency_object = agency_schema.dump(agency_response)
    try:
        agency_object["failed_launches"] = min(
            agency_object["failed_launches"], agency_object["total_launch_count"]
        )
        agency_object["successful_launches"] = (
            agency_object["total_launch_count"] - agency_object["failed_launches"]
        )
    except:
        pass

    # 
    if len(agency_object) == 0:
        return jsonify({"error": f"{agency_id} not found"}), status_notfound


    # execute realted agency query
    agency_country = agency_object["country"]
    related_agency_query = f"""
        SELECT a.* FROM agency_view AS a
            WHERE a.id!={agency_id}
            ORDER BY total_launch_count, IF(a.country LIKE '%{agency_country}%', 1, 0) DESC
            LIMIT {related_agency_limit}
    """
    related_agency_response = db.session.execute(related_agency_query)
    related_agency_object = agencies_schema.dump(related_agency_response)

    # execute related expedition query and get object
    linked_expedition_query = f"""
        SELECT e.* FROM expedition_view AS e
            WHERE e.agency_id={agency_id}
            ORDER BY e.date DESC
            LIMIT {linked_expedition_limit}
    """
    linked_expedition_response = db.session.execute(linked_expedition_query)
    linked_expedition_object = expeditions_schema.dump(linked_expedition_response)

    # execute related story query and get object
    linked_story_query = f"""
        SELECT s.* FROM story_view AS s
            RIGHT JOIN storyexpedition AS se ON se.story_id=s.id
            LEFT JOIN expedition AS e ON se.expedition_id=e.id
            WHERE s.id IS NOT NULL AND e.agency_id={agency_id}
            GROUP BY s.id
            ORDER BY s.publish_date DESC
            LIMIT {linked_story_limit}
    """
    story_response = db.session.execute(linked_story_query)
    story_object = stories_schema.dump(story_response)

    # execute related pad query
    agency_pad_query = f"""
        SELECT p.*, COUNT(*) AS count
        FROM pad as p
        RIGHT JOIN select_expedition as e ON e.pad_id=p.id
        WHERE e.agency_id={agency_id}
        GROUP BY p.id"""
    pad_response = db.session.execute(agency_pad_query)
    pad_object = pads_schema.dump(pad_response)
    agency_object["pads"] = pad_object

    # return object with status OK
    return_object = {
        "agency": agency_object,
        "related_agencies": related_agency_object,
        "linked_expeditions": linked_expedition_object,
        "linked_stories": story_object,
    }
    return jsonify(return_object), status_ok


# --------------------
# Expedition Endpoints
# --------------------

# Retrieve expeditions
@app.route("/api/expeditions", methods=["GET"])
def get_expeditions():
    # get URL args
    keywords = request.args.getlist("keyword", type=str)
    pad_names = request.args.getlist("pad_name", type=str)
    rocket_names = request.args.getlist("rocket_name", type=str)
    agency_ids = request.args.getlist("agency_id", type=str)
    agency_countries = request.args.getlist("agency_country", type=str)
    epxedition_date_start = request.args.get(
        "expedition_date_start", default="0000-01-01T00:00:00.000Z", type=str
    )
    expedition_date_end = request.args.get(
        "expedition_date_end", default="9999-01-01T00:00:00.000Z", type=str
    )
    sort_by = request.args.get("sort_by", default="date", type=str)
    order_by = request.args.get("order_by", default="desc", type=str)
    limit = request.args.get("limit", default=10, type=str)
    offset = request.args.get("offset", default=0, type=str)

    # make sure sort_by is a valid string
    if sort_by.lower() not in {
        "name",
        "date",
        "rocket_name",
        "pad_name",
        "agency_name",
        "agency_country",
        "total_story_count",
        "most_recent_news_story",
        "hits",
    }:
        return jsonify({"error": f"invalid sort string"}), status_badrequest

    # make sure order_by is desc or asc
    if order_by.lower() not in {"desc", "asc"}:
        return jsonify({"error": f"invalid order string"}), status_badrequest

    # make sure offset and limit are ints
    try:
        int(limit)
    except ValueError:
        return jsonify({"error": f"{limit} is not an integer"}), status_badrequest
    try:
        int(offset)
    except ValueError:
        return jsonify({"error": f"{offset} is not an integer"}), status_badrequest
    limit = int(limit)
    offset = int(offset)

    default_query = "SELECT * FROM expedition_view AS e "

    filter_req = [
        "e.date>='" + str(epxedition_date_start) + "'",
        "e.date<='" + str(expedition_date_end) + "'",
    ]

    # keyword filter
    keyword_cols = [
        "e.name",
        "e.date",
        "e.rocket_name",
        "e.pad_name",
        "e.pad_latitude",
        "e.pad_longitude",
        "e.agency_name",
        "e.agency_type",
        "e.agency_country",
        "e.agency_abbrev",
        "e.total_story_count",
        "e.most_recent_news_story",
    ]
    filter_req.append(build_filter(keywords, keyword_cols))
    if sort_by == "hits":
        sort_by = build_hits(keywords, keyword_cols, "date")

    # pad_name filter
    filter_req.append(build_col_filter(pad_names, "e.pad_name"))

    # rocket_name filter
    filter_req.append(build_col_filter(rocket_names, "e.rocket_name"))

    # agency_id filter
    filter_req.append(build_col_filter(agency_ids, "e.agency_id"))

    # agency_country filter
    filter_req.append(build_col_filter(agency_countries, "e.agency_country"))

    # build filter req string
    filter_req_string = " and ".join(filter_req)
    if filter_req_string != "":
        filter_req_string = "WHERE " + filter_req_string

    # order, limit, offset
    additional_req = ["ORDER BY " + sort_by, order_by, "LIMIT 99999"]

    # execute query
    final_query = (
        default_query + " " + filter_req_string + " " + " ".join(additional_req)
    )
    expeditions = db.session.execute(final_query)

    # return as json
    result = expeditions_schema.dump(expeditions)
    return (
        jsonify({"count": len(result), "expeditions": result[offset : offset + limit]}),
        status_ok,
    )


# Retrieve single expedition
@app.route("/api/expeditions/<expedition_id>", methods=["GET"])
def get_expedition(expedition_id):
    assert expedition_id == request.view_args["expedition_id"]

    # get URL args
    related_expedition_limit = request.args.get(
        "related_expedition_limit", default="10", type=int
    )
    linked_story_limit = request.args.get("linked_story_limit", default="10", type=int)

    # execute agency query and get object
    expedition_query = (
        f"SELECT * FROM expedition_view AS e WHERE e.id='{expedition_id}'"
    )
    expedition_response = db.session.execute(expedition_query).first()
    expedition_object = expedition_schema.dump(expedition_response)

    # check if we found the id
    if len(expedition_object) == 0:
        return jsonify({"error": f"{expedition_id} not found"}), status_notfound

    # execute related expedition query and get object
    agency_id = expedition_object["agency_id"]
    related_expedition_query = f"""
        SELECT
            e.* FROM expedition_view AS e
            WHERE e.agency_id={agency_id} AND e.id!='{expedition_id}'
            ORDER BY most_recent_news_story DESC
            LIMIT {related_expedition_limit}
    """
    related_expedition_response = db.session.execute(related_expedition_query)
    related_expedition_object = expeditions_schema.dump(related_expedition_response)

    # execute related story query and get object
    linked_story_query = f"""
        SELECT s.* FROM story_view AS s
            RIGHT JOIN storyexpedition AS se ON se.story_id=s.id
            WHERE s.id IS NOT NULL AND se.expedition_id='{expedition_id}'
            GROUP BY s.id
            ORDER BY s.publish_date DESC
            LIMIT {linked_story_limit}
    """
    story_response = db.session.execute(linked_story_query)
    story_object = stories_schema.dump(story_response)

    # return result with status OK
    return_object = {
        "expedition": expedition_object,
        "related_expeditions": related_expedition_object,
        "linked_stories": story_object,
    }
    return jsonify(return_object), status_ok


# --------------------
# News Story Endpoints
# --------------------

# Retrieve stories
@app.route("/api/news", methods=["GET"])
def get_stories():
    # get URL args
    keywords = request.args.getlist("keyword", type=str)
    sites = request.args.getlist("site", type=str)
    publish_date_start = request.args.get(
        "publish_date_start", default="0000-01-01T00:00:00.000Z", type=str
    )
    publish_date_end = request.args.get(
        "publish_date_end", default="9999-01-01T00:00:00.000Z", type=str
    )
    related_agencies = request.args.getlist("related_agency", type=str)
    related_expeditions = request.args.getlist("related_expedition", type=str)
    tags = request.args.getlist("tag", type=str)
    authors = request.args.getlist("author", type=str)
    min_word_count = request.args.get("min_word_count", default=0, type=str)
    max_word_count = request.args.get("max_word_count", default=999999, type=str)
    sort_by = request.args.get("sort_by", default="publish_date", type=str)
    order_by = request.args.get("order_by", default="desc", type=str)
    limit = request.args.get("limit", default=10, type=str)
    offset = request.args.get("offset", default=0, type=str)

    # make sure word counts are integers
    try:
        int(min_word_count)
        int(max_word_count)
    except ValueError:
        return (
            jsonify({"error": f"specified word_count is not an integer"}),
            status_badrequest,
        )

    # make sure sort_by is a valid string
    if sort_by.lower() not in {
        "title",
        "site",
        "publish_date",
        "related_agency_count",
        "related_expedition_count",
        "author",
        "word_count",
        "hits",
    }:
        return (
            jsonify({"error": f"{sort_by} is not a valid sort string"}),
            status_badrequest,
        )

    # make sure order_by is desc or asc
    if order_by.lower() not in {"desc", "asc"}:
        return (
            jsonify({"error": f"{order_by} is not a valid order string"}),
            status_badrequest,
        )

    # make sure offset and limit are ints
    try:
        int(limit)
    except ValueError:
        return jsonify({"error": f"{limit} is not an integer"}), status_badrequest
    try:
        int(offset)
    except ValueError:
        return jsonify({"error": f"{offset} is not an integer"}), status_badrequest
    limit = int(limit)
    offset = int(offset)

    #  make sure related agencies are ints
    try:
        [int(a) for a in related_agencies]
    except ValueError:
        return (
            jsonify({"error": f"related agency is not an integer"}),
            status_badrequest,
        )

    default_query = "SELECT * FROM story_view AS s"

    filter_req = [
        f"s.publish_date>='{publish_date_start}'",
        f"s.publish_date<='{publish_date_end}'",
        f"s.word_count>={min_word_count}",
        f"s.word_count<={max_word_count}",
    ]

    # keyword filter
    keyword_cols = [
        "s.title",
        "s.site",
        "s.summary",
        "s.publish_date",
        "s.author",
        "s.tags",
        "s.word_count",
        "s.site_twitter",
        "s.related_agency_count",
        "s.related_expedition_count",
    ]
    filter_req.append(build_filter(keywords, keyword_cols))
    if sort_by == "hits":
        sort_by = build_hits(keywords, keyword_cols, "publish_date")

    # site name filter
    filter_req.append(build_col_filter(sites, "s.site"))

    # tag filter
    filter_req.append(build_col_filter(tags, "s.tags"))

    # author filter
    filter_req.append(build_col_filter(authors, "s.author"))

    # related agency filter
    related_agency_filter = [
        f"({a_id} IN \
			(SELECT \
				a.id \
				FROM select_agency AS a \
				LEFT JOIN select_expedition AS e ON e.agency_id=a.id \
				RIGHT JOIN storyexpedition AS se ON se.expedition_id=e.id \
				WHERE se.story_id=s.id))"
        for a_id in related_agencies
    ]
    if len(related_agency_filter) > 0:
        filter_req.append("(" + " or ".join(related_agency_filter) + ")")

    # related expedition filter
    related_expedition_filter = [
        f"('{e_id}' IN \
			(SELECT \
				e.id \
				FROM select_expedition AS e \
				RIGHT JOIN storyexpedition AS se ON se.expedition_id=e.id \
				WHERE se.story_id=s.id))"
        for e_id in related_expeditions
    ]
    if len(related_expedition_filter) > 0:
        filter_req.append("(" + " or ".join(related_expedition_filter) + ")")

    # build filter req string
    filter_req_string = " and ".join(filter_req)
    if filter_req_string != "":
        filter_req_string = "WHERE " + filter_req_string

    # order, limit, offset
    additional_req = ["ORDER BY " + sort_by, order_by, "LIMIT 999999"]

    # execute query
    final_query = (
        default_query + " " + filter_req_string + " " + " ".join(additional_req)
    )
    stories = db.session.execute(final_query)

    count_query = "SELECT COUNT(*) AS count FROM story_view AS s " + filter_req_string
    story_count = count_schema.dump(db.session.execute(count_query).first())["count"]

    # return as json
    result = stories_schema.dump(stories)
    return (
        jsonify({"count": len(result), "stories": result[offset : offset + limit]}),
        status_ok,
    )


# Retrieve single story
@app.route("/api/news/<news_id>", methods=["GET"])
def get_story(news_id):
    assert news_id == request.view_args["news_id"]

    # get URL args
    related_story_limit = request.args.get(
        "related_story_limit", default="10", type=int
    )

    # execute story query and get object
    story_query = f"SELECT * FROM story_view AS s WHERE s.id='{news_id}'"
    story_response = db.session.execute(story_query).first()
    story_object = story_schema.dump(story_response)

    # check if we found the id
    if len(story_object) == 0:
        return jsonify({"error": f"{news_id} not found"}), status_notfound

    # execute related expedition query and get object
    linked_expedition_query = f"""
        SELECT
            e.* FROM expedition_view AS e
            RIGHT JOIN storyexpedition AS se ON se.expedition_id=e.id
            LEFT JOIN select_story AS s ON s.id=se.story_id
            WHERE s.id IS NOT NULL AND s.id='{news_id}'"""
    linked_expedition_response = db.session.execute(linked_expedition_query)
    linked_expedition_object = expeditions_schema.dump(linked_expedition_response)

    # execute linked agency query and get object
    linked_agency_query = f"""
        SELECT
            a.* FROM agency_view AS a
            RIGHT JOIN expedition AS e ON e.agency_id=a.id
            RIGHT JOIN storyexpedition AS se ON se.expedition_id=e.id
            LEFT JOIN select_story AS s ON s.id=se.story_id
            WHERE s.id IS NOT NULL AND s.id='{news_id}'
            GROUP BY a.id"""
    linked_agency_response = db.session.execute(linked_agency_query)
    linked_agency_object = agencies_schema.dump(linked_agency_response)

    # execute related story query and get object
    this_site = story_object["site"]
    related_story_query = f"""
        SELECT s.* FROM story_view AS s
            RIGHT JOIN storyexpedition AS se ON se.story_id=s.id
            LEFT JOIN expedition AS e ON se.expedition_id=e.id
    """

    # related story filter
    related_story_filter = [f"s.site='{this_site}'"]

    # expedition filter
    linked_expedition_ids = []
    for le in linked_expedition_object:
        linked_expedition_ids.append(le["id"])
    related_story_expedition_filter = [
        f"se.expedition_id='{e_id}'" for e_id in linked_expedition_ids
    ]

    # agency filter
    linked_agency_ids = []
    for la in linked_agency_object:
        linked_agency_ids.append(la["id"])
    related_story_agency_filter = [f"agency_id={a_id}" for a_id in linked_agency_ids]

    filter_string = (
        f"s.id IS NOT NULL AND ("
        + " OR ".join(
            related_story_filter
            + related_story_expedition_filter
            + related_story_agency_filter
        )
        + f") AND s.id!='{story_object['id']}'"
    )

    related_story_query += (
        " WHERE "
        + filter_string
        + f" GROUP BY s.id ORDER BY s.publish_date DESC LIMIT {related_story_limit}"
    )

    related_story_response = db.session.execute(related_story_query)
    related_stories_object = stories_schema.dump(related_story_response)

    # return as json with status OK
    return_object = {
        "story": story_object,
        "linked_expeditions": linked_expedition_object,
        "linked_agencies": linked_agency_object,
        "related_stories": related_stories_object,
    }
    return jsonify(return_object), status_ok


# ------------------
# Overview Endpoints
# ------------------

# Retrieve overview of ALL agencies
@app.route("/api/overview/agencies", methods=["GET"])
def get_overview_agencies():
    query = """
        SELECT
            a.id,
            a.name,
            a.founding_year,
            a.country,
            a.type,
            a.total_launch_count,
            a.total_story_count
            FROM agency_view AS a
    """

    # execute query
    agencies = db.session.execute(query)

    # return as json
    result = agencies_schema.dump(agencies)
    agency_count = len(result)
    return jsonify({"count": agency_count, "agencies": result}), status_ok


# Retrieve overview of ALL expeditions
@app.route("/api/overview/expeditions", methods=["GET"])
def get_overview_expeditions():
    query = """
        SELECT   
            e.id,
            e.name,
            e.date,
            e.rocket_name,
            e.pad_name,
            e.total_story_count
            FROM expedition_view AS e
    """

    # execute query
    expeditions = db.session.execute(query)

    # return as json
    result = expeditions_schema.dump(expeditions)
    expedition_count = len(result)
    return jsonify({"count": expedition_count, "expeditions": result}), status_ok


# Retrieve overview of ALL news
@app.route("/api/overview/news", methods=["GET"])
def get_overview_news():
    query = "SELECT * FROM select_story AS s"

    # execute query
    stories = db.session.execute(query)

    # return as json
    result = stories_schema_2.dump(stories)
    story_count = len(result)
    return jsonify({"count": story_count, "stories": result}), status_ok

# Retrieve overview of ALL pads
@app.route("/api/overview/pads", methods=["GET"])
def get_overview_pads():
    query = """
    SELECT p.*, COUNT(*) AS count
        FROM pad as p
        JOIN select_expedition as e ON e.pad_id=p.id
        GROUP BY p.id;
    """

    # execute query
    pads = db.session.execute(query)

    # return as json
    result = pads_schema.dump(pads)
    pad_count = len(result)
    return jsonify({"count": pad_count, "pads": result}), status_ok

db.init_app(app)

if __name__ == "__main__":
    db.init_app(app)
    app.run(port=8080)
