from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import relationship

db = SQLAlchemy()

# Agency model
class Agency(db.Model):
    __tablename__ = "agency"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    type = db.Column(db.String())
    country = db.Column(db.String())
    abbrev = db.Column(db.String())
    description = db.Column(db.String())
    administrator = db.Column(db.String())
    founding_year = db.Column(db.String())
    total_launch_count = db.Column(db.Integer())
    successful_launches = db.Column(db.Integer())
    failed_launches = db.Column(db.Integer())
    pending_launches = db.Column(db.Integer())
    info_url = db.Column(db.String())
    wiki_url = db.Column(db.String())
    logo_url = db.Column(db.String())
    image_url = db.Column(db.String())

# Expedition model
class Expedition(db.Model):
    __tablename__ = "expedition"
    id = db.Column(db.String, primary_key=True)
    name = db.Column(db.String())
    date = db.Column(db.String())
    agency_id = db.Column(db.Integer(), db.ForeignKey("agency.id"))
    rocket_name = db.Column(db.String())
    pad_id = db.Column(db.Integer(), db.ForeignKey("pad.id"))
    image = db.Column(db.String())
    agency = relationship("Agency")
    pad = relationship("Pad")

# Pad model
class Pad(db.Model):
    __tablename__ = "pad"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    latitude = db.Column(db.String())
    longitude = db.Column(db.Integer())
    image = db.Column(db.String())

# Story model
class Story(db.Model):
    __tablename__ = "story"
    id = db.Column(db.String, primary_key=True)
    title = db.Column(db.String())
    url = db.Column(db.String())
    image_url = db.Column(db.Integer())
    site = db.Column(db.String())
    summary = db.Column(db.Integer())
    publish_date = db.Column(db.String())
    site_url = db.Column(db.String())
    site_logo_url = db.Column(db.String())

# Story-Expedition model
class StoryExpedition(db.Model):
    __tablename__ = "storyexpedition"
    id = db.Column(db.Integer, primary_key=True)
    story_id = db.Column(db.Integer(), db.ForeignKey("story.id"))
    expedition_id = db.Column(db.Integer(), db.ForeignKey("expedition.id"))

    story = relationship("Story")
    expedition = relationship("Expedition")

# Astronomy Picture model
class AstronomyPicture(db.Model):
    __tablename__ = "astronomypicture"
    id = db.Column(db.Integer, primary_key=True)
    month = db.Column(db.Integer())
    day = db.Column(db.Integer())
    explanation = db.Column(db.String())
    hdurl = db.Column(db.String())
    title = db.Column(db.String())
    url = db.Column(db.String())
