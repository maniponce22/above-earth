#!/usr/bin/env python3

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

import unittest
from unittest import main, TestCase
import requests
import sys
import os
from datetime import datetime
import pytz


class APIUnitTests(TestCase):

    # ---------
    # Agencies
    # ---------

    # test standard response (no parameters)
    def test_agencies_standard(self):
        try:
            response = requests.get("https://www.aboveearth.me/api/agencies")
            self.assertEqual(response.status_code, 200)
            self.assertEqual(len(response.json()["agencies"]), 10)
        except requests.exceptions.ConnectionError:
            pass

    # test with limit
    def test_agencies_limit(self):
        try:
            response = requests.get("https://www.aboveearth.me/api/agencies?limit=20")
            self.assertEqual(response.status_code, 200)
            self.assertEqual(len(response.json()["agencies"]), 20)
        except requests.exceptions.ConnectionError:
            pass

    # test with offset
    def test_agencies_offset(self):
        try:
            response_no_offset = requests.get("https://www.aboveearth.me/api/agencies")
            response_offset = requests.get(
                "https://www.aboveearth.me/api/agencies?offset=10"
            )

            self.assertEqual(response_no_offset.status_code, 200)
            self.assertEqual(response_offset.status_code, 200)

            self.assertEqual(len(response_no_offset.json()["agencies"]), 10)
            self.assertEqual(len(response_offset.json()["agencies"]), 10)

            no_offset_ids = set()
            for a in response_no_offset.json()["agencies"]:
                no_offset_ids.add(a["id"])

            for a in response_offset.json()["agencies"]:
                self.assertFalse(a["id"] in no_offset_ids)
        except requests.exceptions.ConnectionError:
            pass

    # test founding year range
    def test_agencies_founding_year(self):
        try:
            start, end = 1970, 1990
            response = requests.get(
                f"https://www.aboveearth.me/api/agencies?founding_year_start={start}&founding_year_end={end}&limit=999"
            )
            self.assertEqual(response.status_code, 200)

            for a in response.json()["agencies"]:
                self.assertTrue(start <= int(a["founding_year"]) <= end)
        except requests.exceptions.ConnectionError:
            pass

    # test country
    def test_agencies_country(self):
        try:
            country = "CHN"
            response = requests.get(
                f"https://www.aboveearth.me/api/agencies?country={country}&limit=999"
            )
            self.assertEqual(response.status_code, 200)

            for a in response.json()["agencies"]:
                self.assertTrue(country in a["country"])
        except requests.exceptions.ConnectionError:
            pass

    # test type
    def test_agencies_type(self):
        try:
            type1, type2 = "Government", "Commercial"
            response = requests.get(
                f"https://www.aboveearth.me/api/agencies?type={type1}&type={type2}&limit=999"
            )
            self.assertEqual(response.status_code, 200)

            for a in response.json()["agencies"]:
                self.assertTrue(type1 in a["type"] or type2 in a["type"])
        except requests.exceptions.ConnectionError:
            pass

    # test sort and order by
    def test_agencies_sort(self):
        try:
            response = requests.get(
                f"https://www.aboveearth.me/api/agencies?sort_by=country&order_by=asc"
            )
            self.assertEqual(response.status_code, 200)

            i = iter(response.json()["agencies"])
            current_country = next(i)["country"]
            for a in i:
                self.assertTrue(a["country"] >= current_country)
                current_country = a["country"]
        except requests.exceptions.ConnectionError:
            pass

    # ID TESTS
    # test get agency
    def test_agency_id(self):
        try:
            response = requests.get(
                f"https://www.aboveearth.me/api/agencies/44?linked_expedition_limit=2&linked_story_limit=2&related_agency_limit=2"
            )
            self.assertEqual(response.status_code, 200)

            a = response.json()["agency"]
            self.assertEqual(a["abbrev"], "NASA")
            self.assertEqual(a["administrator"], "Administrator: Jim Bridenstine")
            self.assertEqual(a["country"], "USA")
            self.assertEqual(a["founding_year"], "1958")
            self.assertEqual(a["id"], 44)
            self.assertEqual(a["name"], "National Aeronautics and Space Administration")
            self.assertEqual(a["type"], "Government")

            self.assertEqual(len(response.json()["linked_expeditions"]), 2)
            for e in response.json()["linked_expeditions"]:
                self.assertEqual(a["id"], 44)

            self.assertEqual(len(response.json()["linked_stories"]), 2)

            self.assertEqual(len(response.json()["related_agencies"]), 2)
            for a in response.json()["related_agencies"]:
                self.assertTrue(response.json()["agency"]["country"] in {n for n in a["country"].split(",")})
        except requests.exceptions.ConnectionError:
            pass

    # test get nonexistent agency
    def test_agency_nonexistent_id(self):
        try:
            response = requests.get(f"https://www.aboveearth.me/api/agencies/-100")
            self.assertEqual(response.status_code, 404)
            self.assertEqual(response.json()["error"], "-100 not found")
        except requests.exceptions.ConnectionError:
            pass

    # test get invalid agency
    def test_agency_invalid_id(self):
        try:
            response = requests.get(f"https://www.aboveearth.me/api/agencies/abc123")
            self.assertEqual(response.status_code, 400)
            self.assertEqual(response.json()["error"], "abc123 is not an integer")
        except requests.exceptions.ConnectionError:
            pass

    # -----------
    # Expeditions
    # -----------

    # test standard response (no parameters)
    def test_expeditions_standard(self):
        try:
            response = requests.get("https://www.aboveearth.me/api/expeditions")
            self.assertEqual(response.status_code, 200)
            self.assertEqual(len(response.json()["expeditions"]), 10)
        except requests.exceptions.ConnectionError:
            pass

    # test with limit
    def test_expeditions_limit(self):
        try:
            response = requests.get(
                "https://www.aboveearth.me/api/expeditions?limit=20"
            )
            self.assertEqual(response.status_code, 200)
            self.assertEqual(len(response.json()["expeditions"]), 20)
        except requests.exceptions.ConnectionError:
            pass

    # test with offset
    def test_expeditions_offset(self):
        try:
            response_no_offset = requests.get(
                "https://www.aboveearth.me/api/expeditions"
            )
            response_offset = requests.get(
                "https://www.aboveearth.me/api/expeditions?offset=10"
            )

            self.assertEqual(response_no_offset.status_code, 200)
            self.assertEqual(response_offset.status_code, 200)

            self.assertEqual(len(response_no_offset.json()["expeditions"]), 10)
            self.assertEqual(len(response_offset.json()["expeditions"]), 10)

            no_offset_ids = set()
            for e in response_no_offset.json()["expeditions"]:
                no_offset_ids.add(e["id"])

            for e in response_offset.json()["expeditions"]:
                self.assertFalse(e["id"] in no_offset_ids)
        except requests.exceptions.ConnectionError:
            pass

    # test pad name
    def test_expeditions_pad(self):
        try:
            pad = "Launch Pad A"
            response = requests.get(
                f"https://www.aboveearth.me/api/expeditions?pad_name={pad}&limit=999"
            )
            self.assertEqual(response.status_code, 200)

            for e in response.json()["expeditions"]:
                self.assertTrue(pad in e["pad_name"])
        except requests.exceptions.ConnectionError:
            pass

    # test rocket name
    def test_expeditions_rocket(self):
        try:
            rocket = "Starship Prototype"
            response = requests.get(
                f"https://www.aboveearth.me/api/expeditions?rocket_name={rocket}&limit=999"
            )
            self.assertEqual(response.status_code, 200)

            for e in response.json()["expeditions"]:
                self.assertTrue(rocket in e["rocket_name"])
        except requests.exceptions.ConnectionError:
            pass

    # test agency ID
    def test_expeditions_agency_id(self):
        try:
            agency_id = 44
            response = requests.get(
                f"https://www.aboveearth.me/api/expeditions?agency_id={agency_id}&limit=999"
            )
            self.assertEqual(response.status_code, 200)

            for e in response.json()["expeditions"]:
                self.assertEqual(agency_id, e["agency_id"])
        except requests.exceptions.ConnectionError:
            pass

    # test agency country
    def test_expeditions_agency_country(self):
        try:
            country1, country2 = "USA", "CHN"
            response = requests.get(
                f"https://www.aboveearth.me/api/expeditions?agency_country={country1}&agency_country={country2}&limit=999"
            )
            self.assertEqual(response.status_code, 200)

            for e in response.json()["expeditions"]:
                self.assertTrue(e["agency_country"] in [country1, country2])
        except requests.exceptions.ConnectionError:
            pass

    # test date range
    def test_expeditions_date(self):
        try:
            start, end = "1950-01-01T00:00:00.000Z", "1970-01-01T00:00:00.000Z"
            response = requests.get(
                f"https://www.aboveearth.me/api/expeditions?expedition_date_start={start}&expedition_date_end={end}&limit=999"
            )
            self.assertEqual(response.status_code, 200)

            for e in response.json()["expeditions"]:
                self.assertTrue(start <= e["date"] <= end)
        except requests.exceptions.ConnectionError:
            pass

    # test order and sort by
    def test_expeditions_sort(self):
        try:
            response = requests.get(
                f"https://www.aboveearth.me/api/expeditions?sort_by=most_recent_news_story&order_by=desc"
            )
            self.assertEqual(response.status_code, 200)

            i = iter(response.json()["expeditions"])
            date = next(i)["most_recent_news_story"]
            for a in i:
                self.assertTrue(a["most_recent_news_story"] <= date)
                date = a["most_recent_news_story"]
        except requests.exceptions.ConnectionError:
            pass

    # ID TESTS
    # test get expedition
    def test_expedition_id(self):
        try:
            response = requests.get(
                f"https://www.aboveearth.me/api/expeditions/c0ac9a61-5aac-40d7-9919-3c29ea7d4172?linked_story_limit=2&related_expedition_limit=2"
            )
            self.assertEqual(response.status_code, 200)

            e = response.json()["expedition"]
            self.assertEqual(e["agency_abbrev"], "SpX")
            self.assertEqual(e["agency_country"], "USA")
            self.assertEqual(e["agency_founding_year"], "2002")
            self.assertEqual(e["agency_id"], 121)
            self.assertEqual(e["agency_name"], "SpaceX")
            self.assertEqual(e["agency_type"], "Commercial")
            self.assertEqual(e["date"], "2021-03-03T23:15:00Z")
            self.assertEqual(e["id"], "c0ac9a61-5aac-40d7-9919-3c29ea7d4172")
            self.assertEqual(e["name"], "Starship SN10 | 10 km Flight")
            self.assertEqual(e["pad_latitude"], "25.997116")
            self.assertEqual(e["pad_longitude"], "-97.15503099856647")
            self.assertEqual(e["pad_name"], "Launch Pad A")
            self.assertEqual(e["rocket_name"], "Starship Prototype")

            self.assertEqual(len(response.json()["linked_stories"]), 2)

            self.assertEqual(len(response.json()["related_expeditions"]), 2)
            for e in response.json()["related_expeditions"]:
                self.assertEqual(
                    e["agency_abbrev"], response.json()["expedition"]["agency_abbrev"]
                )
                self.assertEqual(
                    e["agency_country"], response.json()["expedition"]["agency_country"]
                )
                self.assertEqual(
                    e["agency_id"], response.json()["expedition"]["agency_id"]
                )
                self.assertEqual(
                    e["agency_name"], response.json()["expedition"]["agency_name"]
                )
        except requests.exceptions.ConnectionError:
            pass

    # test get nonexistent agency
    def test_expedition_nonexistent_id(self):
        try:
            response = requests.get(f"https://www.aboveearth.me/api/expeditions/-200")
            self.assertEqual(response.status_code, 404)
            self.assertEqual(response.json()["error"], "-200 not found")
        except requests.exceptions.ConnectionError:
            pass

    # ------------
    # News Stories
    # ------------

    # test standard response (no parameters)
    def test_stories_standard(self):
        try:
            response = requests.get("https://www.aboveearth.me/api/news")
            self.assertEqual(response.status_code, 200)
            self.assertEqual(len(response.json()["stories"]), 10)
        except requests.exceptions.ConnectionError:
            pass

    # test with limit
    def test_stories_limit(self):
        try:
            response = requests.get("https://www.aboveearth.me/api/news?limit=20")
            self.assertEqual(response.status_code, 200)
            self.assertEqual(len(response.json()["stories"]), 20)
        except requests.exceptions.ConnectionError:
            pass

    # test with offset
    def test_stories_offset(self):
        try:
            response_no_offset = requests.get("https://www.aboveearth.me/api/news")
            response_offset = requests.get(
                "https://www.aboveearth.me/api/news?offset=10"
            )

            self.assertEqual(response_no_offset.status_code, 200)
            self.assertEqual(response_offset.status_code, 200)

            self.assertEqual(len(response_no_offset.json()["stories"]), 10)
            self.assertEqual(len(response_offset.json()["stories"]), 10)

            no_offset_ids = set()
            for s in response_no_offset.json()["stories"]:
                no_offset_ids.add(s["id"])

            for s in response_offset.json()["stories"]:
                self.assertFalse(s["id"] in no_offset_ids)
        except requests.exceptions.ConnectionError:
            pass

    # test site filter
    def test_stories_site(self):
        try:
            site = "SpaceNews"
            response = requests.get(
                f"https://www.aboveearth.me/api/news?site={site}&limit=999"
            )
            self.assertEqual(response.status_code, 200)

            for s in response.json()["stories"]:
                self.assertTrue(site in s["site"])
        except requests.exceptions.ConnectionError:
            pass

    # test date range
    def test_stories_date(self):
        try:
            start, end = "1950-01-01T00:00:00.000Z", "1970-01-01T00:00:00.000Z"
            response = requests.get(
                f"https://www.aboveearth.me/api/news?publish_date_start={start}&publish_date_end={end}&limit=999"
            )
            self.assertEqual(response.status_code, 200)

            for s in response.json()["stories"]:
                self.assertTrue(start <= s["publish_date"] <= end)
        except requests.exceptions.ConnectionError:
            pass

    # test related agency filter
    def test_stories_agency(self):
        try:
            agency_id = 121
            response = requests.get(
                f"https://www.aboveearth.me/api/news?related_agency={agency_id}&limit=2"
            )
            self.assertEqual(response.status_code, 200)

            for s in response.json()["stories"]:
                r = requests.get(f"https://www.aboveearth.me/api/news/{s['id']}")
                self.assertEqual(response.status_code, 200)
                ids = {a["id"] for a in r.json()["linked_agencies"]}
                self.assertTrue(agency_id in ids)
        except requests.exceptions.ConnectionError:
            pass

    # test related expedition filter
    def test_stories_expedition(self):
        try:
            expedition_id = "65896761-b6ca-4df3-9699-e077a360c52a"
            response = requests.get(
                f"https://www.aboveearth.me/api/news?related_expedition={expedition_id}&limit=2"
            )
            self.assertEqual(response.status_code, 200)

            for s in response.json()["stories"]:
                r = requests.get(f"https://www.aboveearth.me/api/news/{s['id']}")
                self.assertEqual(response.status_code, 200)
                ids = {e["id"] for e in r.json()["linked_expeditions"]}
                self.assertTrue(expedition_id in ids)
        except requests.exceptions.ConnectionError:
            pass

    # test tag filter
    def test_stories_tag(self):
        try:
            tag = "SpaceX"
            response = requests.get(
                f"https://www.aboveearth.me/api/news?tag={tag}&limit=2"
            )
            self.assertEqual(response.status_code, 200)

            for s in response.json()["stories"]:
                r = requests.get(f"https://www.aboveearth.me/api/news/{s['id']}")
                self.assertEqual(response.status_code, 200)
                tags = {
                    t.lower()
                    for t in r.json()["story"]["tags"].strip().split(", ")
                    if t != ""
                }
                self.assertTrue(tag.lower() in tags)
        except requests.exceptions.ConnectionError:
            pass

    # test author filter
    def test_stories_author(self):
        try:
            author = "Philip Sloss"
            response = requests.get(
                f"https://www.aboveearth.me/api/news?author={author}&limit=2"
            )
            self.assertEqual(response.status_code, 200)

            for s in response.json()["stories"]:
                r = requests.get(f"https://www.aboveearth.me/api/news/{s['id']}")
                self.assertEqual(response.status_code, 200)
                self.assertEqual(r.json()["story"]["author"], author)
        except requests.exceptions.ConnectionError:
            pass

    # test word count range
    def test_stories_words(self):
        try:
            word_min = 600
            word_max = 700
            response = requests.get(
                f"https://www.aboveearth.me/api/news?min_word_count={word_min}&max_word_count={word_max}&&limit=2"
            )
            self.assertEqual(response.status_code, 200)

            for s in response.json()["stories"]:
                r = requests.get(f"https://www.aboveearth.me/api/news/{s['id']}")
                self.assertEqual(response.status_code, 200)
                word_count = r.json()["story"]["word_count"]
                self.assertTrue(word_count >= word_min and word_count <= word_max)
        except requests.exceptions.ConnectionError:
            pass

    # test order and sort by
    def test_stories_sort(self):
        try:
            response = requests.get(
                f"https://www.aboveearth.me/api/news?sort_by=publish_date&order_by=asc"
            )
            self.assertEqual(response.status_code, 200)

            i = iter(response.json()["stories"])
            date = next(i)["publish_date"]
            for s in i:
                self.assertTrue(s["publish_date"] >= date)
                date = s["publish_date"]
        except requests.exceptions.ConnectionError:
            pass

    # ID TESTS
    # test get story
    def test_story_id(self):
        try:
            response = requests.get(
                f"https://www.aboveearth.me/api/news/604ff9752618e3001c010194"
            )
            self.assertEqual(response.status_code, 200)

            s = response.json()["story"]
            self.assertEqual(s["id"], "604ff9752618e3001c010194")
            self.assertEqual(s["publish_date"], "2021-03-16T00:19:01.000Z")
            self.assertEqual(s["site"], "ElonX")
            self.assertEqual(
                s["title"],
                "Starship SN10 landed intact but exploded shortly after, Elon Musk revealed what had gone wrong",
            )
            self.assertEqual(s["author"], "SCR00CHY")
            self.assertEqual(s["site_twitter"], "scr00chy")
            self.assertEqual(s["site_url"], "www.elonx.net")
            self.assertEqual(
                s["tags"], "SpaceX, Starship, Starship SN10, Super Heavy Starship"
            )
            self.assertEqual(s["word_count"], 1233)

            self.assertTrue(len(response.json()["linked_agencies"]) >= 1)
            self.assertTrue(len(response.json()["linked_expeditions"]) >= 1)
            self.assertEqual(len(response.json()["related_stories"]), 10)
        except requests.exceptions.ConnectionError:
            pass

    # test get nonexistent story
    def test_story_nonexistent_id(self):
        try:
            response = requests.get(f"https://www.aboveearth.me/api/news/-300")
            self.assertEqual(response.status_code, 404)
            self.assertEqual(response.json()["error"], "-300 not found")
        except requests.exceptions.ConnectionError:
            pass

    # ----------------------------
    # Astronomy Picture of the Day
    # ----------------------------

    # apod endpoint
    # test standard response (no params)
    def test_apod_standard(self):
        try:
            response = requests.get("https://www.aboveearth.me/api/apod")
            self.assertEqual(response.status_code, 200)
            today = datetime.now(pytz.utc)
            month = int(today.strftime("%m"))
            day = int(today.strftime("%d"))
            self.assertEqual(response.json()["picture"]["month"], month)
            self.assertTrue(abs(response.json()["picture"]["day"] - day) <= 1)
        except requests.exceptions.ConnectionError:
            pass

    # test specific date
    def test_apod_date(self):
        try:
            month, day = 6, 27
            response = requests.get(
                f"https://www.aboveearth.me/api/apod?month={month}&day={day}"
            )
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.json()["picture"]["month"], month)
            self.assertEqual(response.json()["picture"]["day"], day)
        except requests.exceptions.ConnectionError:
            pass

    # test bad input
    def test_apod_bad(self):
        try:
            response = requests.get("https://www.aboveearth.me/api/apod?month=6")
            self.assertEqual(response.status_code, 400)
        except requests.exceptions.ConnectionError:
            pass

    # pictures endpoint
    # test range
    def test_pictures_range(self):
        try:
            start_month, start_day = 3, 1
            end_month, end_day = 3, 20
            response = requests.get(
                f"https://www.aboveearth.me/api/pictures?start_month={start_month}&start_day={start_day}&end_month={end_month}&end_day={end_day}&limit=30"
            )
            self.assertEqual(response.status_code, 200)
            self.assertEqual(len(response.json()["pictures"]), 20)
            for p in response.json()["pictures"]:
                self.assertEqual(p["month"], start_month)
                self.assertTrue(p["day"] >= start_day and p["day"] <= end_day)
        except requests.exceptions.ConnectionError:
            pass

    # test bad input
    def test_pictures_range(self):
        try:
            response = requests.get(
                f"https://www.aboveearth.me/api/pictures?start_month=2"
            )
            self.assertEqual(response.status_code, 400)
        except requests.exceptions.ConnectionError:
            pass

    # --------
    # Overview
    # --------

    # test agency overview
    def test_overview_agencies(self):
        try:
            response = requests.get(f"https://www.aboveearth.me/api/overview/agencies")
            self.assertEqual(response.status_code, 200)
            self.assertTrue(len(response.json()["agencies"]) > 100)
        except requests.exceptions.ConnectionError:
            pass

    # test expedition overview
    def test_overview_expeditions(self):
        try:
            response = requests.get(
                f"https://www.aboveearth.me/api/overview/expeditions"
            )
            self.assertEqual(response.status_code, 200)
            self.assertTrue(len(response.json()["expeditions"]) > 100)
        except requests.exceptions.ConnectionError:
            pass

    # test story overview
    def test_overview_stories(self):
        try:
            response = requests.get(f"https://www.aboveearth.me/api/overview/news")
            self.assertEqual(response.status_code, 200)
            self.assertTrue(len(response.json()["stories"]) > 100)
        except requests.exceptions.ConnectionError:
            pass

    # test pads overview
    def test_overview_pads(self):
        try:
            response = requests.get(f"https://www.aboveearth.me/api/overview/pads")
            self.assertEqual(response.status_code, 200)
            self.assertTrue(len(response.json()["pads"]) > 50)
        except requests.exceptions.ConnectionError:
            pass


if __name__ == "__main__":
    unittest.main(warnings='ignore')
