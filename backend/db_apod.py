import json
from flask import Flask, request, jsonify
from models import *
from sqlalchemy import *
from sqlalchemy.orm import relationship, sessionmaker
import requests
import json
from os import path, mkdir
import time
import sys

# connect to db
engine = create_engine(
    "mysql+pymysql://admin:totheM00n@aboveearth-database.cfuv0laclydz.us-east-1.rds.amazonaws.com:3306/aboveearth"
)
factory = sessionmaker(bind=engine)
session = factory()


def add_all():
    # ask for images in a range
    response = requests.get(
        "https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY&start_date=2016-01-01&end_date=2016-12-31"
    )

    if not response:
        print("No response")
        return

    current_count = 0
    total_count = len(response.json())

    for entry in response.json():
        current_count += 1
        print(str(current_count) + "/" + str(total_count), end="\r", flush=True)
        splitdate = entry["date"].split("-")

        # keep trying if we don't get an image
        test_year = 2019
        while entry["media_type"] != "image":
            print("Not an image, retrying...")
            response = requests.get(
                f"https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY&date={test_year}-{splitdate[1]}-{splitdate[2]}"
            )
            if not response:
                print("No response in retrying")
            entry = response.json()
            test_year -= 1

        # create a picture object from model
        picture = None
        if "hdurl" in entry:
            picture = AstronomyPicture(
                month=int(splitdate[1]),
                day=int(splitdate[2]),
                explanation=entry["explanation"],
                title=entry["title"],
                hdurl=entry["hdurl"],
                url=entry["url"],
            )
        else:
            picture = AstronomyPicture(
                month=int(splitdate[1]),
                day=int(splitdate[2]),
                explanation=entry["explanation"],
                title=entry["title"],
                url=entry["url"],
            )

        # try to add to db
        try:
            session.add(picture)
            session.commit()
            session.close()
        except:
            session.rollback()
            print("Failed to add picture: " + str(entry["date"]))


add_all()
