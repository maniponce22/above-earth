import json
from flask import Flask, request, jsonify
from models import *
from sqlalchemy import *
from sqlalchemy.orm import relationship, sessionmaker
import requests
import json
from os import path
import time
import sys

engine = create_engine(
    "mysql+pymysql://admin:totheM00n@aboveearth-database.cfuv0laclydz.us-east-1.rds.amazonaws.com:3306/aboveearth"
)
factory = sessionmaker(bind=engine)
session = factory()

# NOTE: CHANGE THIS VARIABLE to choose API between ll or lldev
mode = "ll"

limit = 100

expedition_data = {"done": False, "limit": limit, "current_offset": 0, "ids": {}}
filename = "populate_database/expeditions.json"

if not path.exists("populate_database"):
    os.mkdir("populate_database")

# load from filename
def load_expedition_data():
    global expedition_data
    with open(filename) as infile:
        expedition_data = json.load(infile)

# print to filename
def dump_expedition_data():
    global expedition_data
    with open(filename, "w") as outfile:
        json.dump(expedition_data, outfile)

# setup json file
if not path.exists(filename):
    dump_expedition_data()
load_expedition_data()

# check if already done
if expedition_data["done"]:
    sys.exit("already done")

while True:
    load_expedition_data()
    current_offset = expedition_data["current_offset"]

    # send a request
    response = requests.get(
        "https://"
        + mode
        + ".thespacedevs.com/2.2.0/launch/?limit="
        + str(limit)
        + "&offset="
        + str(current_offset)
    )
    if not response:
        minutes = int(response.json()["detail"].split(" ")[-2]) / 60
        print("Error: no response, try again in " + str(minutes) + " minutes")
        print("Sleeping for " + str((minutes * 50)) + " seconds")
        time.sleep((minutes * 50))
        break

    # check if done
    if len(response.json()["results"]) == 0:
        expedition_data["done"] = True
        dump_expedition_data()
        break

    current_total_expeditions = len(response.json()["results"])
    current_expedition = 0
    # loop through results
    for e_info in response.json()["results"]:
        current_expedition += 1
        print(
            str(current_expedition) + "/" + str(current_total_expeditions),
            end="\r",
            flush=True,
        )

        e_id = e_info["id"]

        if e_id in expedition_data["ids"] and expedition_data["ids"][e_id]:
            continue

        expedition_data["ids"][e_id] = False
        dump_expedition_data()

        # add pad instance if doesn't exist
        pad_info = e_info["pad"]
        if session.query(Pad.id).filter_by(id=pad_info["id"]).first() is None:
            new_pad = Pad(
                id=pad_info["id"],
                name=pad_info["name"],
                latitude=pad_info["latitude"],
                longitude=pad_info["longitude"],
                image=pad_info["map_image"],
            )

            # try to add pad to db
            try:
                session.add(new_pad)
                session.commit()
                session.close()
            except:
                session.rollback()
                continue

        # add expedition instance
        if e_info["launch_service_provider"] != None:
            new_expedition = Expedition(
                id=e_id,
                name=e_info["name"],
                date=e_info["net"],
                agency_id=e_info["launch_service_provider"]["id"],
                rocket_name=e_info["rocket"]["configuration"]["name"],
                pad_id=pad_info["id"],
                image=e_info["image"],
            )
        else:
            new_expedition = Expedition(
                id=e_id,
                name=e_info["name"],
                date=e_info["net"],
                rocket_name=e_info["rocket"]["configuration"]["name"],
                pad_id=pad_info["id"],
                image=e_info["image"],
            )

        # try to add expedition to db
        try:
            session.add(new_expedition)
            session.commit()
            session.close()

            expedition_data["ids"][e_id] = True
            dump_expedition_data()
        except:
            session.rollback()
            print("fail")
            continue

    print(str(current_expedition) + "/" + str(current_total_expeditions), flush=True)

    # make sure success
    success = True
    for e_info in response.json()["results"]:
        e_id = e_info["id"]
        if not expedition_data["ids"][e_id]:
            success = False
            break
    if success:
        # update offset
        page = (expedition_data["current_offset"] // 100) + 1
        expedition_data["current_offset"] += limit
        expedition_data["ids"] = {}
        dump_expedition_data()
        print("Success with page: " + str(page))
    else:
        print("Stopped")
        break
