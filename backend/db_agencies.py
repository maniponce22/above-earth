import json
from flask import Flask, request, jsonify
from models import *
from sqlalchemy import *
from sqlalchemy.orm import relationship, sessionmaker
import requests
import json
from os import path, mkdir
import time
import sys

# connect to db
engine = create_engine(
    "mysql+pymysql://admin:totheM00n@aboveearth-database.cfuv0laclydz.us-east-1.rds.amazonaws.com:3306/aboveearth"
)
factory = sessionmaker(bind=engine)
session = factory()

# NOTE: CHANGE THIS VARIABLE to choose API between ll or lldev
mode = "lldev"

agency_ids = {"ids_collected": False, "ids_loaded": False, "ids": {}}
filename = "populate_database/agencies.json"

if not path.exists("populate_database"):
    os.mkdir("populate_database")

# load agency_ids from json
def load_agency_ids():
    global agency_ids
    with open(filename) as infile:
        agency_ids = json.load(infile)


# dump agency_ids to json
def dump_agency_ids():
    global agency_ids
    with open(filename, "w") as outfile:
        json.dump(agency_ids, outfile)


# setup json file if not exists
if not path.exists(filename):
    dump_agency_ids()
load_agency_ids()

if agency_ids["ids_loaded"]:
    sys.exit("ids already loaded")

# get all agency ids, store them in json
if not agency_ids["ids_collected"]:
    response = requests.get(
        "https://" + mode + ".thespacedevs.com/2.2.0/agencies/?limit=999"
    )
    while True:
        if not response:
            print("No response from server")
            break

        load_agency_ids()

        # add ids
        for a in response.json()["results"]:
            agency_ids["ids"][a["id"]] = False

        dump_agency_ids()

        if response.json()["next"] == None:
            agency_ids["ids_collected"] = True
            dump_agency_ids()
            break
        response = requests.get(response.json()["next"])

load_agency_ids()

# populate actual database
while not agency_ids["ids_loaded"]:
    # make sure all ids were collected
    if not agency_ids["ids_collected"]:
        print("Failed: Agency ids were not collected")
        break

    total_agencies = len(agency_ids["ids"])
    current_agency = 0

    # loop through all ids
    for a_id in agency_ids["ids"]:
        current_agency += 1
        print(str(current_agency) + "/" + str(total_agencies), end="\r", flush=True)

        # skip if we've already added it to db
        if agency_ids["ids"][a_id]:
            continue

        # send request
        response = requests.get(
            "https://" + mode + ".thespacedevs.com/2.0.0/agencies/" + str(a_id) + "/"
        )
        if not response:
            minutes = int(response.json()["detail"].split(" ")[-2]) / 60
            print(str(current_agency) + "/" + str(total_agencies), flush=True)
            print("Error: no response, try again in " + str(minutes) + " minutes")
            print("Sleeping for " + str((minutes * 50)) + " seconds")
            time.sleep((minutes * 50))
            break

        info = response.json()

        new_agency = Agency(
            id=a_id,
            name=info["name"],
            type=info["type"],
            country=info["country_code"],
            abbrev=info["abbrev"],
            description=info["description"],
            admistrator=info["administrator"],
            founding_year=info["founding_year"],
            total_launch_count=info["total_launch_count"],
            successful_launches=info["successful_launches"],
            failed_launches=info["failed_launches"],
            pending_launches=info["pending_launches"],
            info_url=info["info_url"],
            wiki_url=info["wiki_url"],
            logo_url=info["logo_url"],
            image_url=info["image_url"],
        )

        # try to add to db
        try:
            session.add(new_agency)
            session.commit()
            session.close()

            # indicate in the json that the id was added
            agency_ids["ids"][a_id] = True
            dump_agency_ids()
        except:
            session.rollback()
            print("Failed to add agency: " + str(a_id))

    # double check if we're done
    done = True
    for a_id in agency_ids["ids"]:
        if agency_ids["ids"][a_id] == False:
            done = False
            break
    if done:
        agency_ids["ids_loaded"] = True
        dump_agency_ids()
    else:
        print("Failed to add all agencies")
