import selenium
import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
import time

# selenium tests
class tests(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(
            "./node_modules/.bin/chromedriver", options=chrome_options
        )

        self.website = "https://www.aboveearth.me"
        self.wait = WebDriverWait(self.driver, 10)

    # test buttons on splash page
    def test_splash(self):
        self.driver.get(f"{self.website}")
        self.driver.implicitly_wait(15)

        # test page title
        self.assertEqual(self.driver.title, "Above Earth")

        # test about button
        self.driver.find_element_by_xpath(
            "/html/body/div/div/div[1]/div/a/button"
        ).click()
        self.driver.implicitly_wait(15)
        self.assertTrue(f"{self.website}/about" in self.driver.current_url)

        # test agency button
        self.driver.get(f"{self.website}")
        self.driver.find_element_by_xpath(
            "/html/body/div/div/div[3]/div/div[3]/div/a/button"
        ).click()
        time.sleep(10)
        self.assertTrue(f"{self.website}/agencies" in self.driver.current_url)

        # test expedition button
        self.driver.get(f"{self.website}")
        self.driver.find_element_by_xpath(
            "/html/body/div/div/div[3]/div/div[1]/div/a/button"
        ).click()
        time.sleep(10)
        self.assertTrue(f"{self.website}/expeditions" in self.driver.current_url)

        #test news button
        self.driver.get(f"{self.website}")
        self.driver.find_element_by_xpath(
            "/html/body/div/div/div[3]/div/div[2]/div/a/button"
        ).click()
        time.sleep(10)
        self.assertTrue(f"{self.website}/news" in self.driver.current_url)

    # test navbar buttons
    def test_navBar(self):
        self.driver.get(f"{self.website}")
        self.driver.implicitly_wait(15)

        # test logo button
        self.driver.find_element_by_xpath(
            "/html/body/div/div/nav/a").click()
        self.assertTrue(f"{self.website}" in self.driver.current_url)

        # test about button
        self.driver.find_element_by_xpath(
            "/html/body/div/div/nav/div/a[1]").click()
        self.assertTrue(f"{self.website}/about" in self.driver.current_url)

        # test agency button
        self.driver.find_element_by_xpath(
            "/html/body/div/div/nav/div/a[2]").click()
        self.assertTrue(f"{self.website}/agencies" in self.driver.current_url)

        # test expedition button
        self.driver.find_element_by_xpath(
            "/html/body/div/div/nav/div/a[3]").click()
        self.assertTrue(f"{self.website}/expeditions" in self.driver.current_url)

        # test news button
        self.driver.find_elements_by_xpath(
            "/html/body/div/div/nav/div/a[4]")[0].click()
        self.assertTrue("https://www.aboveearth.me/news" in self.driver.current_url)

        # test our visualizations button
        self.driver.find_elements_by_xpath(
            "/html/body/div/div/nav/div/a[5]")[0].click()
        self.assertTrue("https://www.aboveearth.me/our_visualizations" in self.driver.current_url)

        # test provider visualizations button
        self.driver.find_elements_by_xpath(
            "/html/body/div/div/nav/div/a[6]")[0].click()
        self.assertTrue("https://www.aboveearth.me/provider_visualizations" in self.driver.current_url)

    # test links on about page
    def test_about(self):
        # go to about us page
        self.driver.get("https://www.aboveearth.me/about")
        self.driver.implicitly_wait(15)

        original_window = self.driver.current_window_handle

        # there aren't other windows open already
        assert len(self.driver.window_handles) == 1

        # test The Space Devs button
        self.driver.find_elements_by_xpath(
            "/html/body/div/div/div/div[3]/div/div/div[1]/div/a")[0].click()
        self.driver.switch_to.window(self.driver.window_handles[1])
        self.assertTrue("https://thespacedevs.com/llapi" in self.driver.current_url)
        self.driver.close()
        self.driver.switch_to.window(original_window)

        # test Spaceflight News button
        self.driver.find_elements_by_xpath(
            "/html/body/div/div/div/div[3]/div/div/div[2]/div/a")[0].click()
        self.driver.switch_to.window(self.driver.window_handles[1])
        self.assertTrue("https://spaceflightnewsapi.net/" in self.driver.current_url)
        self.driver.close()
        self.driver.switch_to.window(original_window)

        # test NASA button
        self.driver.find_elements_by_xpath(
            "/html/body/div/div/div/div[3]/div/div/div[3]/div/a")[0].click()
        self.driver.switch_to.window(self.driver.window_handles[1])
        self.assertTrue("https://api.nasa.gov/" in self.driver.current_url)
        self.driver.close()
        self.driver.switch_to.window(original_window)

        # test Clearbit button
        self.driver.find_elements_by_xpath(
            "/html/body/div/div/div/div[3]/div/div/div[4]/div/a")[0].click()
        self.driver.switch_to.window(self.driver.window_handles[1])
        self.assertTrue("https://clearbit.com/" in self.driver.current_url)
        self.driver.close()
        self.driver.switch_to.window(original_window)

        # test React link
        self.driver.find_elements_by_xpath(
            "/html/body/div/div/div/div[4]/div/div/div[1]/div/a")[0].click()
        self.driver.switch_to.window(self.driver.window_handles[1])
        self.assertTrue("https://reactjs.org/" in self.driver.current_url)
        self.driver.close()
        self.driver.switch_to.window(original_window)

        # test Amazon link
        self.driver.find_elements_by_xpath(
            "/html/body/div/div/div/div[4]/div/div/div[2]/div/a")[0].click()
        self.driver.switch_to.window(self.driver.window_handles[1])
        self.assertTrue("https://aws.amazon.com/" in self.driver.current_url)
        self.driver.close()
        self.driver.switch_to.window(original_window)

        # test Postman link
        self.driver.find_elements_by_xpath(
            "/html/body/div/div/div/div[4]/div/div/div[3]/div/a")[0].click()
        self.driver.switch_to.window(self.driver.window_handles[1])
        self.assertTrue("https://www.postman.com/" in self.driver.current_url)
        self.driver.close()
        self.driver.switch_to.window(original_window)

        # test Pixabay link
        self.driver.find_elements_by_xpath(
            "/html/body/div/div/div/div[4]/div/div/div[4]/div/a")[0].click()
        self.driver.switch_to.window(self.driver.window_handles[1])
        self.assertTrue("https://pixabay.com/" in self.driver.current_url)
        self.driver.close()
        self.driver.switch_to.window(original_window)

        # test Namecheap link
        self.driver.find_elements_by_xpath(
            "/html/body/div/div/div/div[4]/div/div/div[5]/div/a")[0].click()
        self.driver.switch_to.window(self.driver.window_handles[1])
        self.assertTrue("https://www.namecheap.com/" in self.driver.current_url)
        self.driver.close()
        self.driver.switch_to.window(original_window)

        # test Bootstrap link
        self.driver.find_elements_by_xpath(
            "/html/body/div/div/div/div[4]/div/div/div[6]/div/a")[0].click()
        self.driver.switch_to.window(self.driver.window_handles[1])
        self.assertTrue("https://getbootstrap.com/" in self.driver.current_url)
        self.driver.close()
        self.driver.switch_to.window(original_window)

        # test GitLab link
        self.driver.find_elements_by_xpath(
            "/html/body/div/div/div/div[4]/div/div/div[7]/div/a")[0].click()
        self.driver.switch_to.window(self.driver.window_handles[1])
        self.assertTrue("https://about.gitlab.com/" in self.driver.current_url)
        self.driver.close()
        self.driver.switch_to.window(original_window)

        # test MySQL Link
        self.driver.find_elements_by_xpath(
            "/html/body/div/div/div/div[4]/div/div/div[8]/div/a")[0].click()
        self.driver.switch_to.window(self.driver.window_handles[1])
        self.assertTrue("https://www.mysql.com/about/" in self.driver.current_url)
        self.driver.close()
        self.driver.switch_to.window(original_window)

        # test Docker Link
        self.driver.find_elements_by_xpath(
            "/html/body/div/div/div/div[4]/div/div/div[9]/div/a")[0].click()
        self.driver.switch_to.window(self.driver.window_handles[1])
        self.assertTrue("https://www.docker.com/" in self.driver.current_url)
        self.driver.close()
        self.driver.switch_to.window(original_window)

        # test Flask Link
        self.driver.find_elements_by_xpath(
            "/html/body/div/div/div/div[4]/div/div/div[10]/div/a")[0].click()
        self.driver.switch_to.window(self.driver.window_handles[1])
        self.assertTrue("https://flask.palletsprojects.com/en/1.1.x/" in self.driver.current_url)
        self.driver.close()
        self.driver.switch_to.window(original_window)

        # test Selenium Link
        self.driver.find_elements_by_xpath(
            "/html/body/div/div/div/div[4]/div/div/div[11]/div/a")[0].click()
        self.driver.switch_to.window(self.driver.window_handles[1])
        self.assertTrue("https://www.selenium.dev/about/" in self.driver.current_url)
        self.driver.close()
        self.driver.switch_to.window(original_window)

        # test Leaflet Link
        self.driver.find_elements_by_xpath(
            "/html/body/div/div/div/div[4]/div/div/div[12]/div/a")[0].click()
        self.driver.switch_to.window(self.driver.window_handles[1])
        self.assertTrue("https://leafletjs.com/" in self.driver.current_url)
        self.driver.close()
        self.driver.switch_to.window(original_window)

        # test GitLab repo link
        self.driver.find_elements_by_xpath(
            "/html/body/div/div/div/div[2]/div[2]/div[1]/a")[0].click()
        self.driver.switch_to.window(self.driver.window_handles[1])
        self.assertTrue("https://gitlab.com/maniponce22/above-earth" in self.driver.current_url)
        self.driver.close()
        self.driver.switch_to.window(original_window)

        # test Postman API link
        self.driver.find_elements_by_xpath(
            "/html/body/div/div/div/div[2]/div[2]/div[2]/a")[0].click()
        self.driver.switch_to.window(self.driver.window_handles[1])
        self.assertTrue("https://documenter.getpostman.com/view/14771894/Tz5jfLrW" in self.driver.current_url)
        self.driver.close()
        self.driver.switch_to.window(original_window)
        
    # test agency model page
    def test_agency(self):
        self.driver.get(f"{self.website}/agencies")
        self.driver.implicitly_wait(15)
        
        # test clicking on instance
        self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div[1]/table/tbody/tr[1]/td[1]/a").click()
        self.assertTrue(f"{self.website}/agencies/136" in self.driver.current_url)

        # test filters, search, and sort
        self.driver.get(f"{self.website}/agencies")
        self.driver.implicitly_wait(15)
        self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div[1]/form/div/div/button"
        ).click()
        filter_elem = self.wait.until(EC.element_to_be_clickable((By.XPATH,
            '/html/body/div/div/div/div/div[1]/div/div/div[2]/div[3]/p')))
        filter_elem.click()

        search_elem = self.wait.until(EC.element_to_be_clickable((By.XPATH,
            '/html/body/div/div/div/div/div[1]/form/div/div/div/div/input')))
        time.sleep(10)
        search_elem.click()
        self.wait.until(lambda driver: driver.switch_to.active_element == search_elem)
        search_elem.send_keys("nasa")

        self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div[1]/form/div/div/div/div/div/button"
        ).click()
        
        time.sleep(10)
        card_elem = self.wait.until(EC.element_to_be_clickable((By.XPATH,
            '/html/body/div/div/div/div/table/tbody/tr[1]/td[1]/a')))
        card_elem.click()
        time.sleep(10)
        self.assertTrue(
            f"{self.website}/agencies/285"
            in self.driver.current_url)

        # test pagination
        self.driver.get(f"{self.website}/agencies")
        self.driver.implicitly_wait(15)
        self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div[2]/ul/li[6]/a").click()
        pag_elem = self.wait.until(EC.element_to_be_clickable((By.XPATH,
            '/html/body/div/div/div/div/div[2]/ul/li[6]/a')))
        self.assertEqual(pag_elem.text, '6')


    # test expedition model page
    def test_expedition(self):
        self.driver.get(f"{self.website}/expeditions")
        self.driver.implicitly_wait(15)

        # test clicking on card
        self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div[2]/div[1]/div[1]/a").click()
        self.driver.implicitly_wait(15)
        self.assertTrue(
            f"{self.website}/expeditions/59548105-347d-4477-8747-7fc3f91016c5"
            in self.driver.current_url)

        # test filters, search, and sort
        self.driver.get(f"{self.website}/expeditions")
        self.driver.implicitly_wait(15)
        self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div[1]/div/form/div/div[1]/div/button"
        ).click()
        self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div[1]/div/form/div/div[2]/div/div/div"
        ).click()
        self.driver.find_element_by_xpath(
            "/html/body/div[2]/div[3]/ul/li[6]"
        ).click()

        filter_elem = self.wait.until(EC.element_to_be_clickable((By.XPATH,
            '/html/body/div/div/div/div/div[1]/div/div/div/div[2]/div[3]/p')))
        filter_elem.click()

        time.sleep(10)
        search_elem = self.wait.until(EC.element_to_be_clickable((By.XPATH,
            '/html/body/div/div/div/div/div[1]/div/form/div/div[1]/div/div/div/input')))
        search_elem.click()
        self.wait.until(lambda driver: driver.switch_to.active_element == search_elem)
        search_elem.send_keys("united")

        self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div[1]/div/form/div/div[1]/div/div/div/div/button"
        ).click()
        time.sleep(10)
        card_elem = self.wait.until(EC.element_to_be_clickable((By.XPATH,
            '/html/body/div/div/div/div/div[2]/div[1]/div[1]/a')))
        card_elem.click()
        self.driver.implicitly_wait(15)
        self.assertTrue(
            f"{self.website}/expeditions/290436f9-0eb4-4450-9e28-3ac1de40fc0a"
            in self.driver.current_url)

        # test pagination
        self.driver.get(f"{self.website}/expeditions")
        self.driver.implicitly_wait(15)
        self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div[3]/ul/li[6]/a").click()
        pag_elem = self.wait.until(EC.element_to_be_clickable((By.XPATH,
            '/html/body/div/div/div/div/div[3]/ul/li[6]/a')))
        self.assertEqual(pag_elem.text, '6')

    # test news model page
    def test_news(self):
        self.driver.get(f"{self.website}/news")
        self.driver.implicitly_wait(15)

        # test clicking on card
        self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div[2]/div[1]/div[1]/a").click()
        self.assertTrue(
            f"{self.website}/news/606b407b7d9b43001c18b834"
            in self.driver.current_url)

        # test filters, search, and sort
        self.driver.get(f"{self.website}/news")
        self.driver.implicitly_wait(15)
        self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div[1]/div/form/div/div[1]/div/button"
        ).click()
        self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div[1]/div/form/div/div[2]/div/div/div"
        ).click()
        self.driver.find_element_by_xpath(
            "/html/body/div[2]/div[3]/ul/li[2]"
        ).click()

        filter_elem = self.wait.until(EC.element_to_be_clickable((By.XPATH,
            '/html/body/div/div/div/div/div[1]/div/div/div/div[3]/div[3]/p')))
        filter_elem.click()
        
        self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div[1]/div/form/div/div[1]/div/div/div/input"
        ).send_keys("starship")
        self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div[1]/div/form/div/div[1]/div/div/div/div/button"
        ).click()
        time.sleep(10)
        card_elem = self.wait.until(EC.element_to_be_clickable((By.XPATH,
            '/html/body/div/div/div/div/div[2]/div[1]/div[1]/a')))
        card_elem.click()
        time.sleep(10)
        self.assertTrue(
            f"{self.website}/news/5f8fd18e833d4b00116fb2cb"
            in self.driver.current_url)

        # test pagination
        self.driver.get(f"{self.website}/news")
        self.driver.implicitly_wait(15)
        self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div[3]/ul/li[6]/a").click()
        pag_elem = self.wait.until(EC.element_to_be_clickable((By.XPATH,
            '/html/body/div/div/div/div/div[3]/ul/li[6]/a')))
        self.assertEqual(pag_elem.text, '6')

    # test agency instance page
    def test_agency_instance(self):
        self.driver.get(f"{self.website}/agencies")
        self.driver.implicitly_wait(15)
        
        # test expedition link
        self.driver.get(f"{self.website}/agencies/121")
        self.driver.implicitly_wait(15)
        self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div[3]/div/div[1]/table/tbody/tr[1]/td/a").click()
        self.assertTrue(
            f"{self.website}/expeditions/571bf2c1-59eb-4079-b785-0472324d3a2d" in self.driver.current_url)

        # test news link
        self.driver.get(f"{self.website}/agencies/121")
        self.driver.implicitly_wait(15)
        self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div[3]/div/div[2]/table/tbody/tr[1]/td/a").click()
        self.assertTrue(
            f"{self.website}/news/60662e037d9b43001c18b810" in self.driver.current_url)

        # test agency link
        self.driver.get(f"{self.website}/agencies/121")
        self.driver.implicitly_wait(15)
        self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div[3]/div/div[3]/table/tbody/tr[1]/td/a").click()
        self.assertTrue(
            f"{self.website}/agencies/6" in self.driver.current_url)

    # test expedition instance page
    def test_expedition_instance(self):
        # test agency link
        self.driver.get(
            f"{self.website}/expeditions/c0ac9a61-5aac-40d7-9919-3c29ea7d4172")
        self.driver.implicitly_wait(15)
        self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div[2]/div/p/a").click()
        self.assertTrue(
            f"{self.website}/agencies/121"
            in self.driver.current_url)

        # test story link
        self.driver.get(
            f"{self.website}/expeditions/c0ac9a61-5aac-40d7-9919-3c29ea7d4172")
        self.driver.implicitly_wait(15)
        self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div[3]/div[2]/div[3]/div/table/tbody/tr[1]/td/a").click()
        self.assertTrue(
            f"{self.website}/news/6052610f996303001c50b08b"
            in self.driver.current_url)

    # test news instance page
    def test_news_instance(self):
        original_window = self.driver.current_window_handle

        # there aren't other windows open already
        assert len(self.driver.window_handles) == 1

        # test expedition link
        self.driver.get(
            f"{self.website}/news/60662e037d9b43001c18b810")
        self.driver.implicitly_wait(15)
        self.driver.find_elements_by_xpath(
            "/html/body/div/div/div/div/div[3]/div[1]/div[2]/div/div[2]/div/div[2]/div/div[1]/a")[0].click()
        self.assertTrue(
            f"{self.website}/expeditions/3898f855-5794-48b5-9cb8-6741cb53363b"
            in self.driver.current_url)

        # test agency link
        self.driver.get(
            f"{self.website}/news/60662e037d9b43001c18b810")
        self.driver.implicitly_wait(15)
        self.driver.find_elements_by_xpath(
            "/html/body/div/div/div/div/div[3]/div[1]/div[2]/div/div[3]/div/div[2]/div/a[1]")[0].click()
        self.assertTrue(
            f"{self.website}/agencies/63"
            in self.driver.current_url)

        # test article link
        self.driver.get(
            f"{self.website}/news/60662e037d9b43001c18b810")
        self.driver.implicitly_wait(15)
        self.driver.find_elements_by_xpath(
            "/html/body/div/div/div/div/div[3]/div[1]/div[2]/button")[0].click()
        self.driver.switch_to.window(self.driver.window_handles[1])
        self.assertTrue(
            "https://spaceflightnow.com/2021/04/01/busy-month-of-crew-rotations-on-tap-at-international-space-station/"
            in self.driver.current_url)
        self.driver.close()
        self.driver.switch_to.window(original_window)

        # test site link
        self.driver.find_elements_by_xpath(
            "/html/body/div/div/div/div/div[2]/p/span[1]")[0].click()
        self.driver.switch_to.window(self.driver.window_handles[1])
        self.assertTrue(
            "https://spaceflightnow.com/"
            in self.driver.current_url)
        self.driver.close()
        self.driver.switch_to.window(original_window)

        # test article link 2
        self.driver.find_elements_by_xpath(
            "/html/body/div/div/div/div/div[1]/h2")[0].click()
        self.driver.switch_to.window(self.driver.window_handles[1])
        self.assertTrue(
            "https://spaceflightnow.com/2021/04/01/busy-month-of-crew-rotations-on-tap-at-international-space-station/"
            in self.driver.current_url)
        self.driver.close()
        self.driver.switch_to.window(original_window)

        # test twitter link
        self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div[3]/div[1]/div[2]/div/div[1]/div/div[2]/div/p[1]/small[3]").click()
        self.driver.switch_to.window(self.driver.window_handles[1])
        self.assertTrue(
            "https://twitter.com/@SpaceflightNow"
            in self.driver.current_url or 
            "https://twitter.com/SpaceflightNow"
            in self.driver.current_url)
        self.driver.close()
        self.driver.switch_to.window(original_window)
            
    # test site wide search page
    def test_site_search(self):
        # test agency link
        self.driver.get(f"{self.website}/search/spacex")
        self.driver.implicitly_wait(15)
        self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div[2]/div[1]/div/div/a").click()
        self.assertTrue(
            f"{self.website}/agencies/121"
            in self.driver.current_url)

        # test expedition link
        self.driver.get(f"{self.website}/search/spacex")
        self.driver.implicitly_wait(15)
        self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div[2]/div[2]/div[1]/div[1]/a").click()
        self.assertTrue(
            f"{self.website}/expeditions/d5d607b7-05ed-4142-8703-14b553c195e0"
            in self.driver.current_url)

        # test story link
        self.driver.get(f"{self.website}/search/spacex")
        self.driver.implicitly_wait(15)
        self.driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div[2]/div[3]/div[1]/div[1]/a").click()
        self.assertTrue(
            f"{self.website}/news/5f44a1f5cd8d01772ac435f4"
            in self.driver.current_url)


    def tearDown(self):
        self.driver.quit()

if __name__ == "__main__":
    unittest.main(warnings='ignore')
