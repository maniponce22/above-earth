import React from 'react';
import { mount, shallow, configure } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';

import About from '../components/about/AboutComponent';
import InfoCard from '../components/about/InfoCardComponent';

window.scrollTo = jest.fn();
process.on('unhandledRejection', (reason) => {
	console.log('REJECTION', reason)
})

configure({
    adapter: new Adapter()
})

describe("tests for About page", () => {
    it("renders without crashing", () => {
        const wrapper = shallow(<About />);
        expect(wrapper).toBeDefined();
    });

    it("renders one instance", () => {
        const wrapper = shallow(<About />);
        expect(wrapper).toHaveLength(1);
    });

    it("has all members", () => {
        const wrapper = shallow(<About />);
        expect(wrapper.find(InfoCard)).toHaveLength(5);
    });
});