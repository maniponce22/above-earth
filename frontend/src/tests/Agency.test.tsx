import React from 'react';
import { mount, shallow, configure } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import { Container } from 'react-bootstrap';
import { MemoryRouter } from 'react-router';
import Main from '../components/MainComponent';

import AgencyInstance from '../components/agencies/AgencyInstanceComponent';

window.scrollTo = jest.fn();
process.on('unhandledRejection', (reason) => {
	console.log('REJECTION', reason)
})

configure({
    adapter: new Adapter()
})

const AgencyRoute = props => (
    <MemoryRouter initialEntries={props.initialEntries}>
        <Main {...props} />
    </MemoryRouter>
);

describe("tests for Agency instance page", () => {
    it("renders without crashing", () => {
        const wrapper = mount(<AgencyRoute initialEntries={['/agencies/121']} />);
        expect(wrapper).toBeDefined();
    });

    it("renders one instance", () => {
        const wrapper = mount(<AgencyRoute initialEntries={['/agencies/121']} />);
        expect(wrapper.find(AgencyInstance)).toHaveLength(1);
    });

    it("contains a Container", () => {
        const wrapper = mount(<AgencyRoute initialEntries={['/agencies/121']} />);
        expect(wrapper.find(Container)).toBeTruthy();
    });
});