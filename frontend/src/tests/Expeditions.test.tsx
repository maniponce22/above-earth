import React from 'react';
import { mount, shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {Container, Row, Col} from 'react-bootstrap';

import ExpeditionSearch from '../components/expeditions/ExpeditionSearchComponent';

window.scrollTo = jest.fn();
process.on('unhandledRejection', (reason) => {
	console.log('REJECTION', reason)
})

configure({
    adapter: new Adapter()
})

describe("tests for expeditions model page", () => {
    it("renders without crashing", () => {
        const wrapper = shallow(<ExpeditionSearch />);
        expect(wrapper).toBeDefined();
    });

    it("renders one instance", () => {
        const wrapper = shallow(<ExpeditionSearch />);
        expect(wrapper).toHaveLength(1);
    });

    it("contains a Container", () => {
        const wrapper = shallow(<ExpeditionSearch />);
        expect(wrapper.find(Container)).toBeTruthy();
    });
});