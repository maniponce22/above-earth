import React from 'react';
import { mount, shallow, configure } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import {Container} from 'react-bootstrap'
import { MemoryRouter } from 'react-router';
import { Route } from 'react-router-dom';
import Main from '../components/MainComponent'
import {Row} from 'react-bootstrap'

import StoryInstance from '../components/stories/StoryInstanceComponent';

window.scrollTo = jest.fn();
process.on('unhandledRejection', (reason) => {
	console.log('REJECTION', reason)
})

configure({
    adapter: new Adapter()
})

const StoryRoute = props => (
    <MemoryRouter initialEntries={props.initialEntries}>
        <Main {...props} />
    </MemoryRouter>
  );

describe("tests for News Story instance page", () => {
    it("renders without crashing", () => {
        const wrapper = mount(
            <StoryRoute initialEntries={['/news/604ff9752618e3001c010194']}/>
        );
        expect(wrapper).toBeDefined();
    });

    it("renders one instance", () => {
        const wrapper = mount(<StoryRoute initialEntries={['/news/604ff9752618e3001c010194']}/>);
        expect(wrapper.find(StoryInstance)).toHaveLength(1);
    });

    it("contains a Container", () => {
        const wrapper = mount(<StoryRoute initialEntries={['/news/604ff9752618e3001c010194']}/>);
        expect(wrapper.find(Container)).toBeTruthy();
    });
});