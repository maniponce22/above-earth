import React from 'react';
import { mount, shallow, configure } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import {Container} from 'react-bootstrap'
import { MemoryRouter } from 'react-router';
import { Route } from 'react-router-dom';
import Main from '../components/MainComponent'
import {Row} from 'react-bootstrap'

import ExpeditionInstance from '../components/expeditions/ExpeditionInstanceComponent';

window.scrollTo = jest.fn();
process.on('unhandledRejection', (reason) => {
	console.log('REJECTION', reason)
})

configure({
    adapter: new Adapter()
})

const ExpeditionRoute = props => (
    <MemoryRouter initialEntries={props.initialEntries}>
        <Main {...props} />
    </MemoryRouter>
    );

describe("tests for expedition instance page", () => {
    it("renders without crashing", () => {
        const wrapper = mount(
            <ExpeditionRoute initialEntries={['/expeditions/c0ac9a61-5aac-40d7-9919-3c29ea7d4172']}/>
        );
        expect(wrapper).toBeDefined();
    });

    it("renders one instance", () => {
        const wrapper = mount(<ExpeditionRoute initialEntries={['/expeditions/c0ac9a61-5aac-40d7-9919-3c29ea7d4172']}/>);
        expect(wrapper.find(ExpeditionInstance)).toHaveLength(1);
    });

    it("contains a Container", () => {
        const wrapper = mount(<ExpeditionRoute initialEntries={['/expeditions/c0ac9a61-5aac-40d7-9919-3c29ea7d4172']}/>);
        expect(wrapper.find(Container)).toBeTruthy();
    });
});