import React from 'react';
import { mount, shallow, configure } from 'enzyme';
// import Adapter from 'enzyme-adapter-react-16';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import toJson from 'enzyme-to-json';

import PaginationComponent from '../components/Pagination';

window.scrollTo = jest.fn();
process.on('unhandledRejection', (reason) => {
	console.log('REJECTION', reason)
})

configure({
    adapter: new Adapter()
})

// Author: Bruce
describe('Tests for Pagination Component', () => {
    test('renders correctly with >7 pages, starting window', () => {
        const totalInstances = 100;
        const items = 10;
        const current = 1;
        const pageChange = page => ({page});
        const wrapper = mount(<PaginationComponent
            total = {totalInstances}
            itemsPerPage = {items}
            currentPage = {current}
            onPageChange = {pageChange}
            />);
        expect(toJson(wrapper)).toMatchSnapshot();
    });

    test('renders correctly with <=7 pages', () => {
        const totalInstances = 10;
        const items = 10;
        const current = 1;
        const pageChange = page => ({page});
        const wrapper = mount(<PaginationComponent
            total = {totalInstances}
            itemsPerPage = {items}
            currentPage = {current}
            onPageChange = {pageChange}
            />);
        expect(toJson(wrapper)).toMatchSnapshot(); 
    });
});


