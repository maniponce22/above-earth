import React from 'react';
import { mount, shallow, configure } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';

import Header from '../components/HeaderComponent';
import Navbar from 'react-bootstrap/Navbar';
import { LinkContainer } from 'react-router-bootstrap';

configure({
    adapter: new Adapter()
})

describe("tests for header component", () => {
    it("renders without crashing", () => {
        const wrapper = shallow(<Header />);
        expect(wrapper).toBeDefined();
    });

    it("renders one instance", () => {
        const wrapper = shallow(<Header />);
        expect(wrapper).toHaveLength(1);
    });

    it("has Navbar with 5 links", () => {
        const wrapper = shallow(<Header />);
        expect(wrapper.find(Navbar)).toHaveLength(1);
        expect(wrapper.find(LinkContainer)).toHaveLength(7);
    });
});