import React from 'react';
import { mount, shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {Container, Row, Col} from 'react-bootstrap';

import StoriesSearch from '../components/stories/StoriesSearchComponent';

window.scrollTo = jest.fn();
process.on('unhandledRejection', (reason) => {
	console.log('REJECTION', reason)
})

configure({
    adapter: new Adapter()
})

describe("tests for News Story model page", () => {
    it("renders without crashing", () => {
        const wrapper = shallow(<StoriesSearch />);
        expect(wrapper).toBeDefined();
    });

    it("renders one instance", () => {
        const wrapper = shallow(<StoriesSearch />);
        expect(wrapper).toHaveLength(1);
    });

    it("contains a Container", () => {
        const wrapper = shallow(<StoriesSearch />);
        expect(wrapper.find(Container)).toBeTruthy();
    });
});