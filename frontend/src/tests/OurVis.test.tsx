import React from 'react';
import { mount, shallow, configure } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';

import OurVisualizations from '../components/graphs/ourVis/OurVisComponent';
import { Container } from 'react-bootstrap'
import Tab from '@material-ui/core/Tab';

window.scrollTo = jest.fn();
process.on('unhandledRejection', (reason) => {
	console.log('REJECTION', reason)
})

configure({
    adapter: new Adapter()
})

describe("tests for About page", () => {
    it("renders without crashing", () => {
        const wrapper = shallow(<OurVisualizations />);
        expect(wrapper).toBeDefined();
    });

    it("renders one instance", () => {
        const wrapper = shallow(<OurVisualizations />);
        expect(wrapper).toHaveLength(1);
    });

    it("contains a Container", () => {
        const wrapper = shallow(<OurVisualizations />);
        expect(wrapper.find(Container)).toBeTruthy();
    });

    it("has all tabs", () => {
        const wrapper = shallow(<OurVisualizations />);
        expect(wrapper.find(Tab)).toHaveLength(3);
    });
});