import React from 'react';
import { mount, shallow, configure } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';

import SplashScreen from '../components/splash/SplashScreen';
import {Card} from 'react-bootstrap'

window.scrollTo = jest.fn();
process.on('unhandledRejection', (reason) => {
	console.log('REJECTION', reason)
})

configure({
    adapter: new Adapter()
})

describe("tests for splash screen page", () => {
    it("renders without crashing", () => {
        const wrapper = shallow(<SplashScreen />);
        expect(wrapper).toBeDefined();
    });

    it("renders one instance", () => {
        const wrapper = shallow(<SplashScreen />);
        expect(wrapper).toHaveLength(1);
    });

    it("has 3 cards for each instance", () => {
        const wrapper = shallow(<SplashScreen />);
        expect(wrapper.find(Card)).toHaveLength(3);
    });
});