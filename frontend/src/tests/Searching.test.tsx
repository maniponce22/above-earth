import React from 'react';
import { mount, shallow, configure } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import {Container} from 'react-bootstrap'
import { MemoryRouter } from 'react-router';
import { Route } from 'react-router-dom';
import Main from '../components/MainComponent'
import {Row} from 'react-bootstrap'

import SearchComponent from '../components/Searching';

window.scrollTo = jest.fn();
process.on('unhandledRejection', (reason) => {
	console.log('REJECTION', reason)
})

configure({
    adapter: new Adapter()
})

const SearchRoute = props => (
    <MemoryRouter initialEntries={props.initialEntries}>
        <Main {...props} />
    </MemoryRouter>
  );

describe("tests for Site wide search", () => {
    it("renders without crashing", async () => {
        const wrapper = mount(
            <SearchRoute initialEntries={['/search/test']}/>
        );
        expect(wrapper).toBeDefined();
    });

    it("renders one instance", () => {
        const wrapper = mount(<SearchRoute initialEntries={['/search/test']}/>);
        expect(wrapper.find(SearchComponent)).toHaveLength(1);
    });

    it("contains a Container", () => {
        const wrapper = mount(<SearchRoute initialEntries={['/search/test']}/>);
        expect(wrapper.find(Container)).toBeTruthy();
    });
});