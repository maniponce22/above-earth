import React from 'react';
import { mount, shallow, configure } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';

import ProviderVisualizations from '../components/graphs/providerVis/ProviderVisComponent';
import { Container } from 'react-bootstrap'
import Tab from '@material-ui/core/Tab';

window.scrollTo = jest.fn();
process.on('unhandledRejection', (reason) => {
	console.log('REJECTION', reason)
})

configure({
    adapter: new Adapter()
})

describe("tests for About page", () => {
    it("renders without crashing", () => {
        const wrapper = shallow(<ProviderVisualizations />);
        expect(wrapper).toBeDefined();
    });

    it("renders one instance", () => {
        const wrapper = shallow(<ProviderVisualizations />);
        expect(wrapper).toHaveLength(1);
    });

    it("contains a Container", () => {
        const wrapper = shallow(<ProviderVisualizations />);
        expect(wrapper.find(Container)).toBeTruthy();
    });

    it("has all tabs", () => {
        const wrapper = shallow(<ProviderVisualizations />);
        expect(wrapper.find(Tab)).toHaveLength(3);
    });
});