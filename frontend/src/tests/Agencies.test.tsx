import React from 'react';
import { mount, shallow, configure } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';

import AgenciesList from '../components/agencies/AgenciesSearchComponent';

window.scrollTo = jest.fn();
process.on('unhandledRejection', (reason) => {
	console.log('REJECTION', reason)
})

configure({
    adapter: new Adapter()
})

describe("tests for Agency model page", () => {
    it("renders without crashing", () => {
        const wrapper = shallow(<AgenciesList />);
        expect(wrapper).toBeDefined();
    });

    it("renders one instance", () => {
        const wrapper = shallow(<AgenciesList />);
        expect(wrapper).toHaveLength(1);
    });

    it("contains a table", () => {
        const wrapper = shallow(<AgenciesList />);
        expect(wrapper.find('thead')).toBeTruthy();
    });
});