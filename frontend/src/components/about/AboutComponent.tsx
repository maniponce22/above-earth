import React, { Component, Fragment } from 'react';
import axios from 'axios';

// info card for displaying a member
import InfoCard from '../about/InfoCardComponent';
import Utils from '../utils/Utils'

// react-bootstrap imports
import { CardDeck, CardColumns, Card, Jumbotron, Button, Container, Row, Col } from 'react-bootstrap';

interface Member {
    key:              number,
    name:             string,
    gitNames:         string[],
    gitID:            string,
    photoUrl:         string,
    bio:              string,
    responsibilities: string,
    commits:          number,
    issues:           number,
    tests:            number,
    linkedin:         string,
}

type AboutState = {
    status:         number,
    numDone:        number,
    totalCommits:   number,
    totalIssues:    number,
    totalUnitTests: number,
    members:        Member[]
}

const video_ratio = 560 / 315;

const apis = [
    {
        title:       "The Space Devs",
        description: "API for rocket launches, space events and crewed spaceflights",
        url:         "https://thespacedevs.com/llapi"
    },
    {
        title:       "Spaceflight News",
        description: "API for spaceflight related news",
        url:         "https://spaceflightnewsapi.net/"
    },
    {
        title:       "NASA",
        description: "API for astronomy pictures of the day on splash page",
        url:         "https://api.nasa.gov/"
    },
    {
        title:       "Clearbit",
        description: "API for news publisher logos",
        url:         "https://clearbit.com/"
    }
]

const tools = [
    {
        title:       "React.js",
        description: "Frontend web development framework",
        image:       "images/Logos/reactjs.png",
        url:         "https://reactjs.org/"
    },
    {
        title:       "Amazon Web Services",
        description: "Website hosting service",
        image:       "images/Logos/aws.png",
        url:         "https://aws.amazon.com/"
    },
    {
        title:       "Postman",
        description: "API development and documentation",
        image:       "images/Logos/postman.jpg",
        url:         "https://www.postman.com/"
    },
    {
        title:       "Pixabay",
        description: "Stunning free images and royalty free stock",
        image:       "images/Logos/pixabay.png",
        url:         "https://pixabay.com/"
    },
    {
        title:       "NameCheap",
        description: "Domain name registration and web hosting services",
        image:       "images/Logos/namecheap.jpg",
        url:         "https://www.namecheap.com/"
    },
    {
        title:       "Bootstrap",
        description: "Frontend web development CSS framework",
        image:       "images/Logos/bootstrap.png",
        url:         "https://getbootstrap.com/"
    },
    {
        title:       "GitLab",
        description: "Source code manager with DevOps tools",
        image:       "images/Logos/gitlab.jpg",
        url:         "https://about.gitlab.com/"
    },
    {
        title:       "MySQL",
        description: "Relational database management system",
        image:       "images/Logos/mysql.jpg",
        url:         "https://www.mysql.com/about/"
    },
    {
        title:       "Docker",
        description: "Container for frontend and backend development",
        image:       "images/Logos/docker.jpg",
        url:         "https://www.docker.com/"
    },
    {
        title:       "Flask",
        description: "Backend web development framework in Python",
        image:       "images/Logos/flask.png",
        url:         "https://flask.palletsprojects.com/en/1.1.x/"
    },
    {
        title:       "Selenium",
        description: "GUI testing automation",
        image:       "images/Logos/selenium.png",
        url:         "https://www.selenium.dev/about/"
    },
    {
        title:       "Leaflet",
        description: "Javascript library for interactive maps",
        image:       "images/Logos/leaflet.png",
        url:         "https://leafletjs.com/"
    }
]

class About extends Component<any, AboutState> {
    state: AboutState = {
        status: Utils.starting,
        numDone: 0,
        totalCommits: 0,
        totalIssues: 0,
        totalUnitTests: 0,
        members: [
            {
                key: 0,
                name: "Cindy Pham",
                gitNames: [
                    "Cindy Pham"
                ],
                gitID: "phamcindy619",
                photoUrl: "images/CindyPham.jpg",
                bio: "I was born and raised in Austin, TX. I'm currently a senior CS major and will be graduating in May 2021. I have experience in full-stack web, Android, and game development.",
                responsibilities: "Phase 1 Project Leader. Responsible for the About page and Agency model pages.",
                commits: 0,
                issues: 0,
                tests: 29,
                linkedin: "https://www.linkedin.com/in/phamcindy619"
            },
            {
                key: 1,
                name: "Bruce Luo",
                gitNames: [
                    "Bruce Luo"
                ],
                gitID: "Feezy15",
                photoUrl: "images/Bruce.jpg",
                bio: "Hi! I'm a third year CS and Math major at UT Austin. I'm from Katy, Texas and I enjoy fishing, gaming, and hiking in my free time.",
                responsibilities: "Phase 4 Project Leader. Responsible for the Agency model pages.",
                commits: 0,
                issues: 0,
                tests: 2,
                linkedin: "https://www.linkedin.com/in/b-luo"
            },
            {
                key: 2,
                name: "Ethan Lao",
                gitNames: [
                    "Ethan Lao",
                    "ethan-lao"
                ],
                gitID: "ethan-lao",
                photoUrl: "images/EthanLao.jpg",
                bio: "Hello! I am a sophomore CS major at UT. I'm very interested in full-stack web development, and I also enjoy spending time outdoors and watching any musical.",
                responsibilities: "Phase 2 Project Leader. Responsible for the backend and the News Story pages.",
                commits: 0,
                issues: 0,
                tests: 61,
                linkedin: "https://www.linkedin.com/in/ethan-lao"
            },
            {
                key: 3,
                name: "Manuel Ponce",
                gitNames: [
                    "Manuel Ponce",
                    "Manuel"
                ],
                gitID: "maniponce22",
                photoUrl: "images/Manuel.png",
                bio: "I'm a senior CS major graduating May 2021. My favorite thing to program are mobile apps on Swift and Kotlin. Besides that, I love music, soccer, and skateboarding.",
                responsibilities: "Phase 3 Project Leader. Responsible for the Splash page.",
                commits: 0,
                issues: 0,
                tests: 4,
                linkedin: "https://www.linkedin.com/in/manuelponcecs"
            },
            {
                key: 4,
                name: "Joshua Skadberg",
                gitNames: [
                    "Joshua Skadberg",
                    "Josh"
                ],
                gitID: "skadbergjosh",
                photoUrl: "images/JoshSummers.jpg",
                bio: "Hi! I'm a junior computer science major. I am interested in A.I. and machine learning, and also would like to learn more about cyber security. For my hobbies, I like to exercise, boulder, play games, and watch YouTube in my spare time.",
                responsibilities: "Responsible for the Expedition model pages.",
                commits: 0,
                issues: 0,
                tests: 6,
                linkedin: "https://www.linkedin.com/in/joshua-skadberg-b93451184"
            },
            
        ]
    }

    getCommits = () : void => {
        let currPage = "1";
        this.getPage(currPage);
        // this.setState({numDone: this.state.numDone + 1})
    }

    getPage = (page: string) : void => {
        axios.get('https://gitlab.com/api/v4/projects/24759359/repository/commits?page=' + page)
            .then(res => {
                var numOfCommits = res.data.length;
                if (typeof res.data !== "undefined") {
                    var stateCopy = Object.assign({}, this.state);
                    for (let i = 0; i < numOfCommits; i++) {
                        var commit = res.data[i];
                        for (let j = 0; j < this.state.members.length; j++) {
                            var member = this.state.members[j];
                            for (let v = 0; v < member.gitNames.length; v++) {
                                var name = member.gitNames[v];
                                if (commit["author_name"] === name) {
                                    stateCopy.members[j].commits++;
                                    stateCopy.totalCommits++;
                                }
                            }
                        }
                    }
                    this.setState(stateCopy);

                    if (res.headers["x-next-page"] !== "") {
                        this.getPage(res.headers["x-next-page"]);
                    } else {
                        this.setState({numDone: this.state.numDone + 1})
                    }
                }
            }).catch(e => {
                this.setState({numDone: this.state.numDone + 1})
            })
    }

    getIssues = () : void => {
        // Get each member's issues
        for (let i = 0; i < this.state.members.length; i++) {
            var member = this.state.members[i];
            axios.get('https://gitlab.com/api/v4/projects/24759359/issues_statistics?assignee_username=' + member.gitID)
                .then(res => {
                    var numOfIssues = res.data.statistics.counts.all;
                    if (typeof res.data !== "undefined") {
                        var stateCopy = Object.assign({}, this.state);
                        stateCopy.members[i].issues += numOfIssues;
                        stateCopy.totalIssues += numOfIssues;
                        this.setState(stateCopy);
                    }
                })
        }
        this.setState({numDone: this.state.numDone + 1})
    }

    getTests = () : void => {
        // Count all unit tests
        var numOfTests = 0;
        for (let i = 0; i < this.state.members.length; i++) {
            numOfTests += this.state.members[i].tests;
        }
        this.setState({
            totalUnitTests: numOfTests,
        });
    }

    componentDidMount() {
        window.scrollTo(0,0);
        this.getCommits();
        this.getIssues();
        this.getTests();
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.numDone !== this.state.numDone && this.state.numDone === 2)
            this.setState({status: Utils.success})
    }

    apiCard = (a, i) => (
        <Card key={i} className="shadow rounded-0">
            <Card.Body>
                <Card.Title>{a.title}</Card.Title>
                <Card.Text style={{minHeight: 75}}>{a.description}</Card.Text>
                <Button variant="outline-dark" className="rounded-0" href={a.url} target="_blank" block>Link</Button>
            </Card.Body>
        </Card>
    )

    toolCard = (t, i) => (
        <div key={i}>
            <Card style={{ height: '24rem' }} className="rounded-0 shadow mt-2">
                <a href={t.url} target="_blank">
                    <Card.Img variant="top" src={t.image} alt={t.title} height="250" style={{objectFit: 'contain'}}/>
                </a>
                <Card.Body>
                    <a href={t.url} target="_blank" style={{color: "black"}}>
                        <Card.Title>{t.title}</Card.Title>
                    </a>
                    <Card.Text>{t.description}</Card.Text>
                </Card.Body>
            </Card>
        </div>
    )

    number(num) {
        if (this.state.status !== Utils.success)
            return Utils.renderMediumSpinner();
        return num;
    }

    render() {
        return (
            <Fragment>
                <div>
                    <Jumbotron>
                        <h1>About Us</h1>
                        <p>
                            Above Earth is a website that informs users on upcoming and recently past flight and space exploration expeditions.
                            It provides a collection of expeditions, space agencies, and related news.
                        </p>
                    </Jumbotron>
                    <Container>
                        <h2>Meet the Developers</h2> 
                        <CardColumns >{this.state.members.map(member => 
                            <InfoCard
                                key={member.key}
                                name={member.name}
                                photoUrl={member.photoUrl}
                                bio={member.bio}
                                responsibilities={member.responsibilities}
                                commits={member.commits}
                                issues={member.issues}
                                tests={member.tests}
                                linkedin={member.linkedin}
                                status={this.state.status}
                            />)}
                            <Card style={{height: "44rem"}} className="rounded-0 mt-2 shadow">
                                <Card.Img variant="top" src="spaceNav.png" alt="Above Earth" height="350" style={{padding: 80}}/>
                                <Card.Body>
                                    <Card.Title className="text-center">
                                        Total Commits: {this.number(this.state.totalCommits)}
                                    </Card.Title>
                                    <Card.Title className="text-center">
                                        Total Issues: {this.number(this.state.totalIssues)}
                                    </Card.Title>
                                    <Card.Title className="text-center">
                                        Total Unit Tests: {this.state.totalUnitTests}
                                    </Card.Title>
                                </Card.Body>
                            </Card>
                        </CardColumns>

                        <Row className="justify-content-md-center mt-5">
                            <Col sm="12" md="auto">
                                <Button variant="outline-dark" className="rounded-0 shadow" size="lg" href="https://gitlab.com/maniponce22/above-earth" target="_blank">Visit the GitLab Repo</Button>
                            </Col>
                            <Col sm="12" md="auto">
                                <Button variant="outline-dark" className="rounded-0 shadow" size="lg" href="https://documenter.getpostman.com/view/14771894/Tz5jfLrW" target="_blank">Visit Postman API</Button>
                            </Col>
                        </Row>
                    </Container>
                    <Container className="mt-5">
                        <Row>
                            <h2>Data:</h2>
                            <CardDeck className="d-flex flex-wrap">
                                {apis.map((a, i) => this.apiCard(a, i))}
                            </CardDeck>
                        </Row>
                    </Container>
                    <Container className="mt-5">
                        <Row>
                            <h2>Tools:</h2>
                            <CardColumns className="ml-0 mr-0">
                                {tools.map((t, i) => this.toolCard(t, i))}
                            </CardColumns>
                        </Row>
                    </Container>
                    <Container className="mt-5 mb-5">
                        <Row>
                            <h2 className="mx-auto text-center">Presentation</h2>
                        </Row>
                        <Row>
                            <div
                            className="mx-auto" 
                            style={{display: "block"}}
                            >
                                <iframe className="mx-auto"
                                width={800}
                                height={800 / video_ratio}
                                src={`https://www.youtube.com/embed/dQL-ctzBIt4`}
                                frameBorder="0"
                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                allowFullScreen
                                title="Embedded youtube"
                                />
                            </div>
                        </Row>
                    </Container>
                </div>
            </Fragment>
        );
    }
}

export default About;
