import React, { Component } from 'react';

// react-bootstrap imports
import { Card, Row, Col, Spinner } from 'react-bootstrap';
import Utils from '../utils/Utils'

type CardProps = {
    name:             string,
    photoUrl:         string,
    bio:              string,
    responsibilities: string,
    commits:          number,
    issues:           number,
    tests:            number,
    linkedin:         string,
    status:           number,
}

class InfoCard extends Component<CardProps> {
    openLinkedIn() {
        window.open(this.props.linkedin)
    }

    number(num) {
        if (this.props.status !== Utils.success)
            return Utils.renderSmallSpinner();
        return num;
    }

    render() {
        return (
            <Card style={{height: "44rem"}} className="rounded-0 shadow mt-2">
                <Card.Img variant="top" className="rounded-0" src={this.props.photoUrl}  alt={this.props.name} height="350" style={{objectFit: 'cover', cursor: "pointer"}} onClick={this.openLinkedIn.bind(this)}/>
                <Card.Body>
                    <a href={this.props.linkedin} target="_blank" style={{color: "black"}}>
                        <Card.Title>{this.props.name}</Card.Title>
                    </a>
                    <Card.Text>{this.props.bio}</Card.Text>
                    <Card.Text>{this.props.responsibilities}</Card.Text>
                </Card.Body>
                <Row className="mx-auto mb-4" style={{position: "absolute", bottom: 0, width: "100%"}}>
                    <Col>
                        <Card.Text className="text-center">
                            Commits:
                                <br />
                            {this.number(this.props.commits)}
                        </Card.Text>
                    </Col>
                    <Col>
                        <Card.Text className="text-center">
                            Issues:
                                <br />
                            {this.number(this.props.issues)}
                        </Card.Text>
                    </Col>
                    <Col>
                        <Card.Text className="text-center">
                            Unit Tests:
                                <br />
                            {this.props.tests}
                        </Card.Text>
                    </Col>
                </Row>
            </Card>
        );
    }
}

export default InfoCard;