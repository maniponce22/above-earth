import React, { Component, Fragment } from 'react';
import axios from 'axios';
import {RouteComponentProps } from 'react-router-dom'

// import util library
import Utils from './utils/Utils'

// material-ui imports
import Divider from '@material-ui/core/Divider'

// react-bootstrap imports
import { Container, Row, Col, Card, Table, Dropdown, ListGroup} from 'react-bootstrap';

const fix_country = function(country) {
    return country.replace(/,/g, ", ").replace(/  /g, " ")
}

const colsPerRow = 4;

type AgencySearch = {
    abbrev: string,
    country: string,
    founding_year: number,
    id: number,
    logo_url: string,
    most_recent_launch: string,
    most_recent_news_story: string,
    name: string,
    total_launch_count: number,
    total_story_count: number,
    type: string
}

type StorySearch = {
    id: string,
    image_url: string,
    publish_date: string,
    related_agency_count: number,
    related_expedition_count: number,
    site: string,
    site_logo_url: string,
    site_url: string,
    summary: string,
    title: string,
    url: string
}

type ExpeditionSearch = {
    agency_abbrev: string,
    agency_country: string,
    agency_id: number,
    agency_name: string,
    date: string,
    id: string,
    image: string,
    most_recent_news_story: string,
    name: string,
    pad_name: string,
    rocket_name: string,
    total_story_count: number
}

const allModels = 0;
const agencyModels = 1;
const storyModels = 2;
const expeditionModels = 3;

type SearchState = {
    status: number,
    updated: number,
    agencies: AgencySearch[],
    stories: StorySearch[],
    expeditions: ExpeditionSearch[],
    keywords: string[],
    loading: boolean,
    value: string;
    filterText: string;
    filterChoice: number;
}

class SearchComponent extends Component<RouteComponentProps<{query}>, SearchState> {
    state: SearchState = {
        status: Utils.starting,
        updated: 0,
        agencies: [],
        stories: [],
        expeditions: [],
        keywords: [],
        loading: false,
        value: '',
        filterText: "Filter By Model",
        filterChoice: allModels,
    };

    constructor(props: RouteComponentProps<{query}>) {
        super(props);
        var query = props.match.params['query'];

        var keywords: string[] = []
        for (var k of query.split(" ")) {
            if (k !== null && k !== undefined && k !== '')
                keywords.push(k)
        }

        this.state.keywords = keywords;
        this.handleQueryAgency();
        this.handleQueryExpedition();
        this.handleQueryNews();

    }

    handleQueryAgency = async () => {
        var keyword_filter = this.state.keywords.map(k => {return String("keyword=").concat(k)}).join("&");

        try {
            await axios(
                `${process.env.REACT_APP_API_URL}/agencies?${keyword_filter}&sort_by=hits&limit=50`
            ).then(response => {
                var data = response.data;
                this.setState({ agencies: data.agencies, updated: this.state.updated + 1});
            })
            .catch(error => {});         
        } catch (error) {}
    };

    handleQueryExpedition = async () => {
        var keyword_filter = this.state.keywords.map(k => {return String("keyword=").concat(k)}).join("&");
        
        try {
            await axios(
                `${process.env.REACT_APP_API_URL}/expeditions?${keyword_filter}&sort_by=hits&limit=50`
            ).then(response => {
                var data = response.data;
                this.setState({ expeditions: data.expeditions, updated: this.state.updated + 1});
            }).catch(error => {});
        } catch (error) {}
    };

    handleQueryNews = async () => {
        var keyword_filter = this.state.keywords.map(k => {return String("keyword=").concat(k)}).join("&");

        try {
            await axios(
                `${process.env.REACT_APP_API_URL}/news?${keyword_filter}&sort_by=hits&limit=50`
            ).then(response => {
                var data = response.data;
                this.setState({stories: data.stories, updated: this.state.updated + 1});
            }).catch(error => {});
        } catch (error) {}
    };

    componentDidUpdate(prevProps, prevState) {
        if (prevState.updated !== this.state.updated && this.state.updated === 3)
            this.setState({status: Utils.success})
    }
    
    renderAgencies() {
        if (this.state.agencies.length === 0)
            return (
                <div className="mb-5">
                    <h2>Agencies</h2>
                    <h5 className="font-italic">No search results were found</h5>
                </div>
            )

        return (
            <div className="mb-5">
                <h2>Agencies</h2>
                {Utils.groupByN(colsPerRow, this.state.agencies).map(row => 
                    <Row className="mb-4">
                        {row.map(agency => {
                            if (agency === null) {
                                return <Col sm={12 / colsPerRow}></Col>
                            }

                            var keywords = this.state.keywords

                            return (
                                <Col sm={12 / colsPerRow}>
                                    <a href={String("/agencies/").concat(agency.id)} style={{textDecoration: "none"}}>
                                        <Card className="text-dark shadow rounded-0 darken">
                                            <Card.Img variant="top" className="rounded-0 border-bottom" style={{objectFit: 'contain', width: '100%', height: '180px'}} src={agency.logo_url}/>
                                            <Card.Body className="mb-0">
                                                <Card.Subtitle className="text-muted mb-2 text-limit-one">
                                                    Country: {Utils.highlightText(fix_country(agency.country), keywords)} <br></br>
                                                </Card.Subtitle>
                                                <Card.Title className="text-limit-one mb-0">
                                                    {Utils.highlightText(agency.name, keywords)}
                                                    <span className="text-muted" style={{fontSize: "10px"}}>&emsp;({Utils.highlightText(agency.abbrev, keywords)})</span>
                                                </Card.Title>
                                            </Card.Body>
                                            <ListGroup className="" variant="flush">
                                                    <ListGroup.Item className="darken-child  py-2" >
                                                        Launches: {Utils.highlightText(agency.total_launch_count, keywords)} <br/>
                                                        Most Recent Launch: {Utils.highlightText(Utils.dateToString(agency.most_recent_launch), keywords)}
                                                    </ListGroup.Item>
                                                    <ListGroup.Item className="darken-child  py-2" >
                                                        Stories: {Utils.highlightText(agency.total_story_count, keywords)} <br/>
                                                        Recent: {Utils.highlightText(Utils.dateToString(agency.most_recent_news_story), keywords)}
                                                    </ListGroup.Item>
                                                </ListGroup>
                                            <Card.Footer>
                                                <small className="text-muted">
                                                    Founded in {Utils.highlightText(agency.founding_year, keywords)}
                                                </small>
                                            </Card.Footer>
                                        </Card>
                                    </a>
                                </Col>
                        )})}
                    </Row>
                )}
            </div>
        )
    }

    renderExpeditions() {
        if (this.state.expeditions.length === 0)
            return (
                <div className="mb-5">
                    <h2>Expeditions</h2>
                    <h5 className="font-italic">No search results were found</h5>
                </div>
            )

        return (
            <div className="mb-5">
                <h2>Expeditions</h2>
                {Utils.groupByN(colsPerRow, this.state.expeditions).map(row => 
                    <Row className="mb-4">
                        {row.map(expedition => {
                            if (expedition === null) {
                                return <Col sm={12 / colsPerRow}></Col>
                            }

                            var keywords = this.state.keywords

                            return (
                                <Col sm={12 / colsPerRow}>
                                    <a href={String("/expeditions/").concat(expedition.id)} style={{textDecoration: "none"}}>
                                        <Card className="text-dark shadow rounded-0 darken">
                                            <Card.Img variant="top" className="rounded-0 border-bottom" style={{objectFit: 'cover', width: '100%', height: '180px'}} src={expedition.image}/>
                                            <Card.Body className="mb-0">
                                                <Card.Subtitle className="text-muted text-limit-one mb-2">
                                                    Agency Name: {Utils.highlightText(expedition.agency_name, keywords)}
                                                    <span className="text-muted" style={{fontSize: "10px"}}>&emsp;({Utils.highlightText(expedition.agency_abbrev, keywords)})</span>
                                                    &emsp;|&emsp;{Utils.highlightText(expedition.agency_country, keywords)}
                                                </Card.Subtitle>
                                                <Card.Title className="text-limit-one mb-0">
                                                    {Utils.highlightText(expedition.name, keywords)}
                                                </Card.Title>
                                            </Card.Body>
                                            <ListGroup className="" variant="flush">
                                                <ListGroup.Item className="darken-child  py-2" >
                                                    Pad Name: {Utils.highlightText(expedition.pad_name, keywords)} <br></br>
                                                    Rocket Name: {Utils.highlightText(expedition.rocket_name, keywords)}
                                                </ListGroup.Item>
                                                <ListGroup.Item className="darken-child  py-2" >
                                                    News Stories: {Utils.highlightText(expedition.total_story_count, keywords)} <br></br>
                                                    Most Recent News Story: {Utils.highlightText(Utils.dateToString(expedition.most_recent_news_story), keywords)}
                                                </ListGroup.Item>
                                            </ListGroup>
                                            <Card.Footer>
                                                <small className="text-muted">
                                                    Launched on {Utils.highlightText(Utils.dateToString(expedition.date), keywords)}
                                                </small>
                                            </Card.Footer>
                                        </Card>
                                    </a>
                                </Col>
                        )})}
                    </Row>
                )}
            </div>
        )
    }

    renderStories() {
        if (this.state.stories.length === 0)
            return (
                <div className="mb-5">
                    <h2>News Stories</h2>
                    <h5 className="font-italic">No search results were found</h5>
                </div>
            )

        return (
            <div className="mb-5">
                <h2>News Stories</h2>
                {Utils.groupByN(colsPerRow, this.state.stories).map(row => 
                    <Row className="mb-4">
                        {row.map(story => {
                            if (story === null) {
                                return <Col sm={12 / colsPerRow}></Col>
                            }

                            var keywords = this.state.keywords

                            return (
                                <Col sm={12 / colsPerRow}>
                                    <a href={String("/news/").concat(story.id)} style={{textDecoration: "none"}}>
                                        <Card className="text-dark shadow rounded-0 darken">
                                            <Card.Img variant="top" className="rounded-0 border-bottom" style={{objectFit: 'cover', width: '100%', height: '180px'}} src={story.image_url}/>
                                            <Card.Body className="mb-0" style={{height: "10em"}}>
                                                <Card.Subtitle className="text-muted text-limit-one mb-2">
                                                    Publisher: {Utils.highlightText(story.site, keywords)}
                                                </Card.Subtitle>
                                                <Card.Title className="text-limit-two mb-2">
                                                    {Utils.highlightText(story.title, keywords)}
                                                </Card.Title>
                                                <Card.Text className="text-limit-two mb-0 text-muted">
                                                    {Utils.highlightText(story.summary, keywords)}
                                                </Card.Text>
                                            </Card.Body>
                                            <Table className="table m-0 text-center fill-width text-muted">
                                                <tbody className="fill-width m-0 p-0">
                                                    <tr className="border-bottom-0">
                                                        <td className="border-left-0">
                                                            Expeditions<br/>Mentioned: {Utils.highlightText(story.related_expedition_count, keywords)}
                                                        </td>
                                                        <td className="border-right-0">
                                                            Agencies<br/>Mentioned: {Utils.highlightText(story.related_agency_count, keywords)}
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </Table>
                                            <Card.Footer>
                                                <small className="text-muted">
                                                    Published on {Utils.highlightText(Utils.dateToString(story.publish_date), keywords)}
                                                </small>
                                            </Card.Footer>
                                        </Card>
                                    </a>
                                </Col>
                        )})}
                    </Row>
                )}
            </div>
        )
    }



    get renderResults() {
        if (this.state.filterChoice === agencyModels) {
            return (
                <div>
                    {this.renderAgencies()}
                </div>
            );
        } else if (this.state.filterChoice === expeditionModels) {
            return (
                <div>
                    {this.renderExpeditions()}
                </div>
            );
        } else if (this.state.filterChoice === storyModels) {
            return (
                <div>
                    {this.renderStories()}
                </div>
            );
        } else {
            return (
                <div>
                    {this.renderAgencies()}
                    <Divider className="mb-4"/>
                    {this.renderExpeditions()}
                    <Divider className="mb-4"/>
                    {this.renderStories()}
                </div>
            );
        }
    }

    render() {
        return (
        <div>
            <Container className="mt-5" style={{paddingRight: "5%", paddingLeft: "5%", minHeight: 900}} fluid>
                <h1 className="text-center">Search Results</h1>
                <h5 className="text-center font-italic">{this.props.match.params['query']}</h5>
                <Dropdown>
                    <Dropdown.Toggle variant="dark" className="rounded-0 shadow mx-auto mb-5" style={{display: "block", fontSize: "18px"}}>
                        {this.state.filterText}
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                        <Dropdown.Item onClick={() => this.setState({filterText: "Filter By Model", filterChoice: allModels})}>All</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setState({filterText: "Filter By Agencies", filterChoice: agencyModels})}>Agencies</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setState({filterText: "Filter By Expeditions", filterChoice: expeditionModels})}>Expeditions</Dropdown.Item>
                        <Dropdown.Item onClick={() => this.setState({filterText: "Filter By News", filterChoice: storyModels})}>News</Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>
                {this.state.status === Utils.success ? this.renderResults : Utils.renderSpinner()}
            </Container>
            
        </div>
        );
    }
}
  

export default SearchComponent;