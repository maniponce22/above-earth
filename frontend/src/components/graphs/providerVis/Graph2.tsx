import React, { Component, Fragment } from 'react';
import axios from 'axios';
import * as d3 from "d3";

import Utils from '../../utils/Utils'

interface USState {
    name: string;
    num_representatives: number;
}

// TODO: update state
type GraphState = {
    status: number;
    states: USState[];
}

const margin = {
    top: 40,
    bottom: 40,
    left: 80,
    right: 40
};
const diameter = 1000;

class Graph2 extends Component<any, GraphState> {
    state: GraphState = {
        status: Utils.loading,
        states: []
    };

    async getData() {
        try {
            await axios.get(
                `https://ratemypolitician.me/api/politicians`,
            ).then(response => {
                var data = response.data;

                if ('error' in data)
                    this.setState({status: Utils.error})
                else {
                    for (let i = 0; i < data.length; i++) {
                        var stateCopy = Object.assign({}, this.state);
                        var index = -1;
                        for (let j = 0; j < stateCopy.states.length; j++) {
                            if (stateCopy.states[j].name === data[i].state) {
                                index = j;
                                break;
                            }
                        }
                        if (index > -1) {
                            stateCopy.states[index].num_representatives++;
                        }
                        else {
                            stateCopy.states.push({
                                name: data[i].state,
                                num_representatives: 1
                            });
                        }
                        this.setState(stateCopy);
                    }
                    this.setState({
                        status:  Utils.success
                    })
                }
            }).catch(err => {
                this.setState({status: Utils.error});
            });            
        } catch (error) {
            this.setState({status: Utils.error});
        }
    }
    
    componentDidMount() {
        this.getData();        
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.status  !== this.state.status &&
            this.state.status === Utils.success)
            this.createGraph();
    }

    createGraph() {
        d3.select("#holder2")
          .append('h2')
          .text("Politicians Per State")
          .style("text-align", "center")

        var json = {
            children: this.state.states
        };

        console.log(json);

        var color = d3.scaleOrdinal(d3.schemeCategory10);

        var bubble = d3.pack()
            .size([diameter, diameter])
            .padding(15);

        var svg = d3.select("#holder2")
            .append("svg")
                .attr("preserveAspectRatio", "xMinYMin meet")
                .attr("viewBox", "0 0 " + (diameter + margin.right + margin.left) + " " + diameter)
                .attr("class", "bubble");

        var root = d3.hierarchy(json)
            .sum(d => d.num_representatives)
            .sort((a, b) => b.num_representatives - a.num_representatives);

        bubble(root);

        console.log(root);

        var node = svg.selectAll(".node")
            .data(root.children)
            .enter()
            .append("g")
                .attr("class", "node")
                .attr("transform", d => "translate(" + d.x + ", " + d.y + ")")
            .append("g")
                .attr("class", "graph");

        node.append("circle")
            .attr("r", d => d.r)
            .style("fill", d => color(d.data.num_representatives));

        node.append("text")
            .attr("dy", ".3em")
            .style("font-size", "11px")
            .style("text-anchor", "middle")
            .text(d => d.data.name + " (" + d.data.num_representatives + ")");
    }

    spinner() {
        if (this.state.status === Utils.loading)
            return Utils.renderSpinner();
    }

    render() {
        return (
            <Fragment>
                {this.spinner()}
                <div id="holder2"/>
            </Fragment>
        );
    }
}
  

export default Graph2;