import React, { Component, Fragment } from 'react';

// import graphs
import Graph1 from './Graph1'
import Graph2 from './Graph2'
import Graph3 from './Graph3'

// material-ui imports
import Tabs       from '@material-ui/core/Tabs';
import Tab        from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box        from '@material-ui/core/Box';
import AppBar     from '@material-ui/core/AppBar';
import Button     from '@material-ui/core/Button';

// react-bootstrap imports
import { Container } from 'react-bootstrap';

type VisState = {
    tab_value: number,
}

function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
        <div style={{display: String(value) === String(index) ? "" : "none"}}>
            <Box p={3} >
                <Typography>{children}</Typography>
            </Box>
        </div>
    );
}

class ProviderVisualizations extends Component<any, VisState> {
    state: VisState = {
        tab_value: 0,
    };

    handleTabChange = (event, newVal) => {
        this.setState({tab_value: newVal})
    }

    openSite() {
        window.open("https://ratemypolitician.me/")
    }

    render() {
        return (
            <Container className="mt-3 mb-3">
                <h1 className="text-center mt-5" style={{fontSize: "64px"}}>Provider Visualizations</h1>
                <Button className="mx-auto mb-4 rounded-0" style={{display: "block", fontSize: "18px"}} onClick={this.openSite.bind(this)}>
                    Visit Rate My Politician
                </Button>
                <AppBar position="static" color="default">
                    <Tabs
                        variant="fullWidth"
                        value={this.state.tab_value}
                        onChange={this.handleTabChange}
                        indicatorColor="primary"
                        textColor="primary"
                        centered>
                        <Tab label="Median Income By State" />
                        <Tab label="Politicians Per State" />
                        <Tab label="Twitter Followers vs Year Founded" />
                    </Tabs>
                </AppBar>
                <TabPanel value={this.state.tab_value} index={0}>
                    <Graph1/>
                </TabPanel>
                <TabPanel value={this.state.tab_value} index={1}>
                    <Graph2/>
                </TabPanel>
                <TabPanel value={this.state.tab_value} index={2}>
                    <Graph3/>
                </TabPanel>
            </Container>
        );
    }
}
  

export default ProviderVisualizations;