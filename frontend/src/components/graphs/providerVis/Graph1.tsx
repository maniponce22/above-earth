import React, { Component, Fragment } from 'react';
import axios from 'axios';
import * as d3 from "d3";
import * as statesGeo from './states.json';

import Utils from '../../utils/Utils'

// material-ui imports
import InputLabel   from '@material-ui/core/InputLabel';
import MenuItem     from '@material-ui/core/MenuItem';
import FormControl  from '@material-ui/core/FormControl';
import Select       from '@material-ui/core/Select';
import Paper        from '@material-ui/core/Paper';
import Typography   from '@material-ui/core/Typography';
import List         from '@material-ui/core/List';
import ListItem     from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider      from '@material-ui/core/Divider';


interface USState {
    name: string;
    median_income: number;
}
// TODO: update state
type GraphState = {
    status: number;
    states: USState[];
}

const aspectRatio = 3.0 / 5; // 2:3 aspect ratio
const width = 860;
const height = width * aspectRatio;
const ease = d3.easeCubicInOut;
const popupTransition = 20;

class Graph1 extends Component<any, GraphState> {
    // TODO: update initial state, DO NOT CHANGE STATUS
    state: GraphState = {
        status: Utils.loading,
        states: []
    };

    async getData() {
        try {
            await axios.get(
                `https://ratemypolitician.me/api/states`,
            ).then(response => {
                var data = response.data;

                if ('error' in data)
                    this.setState({status: Utils.error})
                else
                    // TODO: put data in state, DON'T TOUCH SUCCESS
                    this.setState({
                        states: data,
                        status:  Utils.success
                    })
            }).catch(err => {
                this.setState({status: Utils.error});
            });            
        } catch (error) {
            this.setState({status: Utils.error});
        }
    }
    
    componentDidMount() {
        this.getData();        
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.status  !== this.state.status &&
            this.state.status === Utils.success)
            this.createGraph();
    }

    createGraph() {
        // TODO: make your graph

        d3.select("#holder1")
          .append('h2')
          .text("Median Income by State")
          .style("text-align", "center")
        
        var json = {
            children: this.state.states
        };

        var lowColor = '#f9f9f9'
        // var highColor = '#212529'
        var highColor = "#67bf6b"


        var projection = d3.geoAlbersUsa()
        .translate([width / 2, height / 2]) // translate to center of screen
        .scale([1000]); // scale things down so see entire US

        // Define path generator
        var path = d3.geoPath() // path generator that will convert GeoJSON to SVG paths
        .projection(projection); // tell path generator to use albersUsa projection

        //Create SVG element and append map to the SVG
        var svg = d3.select("#holder1")
            .append("svg")
            .attr("preserveAspectRatio", "xMinYMin meet")
            .attr("viewBox", `0 0 ${width} ${height}`)

        //Get the data in an array. 
        const dataArray : number[] = [];
        const statesNames : string[] = [];
        for (var i = 0; i < this.state.states.length; i++) {
            dataArray.push(this.state.states[i]['median_income'])
            statesNames.push(this.state.states[i]['name'])
        }

        var minVal = d3.min(dataArray)
        var maxVal = d3.max(dataArray)
        var ramp = d3.scaleLinear().domain([minVal,maxVal]).range([lowColor,highColor])

        // // Load GeoJSON data and merge with states data
        // Loop through each state data value in the .csv file
        for (var i = 0; i < dataArray.length; i++) {

            // Grab State Name
            var dataState = statesNames[i];

            // Grab data value 
            var dataValue = dataArray[i];

            // Find the corresponding state inside the GeoJSON
            for (var j = 0; j < statesGeo.features.length; j++) {
                var jsonState = statesGeo.features[j].properties['NAME'];

                if (dataState == jsonState) {
                    // Copy the data value into the JSON
                    let val: string|number = `${dataValue}`;
                    statesGeo.features[j].properties["VALUE"] = val;

                    // Stop looking through the JSON
                    break;
                }
            }
        }

        // Bind the data to the SVG and create one path per GeoJSON feature
        let mouseOver = function(this: any, event, d) {
            d3.select(this)
                .transition()
                .duration(200)
                .style("stroke-opacity", 1)

            mouseMove(event, d);
        }
        
        let mouseLeave = function(this: any, d) {
            d3.select(this)
                .transition()
                .duration(200)
                .style("stroke-opacity", .1)

            d3.select("#graph1")
                .select("#popover")
                .transition()
                .duration(popupTransition)
                .ease(ease)
                .style("opacity", 0)
                .on("end", () => {
                d3.select("#graph1")
                    .select("#popover")
                    .style("left", `0px`)
                    .style("top", `0px`)
                })
        }

        let mouseMove = function(event, d) {
            const offset = 25;
            var pop = d3.select("#graph1")
                .select("#popover")
                .style("left", `${event.pageX + offset}px`)
                .style("top", `${event.pageY - offset}px`)
                .transition()
                .duration(popupTransition)
                .ease(ease)
                .style("opacity", 1)
    
            pop.select("#name")
                .text(d.properties["NAME"])
    
            var list = pop.select("#list")
    
            list.select("#income")
                .select(".MuiListItemText-primary")
                .text(`Median Income: $${d.properties["VALUE"]}`)
        }


        var main = svg.append("g").attr("id", "main")
        main.selectAll("path")
            .data(statesGeo.features)
            .enter()
            .append("path")
            .attr("d", path)
            .style("stroke", "#fff")
            .style("stroke-width", "1")
            .style("fill", function(d) { 
                return ramp(d.properties["VALUE"])
            })
            .style("stroke", "black")
            .attr("class", function(d){ return "Country" } )
            .style("stroke-opacity", .1)
            .on("mouseover", mouseOver )
            .on("mouseleave", mouseLeave )
            .on("mousemove", mouseMove )
        
        // add a legend
        var w = 20, h = 220;

        var key = svg.append("g")
            .attr("id", "legend")
            .attr("transform", `translate(${width - w - 90}, ${height - h - 40})`)
        var legend = key.append("defs")
            .append("svg:linearGradient")
            .attr("id", "gradient")
            .attr("x1", "100%")
            .attr("y1", "0%")
            .attr("x2", "100%")
            .attr("y2", "100%")
            .attr("spreadMethod", "pad");

        legend.append("stop")
            .attr("offset", "0%")
            .attr("stop-color", highColor)
            .attr("stop-opacity", 1);
            
        legend.append("stop")
            .attr("offset", "100%")
            .attr("stop-color", lowColor)
            .attr("stop-opacity", 1);

        key.append("rect")
            .attr("width", w)
            .attr("height", h)
            .style("fill", "url(#gradient)")

        var y = d3.scaleLinear()
            .range([h, 0])
            .domain([minVal, maxVal]);

        var yAxis = d3.axisRight(y);

        key.append("g")
            .attr("class", "y axis")
            .attr("transform", `translate(${w}, 0)`)
            .call(yAxis)
    }

    spinner() {
        if (this.state.status === Utils.loading)
            return Utils.renderSpinner();
    }

    render() {
        return (
            <div id="graph1">
                {this.spinner()}
                <div id="holder1"/>
                <div id="popover" style={{position: "absolute", opacity: 0, WebkitTransform: "translate(0, -100%)"}}>
                    <Paper className="p-2">
                        <Typography id="name" variant="body1">
                            TEST
                        </Typography>
                        <Divider className="mx-0 my-1"/>
                        <List id="list" dense={true} className="mx-0 p-0">
                            <ListItem className="m-0 p-0" id="income">
                                <ListItemText
                                    className="m-0 p-0"
                                    primary=""
                                    secondary=''/>
                            </ListItem>
                        </List>
                    </Paper>
                </div>
            </div>
        );
    }
}
  

export default Graph1;