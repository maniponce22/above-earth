import React, { Component, Fragment } from 'react';
import axios from 'axios';
import * as d3 from "d3";

import Utils from '../../utils/Utils'
import { NamedTupleMember } from 'typescript';

interface NewsSource {
    city: string;
    company_rank: number;
    company_type: string;
    description: string;
    employees: number;
    foundedYear: number;
    index: number;
    logo: string;
    name: string;
    state: string;
    twitter: string;
    twitter_bio: string;
    twitter_followers: number;
}

interface SourceEntry {
    foundedYear: number;
    twitter_followers: number;
    name: string;
}

type GraphState = {
    status: number;
    sources: NewsSource[]
    plot_data: SourceEntry[]
}

// 2:3 aspect ratio
const aspectRatio = 2.0 / 3;
const width = 1000;
const height = width * aspectRatio;
const margin = {
    top: 40,
    bottom: 40,
    left: 80,
    right: 40
};

class Graph3 extends Component<any, GraphState> {
    // TODO: update initial state, DO NOT CHANGE STATUS
    state: GraphState = {
        status: Utils.loading,
        sources: [],
        plot_data: []
    };

    formatArray(data) {
        return data.map(source => {
            const entry: SourceEntry = {foundedYear: source.foundedYear, twitter_followers: source.twitter_followers, name: source.name}
            return entry
        })
    }

    async getData() {
        try {
            await axios.get(
                `https://ratemypolitician.me/api/sources`, //TODO: FIX URL
            ).then(response => {
                var data = response.data;

                if ('error' in data)
                    this.setState({status: Utils.error})
                else {
                    this.setState({
                        sources: data,
                        plot_data: this.formatArray(data),
                        status: Utils.success
                    })
                }

            }).catch(err => {
                this.setState({status: Utils.error});
            });            
        } catch (error) {
            this.setState({status: Utils.error});
        }
    }
    
    componentDidMount() {
        this.getData();        
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.status  !== this.state.status &&
            this.state.status === Utils.success)
            this.createGraph();
    }

    createGraph() {
        d3.select("#holder3")
          .append('h2')
          .text("Twitter Followers vs Year Founded for News Sources")
          .style("text-align", "center")

        var svg = d3.select("#holder3")
            .append("svg")
                .attr("preserveAspectRatio", "xMinYMin meet")
                .attr("viewBox", `0 0 ${width + margin.left + margin.right} ${height + margin.top + margin.bottom}`)
            .append("g")
                .attr("transform",
                        "translate(" + margin.left + "," + margin.top + ")");
        // Add X axis
        var x = d3.scaleLinear()
            .domain([d3.min(this.state.plot_data, d => d.foundedYear) - 10,
                 d3.max(this.state.plot_data, d => d.foundedYear) + 10])
            .range([ 0, width ]);

        svg.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(x));

        // Add Y axis
        var y = d3.scaleLinear()
            .domain([0, d3.max(this.state.plot_data, d => d.twitter_followers) + 50000])
            .range([ height, 0]);

        svg.append("g")
            .call(d3.axisLeft(y));

        var tooltip = d3.select("#holder3")
            .append("div")
            .style("opacity", 0)
            .attr("class", "tooltip")
            .style("background-color", "white")
            .style("border", "solid")
            .style("border-width", "1px")
            .style("border-radius", "5px")
            .style("padding", "10px")

        var mouseover = (event, d) => {
            tooltip.style("opacity", 1)
        }
        var mousemove = (event, d) => {
            var elem: HTMLElement = document.getElementById('holder3')!
            var offsets: DOMRect = elem.getBoundingClientRect()    
            console.log(document.body.scrollTop)
            tooltip
                .html(d.name + "<br>Year: " + d.foundedYear + "<br>Follower Count: " + d.twitter_followers)
                .style("left", (d3.pointer(event)[0] + offsets.x + 90 + document.documentElement.scrollLeft) + "px")
                .style("top", (d3.pointer(event)[1] + offsets.y + document.documentElement.scrollTop) + "px")
        }

        var mouseleave = (event, d) => {
            tooltip
              .transition()
              .duration(200)
              .style("opacity", 0)
              .on("end", () => {
                tooltip
                    .style("left",0)
                    .style("top",0)
                })
        }

        // Add dots
        svg.append('g')
            .selectAll("dot")
            .data(this.state.plot_data)
            .enter()
            .append("circle")
                .attr("cx", function (d) { return x(d.foundedYear); } )
                .attr("cy", function (d) { return y(d.twitter_followers); } )
                .attr("r", 6)
                .style("fill", "#69b3a2")
                .style("opacity", 0.3)
                .style("stroke", "white")
            .on("mouseover", mouseover )
            .on("mousemove", mousemove )
            .on("mouseleave", mouseleave )

    }

    spinner() {
        if (this.state.status !== Utils.success)
            return Utils.renderSpinner();
    }

    render() {
        return (
            <Fragment>
                {this.spinner()}
                <div id="holder3"/>
            </Fragment>
        );
    }
}
  

export default Graph3;