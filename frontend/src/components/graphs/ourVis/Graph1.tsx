import React, { Component, Fragment } from 'react';
import axios from 'axios';
import * as d3 from "d3";

// material-ui imports
import InputLabel   from '@material-ui/core/InputLabel';
import MenuItem     from '@material-ui/core/MenuItem';
import FormControl  from '@material-ui/core/FormControl';
import Select       from '@material-ui/core/Select';
import Paper        from '@material-ui/core/Paper';
import Typography   from '@material-ui/core/Typography';
import List         from '@material-ui/core/List';
import ListItem     from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider      from '@material-ui/core/Divider';

import Utils from '../../utils/Utils'
import * as dataGeo from './world.json';

function padDist(p1, p2) {
    let lon_dist = Math.abs(p1.lon - p2.lon)
    let lat_dist = Math.abs(p1.lat - p2.lat)
    return Math.sqrt(Math.pow(lon_dist, 2) + Math.pow(lat_dist, 2))
}

function numToSelectString(num) {
    return String(num).replaceAll(".", "_")
}

const aspectRatio = 2.0 / 3; // 2:3 aspect ratio
const width = 1000;
const height = width * aspectRatio;
// const margin = {
//     top: 20,
//     bottom: 80,
//     left: 80,
//     right: 20,
// };
const font = "Arial Narrow"

const max_pad_dist = .5;

const colors = [
    "#67bf6b",
    "#a1cd49",
    "#518bc9",
    "#27beb6",
    "#b856a1",
    "#915ba6",
    "#ee2f44",
    "#ec468b",
    "#f36b28",
    "#ef4924",
    "#efd31a",
    "#f4ae1a",
]


const opacityTransition = 200;
const faded = .4;
const extraFaded = .1;
const bubbleGrow = 10;

const ease = d3.easeCubicInOut;
const transitionLength = 1000;
const popupTransition = 20;

const bubbleExtent = [5, 50];

// TODO: update state
type GraphState = {
    pads: {
        count, number,
        name: string,
        lon: number,
        lat: number,
        color: string,
    }[];
    status: number;
}

class Graph1 extends Component<any, GraphState> {
    state: GraphState = {
        pads: [],
        status: Utils.starting,
    };

    async getPads() {
        try {
            await axios.get(
                `${process.env.REACT_APP_API_URL}/overview/pads`
            ).then(response => {
                var data = response.data;
                var pads: any[] = [];

                if ('error' in data)
                    this.setState({status: Utils.error})
                else
                    pads = data.pads.map((p, i) => (
                        {
                            count: p.count,
                            name: p.name,
                            lat: parseFloat(p.latitude),
                            lon: parseFloat(p.longitude),
                        }
                    ))

                    pads.sort((a,b) => b.count - a.count)

                    // find closest pads
                    var pad_groups: any = []
                    var pads = pads.map(p => {
                        for (let i = 0; i < pad_groups.length; i++) {
                            let g = pad_groups[i]
                            if (padDist(p, g) < max_pad_dist)
                                return {...p, color_i: i };
                        }

                        pad_groups.push({lon: p.lon, lat: p.lat})
                        return {...p, color_i: pad_groups.length - 1};
                    })

                    pads = pads.map(p => ({
                        count: p.count,
                        name: p.name,
                        lat: p.lat,
                        lon: p.lon,
                        color: colors[p.color_i % colors.length]
                    }))

                    this.setState({
                        pads: pads,
                        status:    Utils.success
                    })
            }).catch(err => {
                this.setState({status: Utils.error});
            });            
        } catch (error) {
            this.setState({status: Utils.error});
        }
    }
    
    componentDidMount() {
        this.getPads();        
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.status !== this.state.status &&
            this.state.status === Utils.success)
            this.createGraph();
    }

    createGraph() {
        var data = this.state.pads
        var page = d3.select("#graph1")

        // create SVG
        var svg = page.select("#holder1")
            .append("svg")
            .attr("preserveAspectRatio", "xMinYMin meet")
            .attr("viewBox", `0 0 ${width} ${height}`)
            .style("border", "1px solid black")

        // Map and projection
        var projection = d3.geoMercator()
        
        // Add a scale for bubble size
        var countExtent = d3.extent(data, d => d.count)
        var bubbleScale = d3.scaleSqrt()
            .domain(countExtent)  // What's in the data
            .range(bubbleExtent)  // Size in pixel

        // create main graph area
        var main = svg.append("g")

        // // Draw the map
        main.append("g")
            .attr("id", "map")
            .selectAll("path")
            .data(dataGeo.features)
            .enter()
            .append("path")
            .attr("fill", "lightgrey")
            .attr("d", d3.geoPath().projection(projection))
            .style("stroke", "#aaaaaa")
            .style("opacity", faded)

        const zoom = d3.zoom()
            .scaleExtent([159, Infinity])
            .on("zoom", zoomed);

        main.append("rect")
            .attr("id", "mouse_rect")
            .attr("x", 0)
            .attr('y', 0)
            .attr('width', width)
            .attr('height', height)
            .style("fill", "none")
            .style('stroke', "none")
            .style("pointer-events", "none")
            .call(zoom)
            .call(zoom.transform, d3.zoomIdentity.translate(width/2, (height + 200)/2).scale(10000))
            .transition()
            .duration(2000)
            .ease(ease)
            .call(zoom.transform, d3.zoomIdentity.translate(width/2, (height + 200)/2).scale(159))
            .on("end", () => {
                main.select("#mouse_rect")
                    .style("pointer-events", "all")
            })

        // Add circles:
        main.append("g")
            .attr('id', 'bubbles')
            .selectAll("circle")
            .data(data)
            .enter()
            .append("circle")
            .attr("id", d => `padCircle_${numToSelectString(d.lon)}_${numToSelectString(d.lat)}`)
            .attr("cx", d => projection([d.lon, d.lat])[0])
            .attr("cy", d =>projection([d.lon, d.lat])[1])
            .attr("r", d => bubbleScale(d.count))
            .style("fill", d => d.color)
            .style("stroke", "lightgrey")
            .style("stroke-width", ".8px")
            .attr("opacity", faded)
            .on("mouseover", (e, d) => {
                main.select("#bubbles")
                    .selectAll("circle")
                    .filter(c => (c.lon !== d.lon && c.lat !== d.lat))
                    .transition()
                    .duration(opacityTransition)
                    .ease(ease)
                    .style("opacity", extraFaded)

                main.select(`#padCircle_${numToSelectString(d.lon)}_${numToSelectString(d.lat)}`)
                    .transition()
                    .duration(opacityTransition)
                    .ease(ease)
                    .style("opacity", 1)
                    .attr("r", bubbleScale(d.count) + bubbleGrow)

                this.handlePopOpen(e, d);
            })
            .on("mouseout", (e, d) => {
                main.select(`#padCircle_${numToSelectString(d.lon)}_${numToSelectString(d.lat)}`)
                    .transition()
                    .duration(opacityTransition)
                    .ease(ease)
                    .style("opacity", faded)
                    .attr("r", bubbleScale(d.count))

                main.select("#bubbles")
                    .selectAll("circle")
                    .filter(c => (c.lon !== d.lon && c.lat !== d.lat))
                    .transition()
                    .duration(opacityTransition)
                    .ease(ease)
                    .style("opacity", faded)

                this.handlePopClose();
            })
            .on("mousemove", this.handlePopOpen)




        // Add legend: circles
        const valuesToShow = [1,10,25]
        const padding = 20
        const xCircle = padding + bubbleScale(valuesToShow[valuesToShow.length - 1])
        const xLabel = xCircle * 2 + 20
        var legend = svg.append("g").attr("id", "legend")
        legend.selectAll("legend")
            .data(valuesToShow)
            .enter()
            .append("circle")
            .attr("cx", xCircle)
            .attr("cy", d => height - bubbleScale(d) - padding)
            .attr("r", d => bubbleScale(d))
            .style("fill", "none")
            .attr("stroke", "black")
            .attr("stroke-width", ".5px")

        // Add legend: segments
        legend.selectAll("legend")
            .data(valuesToShow)
            .enter()
            .append("line")
            .attr('x1', d => xCircle + bubbleScale(d))
            .attr('x2', xLabel)
            .attr('y1', d => height - bubbleScale(d) - padding)
            .attr('y2', d => height - bubbleScale(d) - padding)
            .attr('stroke', 'black')
            .style('stroke-dasharray', ('2,2'))

        // Add legend: labels
        legend.selectAll("legend")
            .data(valuesToShow)
            .enter()
            .append("text")
            .attr('x', xLabel)
            .attr('y', d => height - bubbleScale(d) - padding)
            .text(d => d)
            .style("font-size", 10)
            .attr('alignment-baseline', 'middle')

        // add title
        svg.append("text")
            .attr("x", width - padding)
            .attr("y", height - padding)
            .attr("dy", "-1em")
            .style("text-anchor", "end")
            .style("font-family", font)
            .style("font-size", "24px")
            .text("Expeditions Per Launch Pad")
        svg.append("text")
            .attr("x", width - padding)
            .attr("y", height - padding)
            .style("text-anchor", "end")
            .style("font-family", font)
            .style("font-size", "16px")
            .text("Scroll and drag me!")

        function zoomed(event) {
            var new_projection = d3.geoMercator()
                .scale(event.transform.k)
                .translate([(event.transform.x) , (event.transform.y)])

            main.select("#map")
                .selectAll("path")
                .attr("d", d3.geoPath().projection(new_projection))
            main.select("#bubbles")
                .selectAll("circle")
                .attr("cx", d => new_projection([d.lon, d.lat])[0])
                .attr("cy", d => new_projection([d.lon, d.lat])[1])
        }
    }

    handlePopOpen = (event, d) => {
        const offset = 25;
        var pop = d3.select("#graph1")
            .select("#popover")
            .style("left", `${event.pageX + offset}px`)
            .style("top", `${event.pageY - offset}px`)
            .transition()
            .duration(popupTransition)
            .ease(ease)
            .style("opacity", 1)

        pop.select("#name")
            .text(d.name)

        var list = pop.select("#list")

        list.select("#coordinates")
            .select(".MuiListItemText-primary")
            .text(`Coordinates: (${d.lat}, ${d.lon})`)
        list.select("#launches")
            .select(".MuiListItemText-primary")
            .text(() => {
                if (d.count === 1)
                    return `${d.count} Expeditions`;
                return `${d.count} Expeditions`;
            })
    };

    handlePopClose = () => {
        d3.select("#graph1")
            .select("#popover")
            .transition()
            .duration(popupTransition)
            .ease(ease)
            .style("opacity", 0)
            .on("end", () => {
                d3.select("#graph1")
                    .select("#popover")
                    .style("left", `0px`)
                    .style("top", `0px`)
            })
    }

    spinner() {
        if (this.state.status !== Utils.success)
            return Utils.renderSpinner();
    }

    render() {
        return (
            <div id="graph1">
                {this.spinner()}
                <div id="holder1"/>
                <div id="popover" style={{position: "absolute", opacity: 0, WebkitTransform: "translate(0, -100%)"}}>
                    <Paper className="p-2">
                        <Typography id="name" variant="body1">
                            TEST
                        </Typography>
                        <Divider className="mx-0 my-1"/>
                        <List id="list" dense={true} className="mx-0 p-0">
                            <ListItem className="m-0 p-0" id="coordinates">
                                <ListItemText
                                    className="m-0 p-0"
                                    primary=""
                                    secondary=''/>
                            </ListItem>
                            <ListItem className="m-0 p-0" id="launches">
                                <ListItemText
                                    className="m-0 p-0"
                                    primary=""
                                    secondary=''/>
                            </ListItem>
                        </List>
                    </Paper>
                </div>
            </div>
        );
    }
}
  
export default Graph1;
