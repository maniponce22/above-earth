import React, { Component, Fragment } from 'react';

// import graphs
import Graph1 from './Graph1'
import Graph2 from './Graph2'
import Graph3 from './Graph3'

// material-ui imports
import Tabs       from '@material-ui/core/Tabs';
import Tab        from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box        from '@material-ui/core/Box';
import AppBar     from '@material-ui/core/AppBar';

// react-bootstrap imports
import { Container } from 'react-bootstrap';

type VisState = {
    tab_value: number,
}

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div style={{display: String(value) === String(index) ? "" : "none"}}>
            <Box p={3} >
                <Typography>{children}</Typography>
            </Box>
        </div>
    );
}

class OurVisualizations extends Component<any, VisState> {
    state: VisState = {
        tab_value: 0,
    };

    handleTabChange = (event, newVal) => {
        this.setState({tab_value: newVal})
    }

    render() {
        return (
            <Container className="mt-3 mb-3">
                <h1 className="text-center my-5" style={{fontSize: "64px"}}>Our Visualizations</h1>
                <AppBar position="static" color="default">
                    <Tabs
                        variant="fullWidth"
                        value={this.state.tab_value}
                        onChange={this.handleTabChange}
                        indicatorColor="primary"
                        textColor="primary"
                        centered>
                        <Tab label="Expeditions Around the World" />
                        <Tab label="Expeditions Per Agency" />
                        <Tab label="Expeditions Over Time" />
                    </Tabs>
                </AppBar>
                <TabPanel value={this.state.tab_value} index={0}>
                    <Graph1/>
                </TabPanel>
                <TabPanel value={this.state.tab_value} index={1}>
                    <Graph2/>
                </TabPanel>
                <TabPanel value={this.state.tab_value} index={2}>
                    <Graph3/>
                </TabPanel>
            </Container>
        );
    }
}
  

export default OurVisualizations;