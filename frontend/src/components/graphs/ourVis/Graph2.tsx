import React, { Component, Fragment } from 'react';
import axios from 'axios';
import * as d3 from "d3";

// material-ui imports
import InputLabel   from '@material-ui/core/InputLabel';
import MenuItem     from '@material-ui/core/MenuItem';
import FormControl  from '@material-ui/core/FormControl';
import Select       from '@material-ui/core/Select';
import Paper        from '@material-ui/core/Paper';
import Typography   from '@material-ui/core/Typography';
import List         from '@material-ui/core/List';
import ListItem     from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider      from '@material-ui/core/Divider';

// import utils
import Utils from '../../utils/Utils'

type Agency = {
    id:                  number;
    name:                string;
    abbrev:              string;
    country:             string;
    founding_year:       number;
    type:                string;
    total_launch_count:  number;
    logo_url:            string;
};


const agencyAttr = new Set([
    "id", "name", "abbrev", "country", "founding_year", "type", 
    "total_launch_count", "logo_url"]);

const aspectRatio = 2.0 / 3; // 2:3 aspect ratio
const width = 1000;
const height = width * aspectRatio;
const margin = {
    top: 20,
    bottom: 80,
    left: 80,
    right: 20,
};
const font = "Arial Narrow"

const numBars = 50;
const y_ticks = 10;


const opacityTransition = 200;
const faded = .8;
const extraFaded = .2;

const ease = d3.easeCubicInOut;
const transitionLength = 1000;
const popupTransition = 20;

const circleRadius = 15;
const circlePadding = 5;


const sort_options = [
    { name: "Name", value: "name" },
    { name: "Country", value: "country" },
    { name: "Founding Year", value: "founding_year"},
    { name: "Agency Type", value: "type" },
    { name: "Expeditions", value: "total_launch_count" },
]

const legend_options = [
    { name: "Country", value: "country" },
    { name: "Agency Type", value: "type" },
]

const colors = [
    "#efd31a",
    "#f36b28",
    "#ee2f44",
    "#b856a1",
    "#518bc9",
    "#67bf6b",
    "#f4ae1a",
    "#ef4924",
    "#ec468b",
    "#915ba6",
    "#27beb6",
    "#a1cd49",
]

type GraphState = {
    status:   number;
    sort:     string;
    legend:   string;
    agencies: Agency[];
}

class Graph2 extends Component<any, GraphState> {
    state: GraphState = {
        status:   Utils.starting,
        sort:     "total_launch_count",
        legend:   "country",
        agencies: []
    };

    sortRef;
    legendRef;

    constructor(props) {
        super(props);
        this.sortRef = React.createRef();
        this.legendRef = React.createRef();
    }

    async getData() {
        try {
            await axios.get(
                `${process.env.REACT_APP_API_URL}/agencies?limit=${numBars}&sort_by=total_launch_count&order_by=desc`
            ).then(response => {
                var data = response.data;
                var updateArr: Agency[] = [];
                
                if ('error' in data)
                    this.setState({status: Utils.error})
                else
                    for (var a of data.agencies) {
                        var insert_agency = {};
                        for (var attr in a)
                            if (agencyAttr.has(attr)) {
                                if (attr === "founding_year")
                                    insert_agency[attr] = parseInt(a[attr]);
                                else if (attr === "country")
                                    insert_agency[attr] = a[attr].replace(", ", "/")
                                else
                                    insert_agency[attr] = a[attr];
                            }
                        updateArr.push(insert_agency as Agency);
                    }

                    this.setState({
                        status:   Utils.success,
                        agencies: updateArr,
                    })

            }).catch(err => {
                this.setState({status: Utils.error});
            });            
        } catch (error) {
            this.setState({status: Utils.error});
        }
    }
    
    componentDidMount() {
        this.getData();        
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.status  !== this.state.status &&
            this.state.status === Utils.success)
            this.createGraph();
    }

    createGraph() {
        var data = this.state.agencies;
        var page = d3.select("#graph2")

        // create SVG and main group
        var svg = page.select("#holder2")
            .append("svg")
            .attr("preserveAspectRatio", "xMinYMin meet")
            .attr("viewBox", `0 0 ${width + margin.left + margin.right} ${height + margin.top + margin.bottom}`)

        // get legend data
        var legend_data = getLegendData(this.state.legend)
        var color_map = getColorMap(legend_data);

        // create x axis
        var x_scale = d3.scaleBand()
            .domain(data.map((a, i) => `${a.abbrev}_${a.id}`))
            .range([0, width])
            .padding(.1)
        var x_axis_group = svg.append("g")
            .attr("transform", `translate(${margin.left}, ${height + margin.top})`)
        var x_axis = d3.axisBottom(x_scale).tickSizeOuter(0)
            .tickFormat(abbrev => abbrev.split("_")[0])
        x_axis_group.append("g")
            .call(x_axis)
            .selectAll("text")
            .attr("transform", "translate(-8, 4), rotate(-45)")
            .style("text-anchor", "end");
        x_axis_group.append("text")
            .attr("transform", `translate(${width/2}, 0)`)
            .attr("dy", "3.5em")
            .attr("text-anchor", "middle")
            .attr('fill', 'black')
            .attr("font-family", font)
            .attr("font-size", "20px")
            .text("Agency Abbreviation")

        // create y axis
        var y_scale = d3.scaleSqrt()
            .domain([0, data[0].total_launch_count * 1.2])
            .range([height, 0])
            .nice()
        var y_axis_group = svg.append("g")
            .attr("transform", `translate(${margin.left}, ${margin.top})`)
        var y_grid = y_axis_group.append("g")
            .style("pointer-events", "none")
            .style("cursor", "default")
            .call(d3.axisLeft(y_scale)
                .ticks(y_ticks)
                .tickSize(-width)
                .tickFormat(""))
        y_grid.selectAll("line")
            .style("stroke", "lightgrey")
            .style("stroke-opacity", .7)
            .style("shape-rendering", "crispEdges")
        y_grid.selectAll("path")
            .style("stroke-width", 0)
        y_axis_group.append("g")
            .call(d3.axisLeft(y_scale).ticks(y_ticks))
        y_axis_group.append("text")
            .attr("transform", `translate(0, ${height/2}), rotate(-90)`)
            .attr("dy", "-3em")
            .attr("text-anchor", "middle")
            .attr('fill', 'black')
            .attr("font-family", font)
            .attr("font-size", "20px")
            .text("Number of Expeditions")

        // create main graph area
        var main = svg.append("g")
            .attr("transform", `translate(${margin.left}, ${margin.top})`)

        // add the bars
        var bars = main.append("g")
            .attr("id", "bars")
            .selectAll("rect")
            .data(data)
            .enter()
            .append("rect")
            .attr("id", d => `agencyRect_${d.id}`)
            .attr("x", d => x_scale(`${d.abbrev}_${d.id}`))
            .attr('y', d => (y_scale(d.total_launch_count)))
            .attr("width", x_scale.bandwidth())
            .attr("height", d => y_scale(0) - y_scale(d.total_launch_count))
            .style("fill", d => color_map[d[this.state.legend]])
            .style("opacity", faded)
            .attr("aria-haspopup", true)
            .style("cursor", "pointer")
            .on("mouseover", (e, d) => {
                bars.filter(r => r !== d)
                    .transition()
                    .duration(opacityTransition)
                    .ease(ease)
                    .style("opacity", extraFaded)
                main.select("#bars")
                    .select(`#${e.target.id}`)
                    .transition()
                    .duration(opacityTransition)
                    .ease(ease)
                    .style("opacity", 1)

                main.select("#circles")
                    .select(`#agencyCircle_${d.id}`)
                    .select("circle")
                    .transition()
                    .duration(opacityTransition)
                    .ease(ease)
                    .attr("r", circleRadius)
                main.select("#circles")
                    .select(`#agencyCircle_${d.id}`)
                    .select("text")
                    .transition()
                    .duration(opacityTransition)
                    .ease(ease)
                    .attr("font-size", "default")

                this.handlePopOpen(e, d);
            })
            .on("mouseout", (e, d) => {
                bars.filter(r => r !== d)
                    .transition()
                    .duration(opacityTransition)
                    .ease(ease)
                    .style("opacity", faded)
                main.select("#bars")
                    .select(`#${e.target.id}`)
                    .transition()
                    .duration(opacityTransition)
                    .ease(ease)
                    .style("opacity", faded)

                main.select("#circles")
                    .select(`#agencyCircle_${d.id}`)
                    .select("circle")
                    .transition()
                    .duration(opacityTransition)
                    .ease(ease)
                    .attr("r", 0)
                main.select("#circles")
                    .select(`#agencyCircle_${d.id}`)
                    .select("text")
                    .transition()
                    .duration(opacityTransition)
                    .ease(ease)
                    .attr("font-size", "0px")

                this.handlePopClose();
            })
            .on("mousemove", this.handlePopOpen)
            .on("click", (e, d) => {
                window.location.href = "/agencies/" + d.id;
            })
        
        
        
        var circles = main.append("g")
            .attr("id", "circles")
            .selectAll("g")
            .data(data)
            .enter()
            .append("g")
            .attr("id", d => `agencyCircle_${d.id}`)
            .style("z-index", "1")
            .each((d) => {
                main.select("#circles")
                    .select(`#agencyCircle_${d.id}`)
                    .selectAll("circle")
                    .data([d])
                    .enter()
                    .append("circle")
                    .attr("cx", c => x_scale(`${c.abbrev}_${c.id}`) + (x_scale.bandwidth() / 2))
                    .attr("cy", c => y_scale(c.total_launch_count) - circleRadius - circlePadding)
                    .attr("r", 0)
                    .style("fill", c => color_map[c[this.state.legend]])
                main.select("#circles")
                    .select(`#agencyCircle_${d.id}`)
                    .selectAll("text")
                    .data([d])
                    .enter()
                    .append("text")
                    .attr("x", c => x_scale(`${c.abbrev}_${c.id}`) + (x_scale.bandwidth() / 2))
                    .attr("y", c => y_scale(c.total_launch_count) - circleRadius - circlePadding)
                    .attr("dy", ".1em")
                    .attr("text-anchor", "middle")
                    .attr("dominant-baseline", "middle")
                    .style("fill", "white")
                    .attr("font-family", font)
                    .attr("font-size", "0px")
                    .text(c => c.total_launch_count)
            })

        // create legend
        addLegend(legend_data, this.state.legend)

        // handle sort changes
        page.select("#sort")
            .on("click", e => {
                sort(e.target.value)
            })

        // handle sort changes
        page.select("#legend")
            .on("click", e => {
                var legend_data = getLegendData(e.target.value)
                updateLegend(legend_data, e.target.value)
            })
        

        function sort(order) {
            var new_data = data;
            if (typeof data[0][order] === "number")
                if (order === "total_launch_count")
                    new_data = data.sort((a, b) => b[order] - a[order]);
                else
                    new_data = data.sort((a, b) => a[order] - b[order]);
            else
                new_data = data.sort((a, b) => (a[order]).localeCompare(b[order]));

            x_scale.domain(new_data.map(a => `${a.abbrev}_${a.id}`));

            bars.transition()
                .duration(transitionLength)
                .ease(ease)
                .attr("x", (d, i) => x_scale(`${d.abbrev}_${d.id}`))
                .on("start", () => {
                    bars.style("pointer-events", "none");
                    svg.select("#legend")
                        .selectAll("g")
                        .style("pointer-events", "none");
                })
                .on("end", () => {
                    bars.style("pointer-events", "all");
                    svg.select("#legend")
                        .selectAll("g")
                        .style("pointer-events", "all");
                })

            circles.selectAll("circle")
                .attr("cx", c => x_scale(`${c.abbrev}_${c.id}`) + (x_scale.bandwidth() / 2))
            circles.selectAll("text")
                .attr("x", c => x_scale(`${c.abbrev}_${c.id}`) + (x_scale.bandwidth() / 2))

            x_axis_group.select("g")
                .transition()
                .duration(transitionLength)
                .ease(ease)
                .call(x_axis)
                .selectAll("text")
                .attr("transform", "translate(-8, 4), rotate(-45)")
                .style("text-anchor", "end");
        }

        function getLegendData(legendType) {
            var s: Set<string> = new Set();
            for (var agency of data) {
                var l = String(agency[legendType])
                if (!(l in s))
                    s.add(l)
            }

            var arr = Array.from(s);
            arr = arr.sort();

            var legendData = arr.map((l, i) => ({ name: l, color: colors[i % colors.length] }))
            return legendData
        }

        function getColorMap(legendData) {
            var colorMap = {}
            legendData.map(l => {colorMap[l.name] = l.color})
            return colorMap
        }

        function addLegend(legendData, legendVar) {
            const legend_padding = 20
            if (svg.select("#legend").empty())
                svg.append("g")
                    .attr("id", "legend")
                    .attr("transform", `translate(${margin.left + width - legend_padding}, ${margin.top + legend_padding})`)

            var legend = svg.select("#legend")

            var transition_time = transitionLength / 1.4;
            if (legend.select("*").empty()) {
                transition_time = 0;
                legend.append("g").attr("id", "prev")
            }

            // prevent breaks from spamming
            legend.select("#new").remove();

            const legend_ease_in = d3.easeElasticIn.period(0.7);
            const legend_ease_out = d3.easeElasticOut.period(0.7);

            legend.select("#prev")
                .style("pointer-events", "none")
                .transition()
                .duration(transition_time)
                .ease(legend_ease_in)
                .attr("transform", "translate(200, 0)")
                .on("end", () => {
                    legend.select("#prev").remove();
                })
            
            legend.append("g")
                .attr("id", "new")
                .attr("transform", "translate(200, 0)")
                .selectAll("g")
                .data(legendData)
                .enter()
                .append("g")
                .attr("id", (d, i) => `l_${i}`)
                .attr("transform", (d, i) => `translate(0, ${i*20})`)
                .on("mouseover", (e, x) => {
                    bars.filter(r => r[legendVar] === x.name)
                        .transition()
                        .duration(opacityTransition)
                        .ease(ease)
                        .style("opacity", 1)
                    bars.filter(r => r[legendVar] !== x.name)
                        .transition()
                        .duration(opacityTransition)
                        .ease(ease)
                        .style("opacity", extraFaded)
                    legend.select(`#${x.name.replace('/', '_')}`)
                        .transition()
                        .duration(opacityTransition)
                        .ease(ease)
                        .style("opacity", 1)
                    legend.select("#prev")
                        .selectAll("g")
                        .filter(r => (x !== r))
                        .transition()
                        .duration(opacityTransition)
                        .ease(ease)
                        .style("opacity", extraFaded)
                })
                .on("mouseout", (e, x) => {
                    bars.transition()
                        .duration(opacityTransition)
                        .ease(ease)
                        .style("opacity", faded)
                    legend.select(`#${x.name.replace('/', '_')}`)
                        .transition()
                        .duration(opacityTransition)
                        .ease(ease)
                        .style("opacity", faded)
                    legend.select("#prev")
                        .selectAll("g")
                        .filter(r => (x !== r))
                        .transition()
                        .duration(opacityTransition)
                        .ease(ease)
                        .style("opacity", faded)
                })
                .each((d, i) => {
                    var this_group = legend.select("#new").select(`#l_${i}`)
                    this_group.append("text")
                        .attr("dx", "-2em")
                        .attr("dy", ".1em")
                        .attr("dominant-baseline", "middle")
                        .attr("text-anchor", "end")
                        .attr("font-family", font)
                        .style("cursor", "default")
                        .text(d.name)
                    this_group.selectAll("line")
                        .data([d])
                        .enter()
                        .append("line")
                        .attr("id", x => x.name.replace('/', '_'))
                        .attr('x1', -20)
                        .attr('y1', 0)
                        .attr('x2', 0)
                        .attr('y2', 0)
                        .style("stroke", x => x.color)
                        .style("stroke-width", "12")
                        .style("opacity", faded)
                })

            legend.select("#new")
                .style("pointer-events", "none")
                .transition()
                .duration(transition_time)
                .delay(transition_time)
                .ease(legend_ease_out)
                .attr("transform", "translate(0, 0)")
                .on("end", () => {
                    legend.select("#new")
                        .style("pointer-events", "all")
                        .attr("id", "prev")
                })
        }

        function updateLegend(legendData, legendVar) {
            addLegend(legendData, legendVar);
            var color_map = getColorMap(legendData);

            bars.transition()
                .duration(transitionLength)
                .ease(ease)
                .style("fill", d => color_map[d[legendVar]])
                .on("start", () => {
                    bars.style("pointer-events", "none");
                })
                .on("end", () => {
                    bars.style("pointer-events", "all");
                })

            circles.selectAll("circle")
                .style("fill", d => color_map[d[legendVar]])
        }
    }

    spinner() {
        if (this.state.status !== Utils.success)
            return Utils.renderSpinner();
    }

    optionBar() {
        if (this.state.status === Utils.success)
            return (
                <div className="mb-3">
                    <FormControl style={{width: "calc(50% - 8px)"}}>
                        <InputLabel>Sort By</InputLabel>
                        <Select
                            id="sort-select"
                            value={this.state.sort}
                            onChange={e => {
                                this.setState({sort: String(e.target.value)});
                                this.sortRef.current.value = e.target.value;
                                this.sortRef.current.click();
                            }}
                        >
                        {sort_options.map(o => 
                            <MenuItem value={o.value}>{o.name}</MenuItem>)}
                        </Select>
                    </FormControl>
                    <FormControl style={{width: "calc(50% - 8px)", marginLeft: "16px"}}>
                        <InputLabel>Choose Legend</InputLabel>
                        <Select
                            id="legend-select"
                            value={this.state.legend}
                            onChange={e => {
                                this.setState({legend: String(e.target.value)});
                                this.legendRef.current.value = e.target.value;
                                this.legendRef.current.click();
                            }}
                        >
                        {legend_options.map(o => 
                            <MenuItem value={o.value}>{o.name}</MenuItem>)}
                        </Select>
                    </FormControl>
                </div>
            );
    }

    handlePopOpen = (event, d) => {
        const offset = 25;
        var pop = d3.select("#graph2")
            .select("#popover")
            .style("left", `${event.pageX + offset}px`)
            .style("top", `${event.pageY - offset}px`)
            .transition()
            .duration(popupTransition)
            .ease(ease)
            .style("opacity", 1)

        pop.select("#name")
            .text(d.name)

        var list = pop.select("#list")

        list.select("#info")
            .select(".MuiListItemText-primary")
            .text(`${d.country} | ${d.type} | Founded in ${d.founding_year}`)
        list.select("#launches")
            .select(".MuiListItemText-primary")
            .text(`${d.total_launch_count} Expeditions`)
    };

    handlePopClose = () => {
        d3.select("#graph2")
            .select("#popover")
            .transition()
            .duration(popupTransition)
            .ease(ease)
            .style("opacity", 0)
            .on("end", () => {
                d3.select("#graph2")
                    .select("#popover")
                    .style("left", `0px`)
                    .style("top", `0px`)
            })
    }

    render() {
        return (
            <div id="graph2" className="mx-0">
                {this.spinner()}
                <input style={{display: "none"}} ref={this.sortRef} id="sort" value={this.state.sort} type="text"/>
                <input style={{display: "none"}} ref={this.legendRef} id="legend" value={this.state.legend} type="text"/>
                {this.optionBar()}
                <div id="holder2"/>

                <div id="popover" style={{position: "absolute", opacity: 0, WebkitTransform: "translate(0, -100%)"}}>
                    <Paper className="p-2">
                        <Typography id="name" variant="body1">
                            TEST
                        </Typography>
                        <Divider className="mx-0 my-1"/>
                        <List id="list" dense={true} className="mx-3 p-0">
                            <ListItem className="m-0 p-0" id="info">
                                <ListItemText
                                    className="m-0 p-0"
                                    primary=""
                                    secondary=''/>
                            </ListItem>
                            <ListItem className="m-0 p-0" id="launches">
                                <ListItemText
                                    className="m-0 p-0"
                                    primary=""
                                    secondary=''/>
                            </ListItem>
                            <ListItem className="m-0 p-0" >
                                <ListItemText
                                    className="m-0 p-0"
                                    secondary='Click to go to page'/>
                            </ListItem>
                        </List>
                    </Paper>
                </div>
            </div>
        );
    }
}

export default Graph2;
