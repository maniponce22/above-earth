import React, { Component, Fragment } from 'react';
import axios from 'axios';
import * as d3 from "d3";

import Utils from '../../utils/Utils'




interface ExpeditionSource {
    agency_abbrev:          string;
    agency_country:         string;
    agency_id:              number;
    agency_name:            string;
    // date:                   Date;
    date:                   string;
    id:                     string;
    image:                  string;
    most_recent_news_story: Date;
    name:                   string;
    pad_name:               string;
    rocket_name:            string;
    total_story_count:      number;
}

// launches over time

interface SourceEntry {
    // date: Date;
    date: string;
    // count: number;
    // expeditions: [];
    value: number;
    // year: number;
}


// TODO: update state
type GraphState = {
    status: number;
    // sources: ExpeditionSource[];
    sources: ExpeditionSource[];
    plot_data: SourceEntry[];
}



const w = 1100,
    h = 600,
    margin = {
      top: 10,
      bottom: 100,
      left: 80,
      right: 30
    };

const width = w - margin.right - margin.left;
const height = h - margin.top - margin.bottom;

const font = "Arial Narrow"



class Graph3 extends Component<any, GraphState> {
    // TODO: update initial state, DO NOT CHANGE STATUS
    state: GraphState = {
        status: Utils.loading,
        sources: [],
        plot_data: []
    };

    formatArray(data) {
        return data.expeditions.map(source => {
            const entry: SourceEntry = {date: source.date, value: 1000}
            return entry
        })
    }

    async getData() {
        try {
            await axios.get(
                `${process.env.REACT_APP_API_URL}/expeditions?limit=100000`, //TODO: FIX URL
            ).then(response => {
                var data = response.data;
    
                
                if ('error' in data)
                    this.setState({status: Utils.error})
                else {
                    this.setState({sources: data, plot_data: this.formatArray(data)})
                    // TODO: put data in state, DON'T TOUCH SUCCESS
                    this.setState({status:  Utils.success})
                }
            }).catch(err => {
                this.setState({status: Utils.error});
            });            
        } catch (error) {
            this.setState({status: Utils.error});
        }
    }
    
    componentDidMount() {
        this.getData();        
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.status  !== this.state.status &&
            this.state.status === Utils.success)
            this.createGraph();
    }


    createGraph() {



        
        var yearList = this.state.plot_data.map(entry => {
            var dateTemp = entry.date;
            var Syear = dateTemp.substring(0,4);
            return parseInt(Syear)
        });

        yearList.sort();

        var dateCountDict = {};

        for(var i = 0; i <yearList.length; i++) {
            var year = yearList[i];
            dateCountDict[year] = 0;
        }
        for(var i = 0; i < yearList.length; i++) {
            var year = yearList[i];
            dateCountDict[year]++;
        }


        var dateList = this.state.plot_data.map(entry => {
            entry.date = entry.date.substring(0,4) + "-01-01";
            var year = entry.date.substring(0,4);
            entry.value = dateCountDict[year];


            return { date: d3.timeParse("%Y-%m-%d")(entry.date), value : entry.value };
        })


        var str = "";
        for(var i = 0; i < dateList.length; i++) {
            str += dateList[i].date + " " + dateList[i].value + "\n";
        }

        var yearSetArr = Array.from(new Set(yearList));
        
        var dateList2 = yearSetArr.map(year => {

            var strYear = year + "-01-01";

            return { date: d3.timeParse("%Y-%m-%d")(strYear), value : dateCountDict[year]};
        })

        

        // TODO: make your graph
        d3.select("#holder3")
          .append('h2')
          .text("Expeditions Over Time")
          .attr("class", "text-center")



        var svg = d3.select("#holder3")
          .append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
          .append("g")
            .attr("transform",
                  "translate(" + margin.left + "," + margin.top + ")");


        


            // Add X axis 
            
            var x = d3.scaleTime()
            .domain(
                d3.extent(dateList2, function(d) { return d.date })
            )
            .range([ 0, width ]);


            var x_axis_group = svg.append("g")
                .attr("transform", `translate(${margin.left-70}, ${height + margin.top})`)
            
            x_axis_group.append("g")
                .call(x)
                .selectAll("text")
            x_axis_group.append("text")
                .attr("transform", `translate(${width/2}, 0)`)
                .attr("dy", "2em")
                .attr("text-anchor", "middle")
                .attr('fill', 'black')
                .attr("font-family", font)
                .attr("font-size", "20px")
                .text("Year")



            svg.append("g")
                .attr("transform", "translate(0," + height + ")")
                .call(d3.axisBottom(x));

            // Add Y axis
            var y = d3.scaleLinear()
                .domain([0, d3.max(dateList2, function(d) { return +d.value; })])
                .range([ height, 0 ]);
                svg.append("g")
                .call(d3.axisLeft(y));

            var y_axis_group = svg.append("g")
                .attr("transform", `translate(${margin.left}, ${margin.top})`)
            
            y_axis_group.append("g")
                .call(y)
                .selectAll("text")
            y_axis_group.append("text")
                .attr("transform", `translate(0, ${height/2}), rotate(-90)`)
                .attr("dy", "-6em")
                .attr("text-anchor", "middle")
                .attr('fill', 'black')
                .attr("font-family", font)
                .attr("font-size", "20px")
                .text("Expedition Count")


            var tooltip = d3.select("#holder3")
                .append("div")
                .style("opacity", 0)
                .attr("class", "tooltip")
                .style("background-color", "white")
                .style("border", "solid")
                .style("border-width", "1px")
                .style("border-radius", "5px")
                .style("padding", "10px")


            let mouseover = function(this: any, event, d) {
                tooltip.style("opacity", 1)
                var elem: HTMLElement = document.getElementById('holder3')!
                var offsets: DOMRect = elem.getBoundingClientRect()  
                d3.select(this)
                    .style("opacity", 1)
                    
                tooltip
                    .style("left", (d3.pointer(event)[0] + offsets.x + 90 + document.documentElement.scrollLeft) + "px")
                    .style("top", (d3.pointer(event)[1] + offsets.y + document.documentElement.scrollTop) + "px")
                    .style("opacity",1)
                    
            }

            
            var mousemove = (event, d) => {
                var elem: HTMLElement = document.getElementById('holder3')!
                var offsets: DOMRect = elem.getBoundingClientRect()    
                console.log(document.body.scrollTop)
                tooltip
                    .html("Year: " + d3.timeFormat("%Y")(d.date) + "<br>Launch Count: " + d.value)
                    .style("left", (d3.pointer(event)[0] + offsets.x + 90 + document.documentElement.scrollLeft) + "px")
                    .style("top", (d3.pointer(event)[1] + offsets.y + document.documentElement.scrollTop) + "px")
            }
    
            var mouseleave = function(this: any, event, d) {
                tooltip
                    .transition()
                    .duration(0)
                    .style("opacity", 0)
                    .on("end", () => {
                        tooltip
                            .style("left",0)
                            .style("top",0)
                    })
                d3.select(this)
                    .style("opacity", 0.3)
            }



            // Add the line
            svg.append("path")
                .datum(dateList2)
                .attr("fill", "none")
                .attr("stroke", "steelblue")
                .attr("stroke-width", 1.5)
                .attr("d", d3.line()
                    .x(function(d) { return x(d.date) })
                    .y(function(d) { return y(d.value) })
                    )
                .style("pointer-events","none")

            // Add dots?
            svg.append('g')
                .selectAll("dot")
                .data(dateList2)
                .enter()
                .append("circle")
                    .attr("cx", function (d) { return x(d.date); } )
                    .attr("cy", function (d) { return y(d.value); } )
                    .attr("r", 6)
                    .style("fill", "#f46d43")
                    .style("opacity", 0.3)
                    .style("stroke", "white")
                .on("mouseover", mouseover )
                .on("mousemove", mousemove )
                .on("mouseleave", mouseleave )
        
    }

    spinner() {
        if (this.state.status === Utils.loading)
            return Utils.renderSpinner();
    }

    render() {
        return (
            <Fragment>
                {this.spinner()}
                <div id="holder3"/>
            </Fragment>
        );
    }
}
  

export default Graph3;