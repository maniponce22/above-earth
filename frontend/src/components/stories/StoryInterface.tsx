export interface StoriesSearchState {
    status:    number;
    count:     number;
    stories:   Story[];
    page_num:  number;
    page_size: number;
    filters:   Filters;
    keywords:  string[];
    sort_by:   string;
    order_by:  string;
}

export interface Filters {
    start_date: Date;
    end_date:   Date;
    publishers: string[];
    agencies:   number[];
    tags:       string[];
}

export interface StoryInstanceState {
    status:             number;
    linked_agencies:    LinkedAgency[];
    linked_expeditions: LinkedExpedition[];
    related_stories:    Story[];
    story:              StoryDetails;
}

export interface LinkedAgency {
    abbrev:                 string;
    country:                string;
    founding_year:          string;
    id:                     number;
    logo_url:               string;
    most_recent_launch:     Date;
    most_recent_news_story: Date;
    name:                   string;
    total_launch_count:     number;
    total_story_count:      number;
    type:                   string;
}

export interface LinkedExpedition {
    agency_abbrev:          string;
    agency_country:         string;
    agency_id:              number;
    agency_name:            string;
    date:                   Date;
    id:                     string;
    image:                  string;
    most_recent_news_story: Date;
    name:                   string;
    pad_name:               string;
    rocket_name:            string;
    total_story_count:      number;
}

export interface Story {
    id:                       string;
    image_url:                string;
    publish_date:             Date;
    related_agency_count:     number;
    related_expedition_count: number;
    site:                     string;
    site_logo_url:            string;
    site_url:                 string;
    summary:                  string;
    title:                    string;
    url:                      string;
}

export interface StoryDetails {
    author:                   string;
    id:                       string;
    image_url:                string;
    publish_date:             Date;
    related_agency_count:     number;
    related_expedition_count: number;
    site:                     string;
    site_logo_url:            string;
    site_twitter:             string;
    site_url:                 string;
    summary:                  string;
    tags:                     string;
    title:                    string;
    url:                      string;
    word_count:               number;
}
