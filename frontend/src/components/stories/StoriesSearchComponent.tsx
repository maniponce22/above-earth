import React, { Component } from 'react';
import axios                from 'axios';

// CSS imports
import 'bootstrap/dist/css/bootstrap.min.css';
import './StoriesSearchComponent.css';

// import interfaces and components
import { StoriesSearchState } from './StoryInterface';
import PaginationComponent    from '../Pagination';
import StoriesFilter          from './StoriesFilterComponent';
import Utils                  from '../utils/Utils';
import GridUtils              from '../utils/GridUtils';

// material UI imports
import Divider from '@material-ui/core/Divider';
import Grow    from '@material-ui/core/Grow';

// react-bootstrap imports
import {Form, Card, Container, Table} from 'react-bootstrap';

class StoriesSearch extends Component<any, StoriesSearchState> {
    state: StoriesSearchState = {
        status:    Utils.starting,
        count:     0,
        stories:   [],
        page_num:  1,
        page_size: 10,

        filters: {
            start_date: new Date("1600-01-01"),
            end_date:   new Date("3000-01-01"),
            publishers: [],
            agencies:   [],
            tags:       []
        },

        keywords: [],
        sort_by:  "publish_date",
        order_by: "desc"
    };



    // ----------
    // AXIOS CALL
    // ----------

    async getStories() {
        // change status to loading
        if (this.state.status !== Utils.starting)
            this.setState({status: Utils.loading})

        // build parameter string
        var parameters = [
            ["limit",              this.state.page_size                          ],
            ["offset",             (this.state.page_num - 1)*this.state.page_size],
            ["publish_date_start", this.state.filters.start_date.toISOString()   ],
            ["publish_date_end",   this.state.filters.end_date.toISOString()     ],
            ["sort_by",            this.state.sort_by                            ],
            ["order_by",           this.state.order_by                           ]]
        this.state.keywords          .map(k => {parameters.push(["keyword", k]);       })
        this.state.filters.publishers.map(p => {parameters.push(["site", p]);          })
        this.state.filters.agencies  .map(a => {parameters.push(["related_agency", a]);})
        this.state.filters.tags      .map(t => {parameters.push(["tag", t]);           })
        var joined_parameters = parameters.map(p => (p[0] + "=" + p[1])).join("&")

        // send axios call
        try {
            await axios.get(
                `${process.env.REACT_APP_API_URL}/news?${joined_parameters}`,
            ).then(response => {
                var data: any = response.data;

                // update stories, count, and status
                if ('error' in data)
                    this.setState({status: Utils.error})
                else if (data.count === 0)
                    this.setState({status: Utils.no_results})
                else
                    this.setState({
                        status:  Utils.success,
                        count:   data.count,
                        stories: data.stories,
                    })
            }).catch(err => {
                this.setState({status: Utils.error});
            });            
        } catch (error) {
            this.setState({status: Utils.error});
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.page_num           === this.state.page_num           &&
            prevState.page_size          === this.state.page_size          &&
            prevState.filters.start_date === this.state.filters.start_date &&
            prevState.filters.end_date   === this.state.filters.end_date   &&
            prevState.sort_by            === this.state.sort_by            &&
            prevState.order_by           === this.state.order_by           &&
            Utils.arraysEqual(prevState.filters.publishers, this.state.filters.publishers) &&
            Utils.arraysEqual(prevState.filters.agencies,   this.state.filters.agencies  ) &&
            Utils.arraysEqual(prevState.filters.tags,       this.state.filters.tags      ) &&
            Utils.arraysEqual(prevState.keywords,           this.state.keywords          ))
            return;

        window.scrollTo(0,0)
        this.getStories();
    }

    componentDidMount() {
        window.scrollTo(0,0)
        this.getStories();
    }



    // ------------
    // RENDER CARDS
    // ------------

    renderCard(story, timeout_val) {
        var keywords = this.state.keywords;
        return (
            <a href={String("/news/").concat(story.id)} className="no-underline">
                <Grow in timeout={timeout_val}>
                    <Card className="shadow rounded-0 m-0 p-0 darken">
                        <Card.Img variant="top" className="rounded-0" style={{objectFit: 'cover', width: '100%', height: '160px'}} src={story.image_url}/>
                        <Card.Body className="mb-0" style={{height: 100}}>
                            <Card.Subtitle className="mb-2 text-muted">
                                Publisher: {Utils.highlightText(story.site, keywords)}
                            </Card.Subtitle>
                            <Card.Title className="mb-0 text text-limit text-dark">
                                {Utils.highlightText(story.title, keywords)}
                            </Card.Title>
                        </Card.Body>
                        <Table className="table m-0 text-center fill-width text-muted">
                            <tbody className="fill-width m-0 p-0">
                                <tr className="border-bottom-0">
                                    <td className="border-left-0">
                                        Expeditions Mentioned: {Utils.highlightText(story.related_expedition_count, keywords)}
                                    </td>
                                    <td className="border-right-0">
                                        Agencies Mentioned: {Utils.highlightText(story.related_agency_count, keywords)}
                                    </td>
                                </tr>
                            </tbody>
                        </Table>
                        <Card.Footer>
                            <small className="text-muted">
                                Published on {Utils.highlightText(Utils.dateToString(story.publish_date), keywords)}
                            </small>
                        </Card.Footer>
                    </Card>
                </Grow>
            </a>
        );
    }

    renderGrid() {
        return GridUtils.renderGrid(
            this.state.stories,
            this.renderCard.bind(this)
        );
    }



    // ------
    // FILTER
    // ------

    handleFilter = (searchProps) => {
        this.setState({
            page_num: 1,
            filters:  searchProps.filters,
            keywords: searchProps.keywords,
            sort_by:  searchProps.sort_by,
            order_by: searchProps.order_by
        })
    }

    

    // ------------
    // RENDER MODES
    // ------------

    renderLoading() {
        return (
            <div>
                {this.renderFilter()}
                {Utils.renderSpinner()}
                {this.renderPagination()}
            </div>
        );
    }

    renderSuccess() {
        return (
            <div>
                {this.renderFilter()}
                {this.renderGrid()}
                {this.renderPagination(true)}
            </div>
        );
    }

    renderNoResults() {
        return(
            <div>
                {this.renderFilter()}
                {Utils.renderMessage("Uh oh! No results were found")}
                {this.renderPagination()}
            </div>
        );
    }

    renderError() {
        return(
            <div>
                {this.renderFilter()}
                {Utils.renderMessage("Error retrieving data")}
                {this.renderPagination()}
            </div>
        );
    }

    renderContent() {
        // render based on status
        switch(this.state.status) {
            case Utils.starting   : return Utils.renderSpinner();
            case Utils.loading    : return this.renderLoading();
            case Utils.success    : return this.renderSuccess();
            case Utils.no_results : return this.renderNoResults();
            default               : return this.renderError();
        }
    }



    // ------
    // RENDER
    // ------

    renderFilter() {
        return (
            <div>
                <StoriesFilter onChange={this.handleFilter}/>
                <Divider className="mb-3"/>
            </div>
        );
    }

    renderPagination(visible=false) {
        return (
            <div className="pagination mb-3 mt-3" style={{display: (visible ? "" : "none")}}>
                <Form.Label className="my-auto mr-2">
                    Stories per page:
                </Form.Label>

                <Form.Control 
                    className="my-auto" 
                    as="select" 
                    style={{width: 100}} 
                    onChange={(event) => 
                        this.setState({
                            page_num: 1, 
                            page_size: parseInt(event.target.value)
                        })
                    }
                    custom>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </Form.Control>

                <Form.Label className="my-auto mx-4">
                    {((this.state.page_num - 1) * this.state.page_size) + 1}-
                    {Math.min(
                        this.state.count, 
                        this.state.page_num * this.state.page_size
                    )} of {this.state.count}
                </Form.Label>

                <PaginationComponent
                    total={this.state.count}
                    itemsPerPage={this.state.page_size}
                    currentPage={this.state.page_num}
                    onPageChange={page => this.setState({page_num: page})}
                />
            </div>
        );
    }

    render() {
        return (
            <Container className="mt-3" style={{paddingLeft: "5%", paddingRight: "5%"}} fluid>
                <h1>News</h1>
                {this.renderContent()}
            </Container>
        );
    }
}

export default StoriesSearch;
