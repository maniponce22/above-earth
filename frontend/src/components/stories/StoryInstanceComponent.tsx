import React, { Component }   from 'react';
import {RouteComponentProps } from 'react-router-dom'
import axios                  from 'axios';

// CSS imports
import 'bootstrap/dist/css/bootstrap.min.css'
import './StoryInstanceComponent.css'

// import interfaces and components
import { StoryInstanceState } from './StoryInterface';
import Utils                  from '../utils/Utils';

// material imports
import CreateIcon  from '@material-ui/icons/Create';
import TwitterIcon from '@material-ui/icons/Twitter';
import TimerIcon   from '@material-ui/icons/Timer';
import Divider     from '@material-ui/core/Divider';

// Twitter feed import
import {TwitterTimelineEmbed} from 'react-twitter-embed';

// react-bootstrap imports
import {Accordion, Table, ListGroup, Card, Button, Image, Row, Col, Container, CardColumns} from 'react-bootstrap'

class StoryInstance extends Component<RouteComponentProps<{id}>, StoryInstanceState> {
    state: StoryInstanceState = {
        status:             Utils.starting,
        
        linked_agencies:    [],
        linked_expeditions: [],
        related_stories:    [],

        story: {
            id:                       "",
            image_url:                "",
            publish_date:             new Date(),
            related_agency_count:     0,
            related_expedition_count: 0,
            site:                     "",
            site_logo_url:            "",
            site_url:                 "",
            summary:                  "",
            title:                    "",
            url:                      "",
            tags:                     "",
            author:                   "",
            word_count:               0,
            site_twitter:             ""
        }
    };



    // ----------
    // AXIOS CALL
    // ----------

    async getStory() {
        // get id
        var id = this.props.match.params['newsId'];
        
        // send axios call
        try {
            await axios.get(
                `${process.env.REACT_APP_API_URL}/news/${id}`,
            ).then(response => {
                var data = response.data;

                // update state
                if ('error' in data)
                    this.setState({status: Utils.error});
                else
                    this.setState({
                        status:             Utils.success,
                        linked_agencies:    data.linked_agencies,
                        linked_expeditions: data.linked_expeditions,
                        related_stories:    data.related_stories,
                        story:              data.story
                    });
            }).catch((err) => {
                this.setState({status: Utils.error});
            });
            
        } catch (error) {
            this.setState({status: Utils.error});
        }
    }

    componentDidMount() {
        window.scrollTo(0,0);
        this.getStory();
    }


    
    // -----
    // LINKS
    // -----

    openArticle() {
        window.open(this.state.story.url);
    }

    openTwitter() {
        window.open("https://twitter.com/@" + this.state.story.site_twitter);
    }

    openPubSite() {
        window.open(String("https://").concat(this.state.story.site_url));
    }



    // --------------
    // EXPEDITION TAB
    // --------------
    
    expeditionEntry(expedition) {
        return (
            <Card className="darken rounded-0 shadow-sm">
                <a href={String("/expeditions/").concat(expedition.id)} className="no-underline">
                    <Card.Img className="rounded-0" variant="top" style={{objectFit: 'cover', height: '200px'}} src={expedition.image}/>
                    <Card.Body className="mb-0 mt-0">
                        <Card.Title className="mb-0 mt-0 text-dark">
                            {expedition.name}
                        </Card.Title>
                    </Card.Body>
                </a>
                <ListGroup className="text-muted" variant="flush">
                    <ListGroup.Item className="darken-child" variant="light">Date: {Utils.dateToString(expedition.date)}</ListGroup.Item>
                    <ListGroup.Item className="darken-child extra-darken" variant="light" action href={String("/agencies/").concat(expedition.agency_id)}>Agency: {expedition.agency_name}</ListGroup.Item>
                    <ListGroup.Item className="darken-child" variant="light">Rocket: {expedition.rocket_name}</ListGroup.Item>
                </ListGroup>
            </Card>
        );
    }

    expeditionTab() {
        if (this.state.linked_expeditions.length === 0)
            return (
                <Accordion key="0" defaultActiveKey="0" className="mb-3">
                    <Card className="rounded-0 shadow">
                        <Accordion.Toggle as={Card.Header} eventKey="0">
                            Expeditions Mentioned ({this.state.linked_expeditions.length})
                        </Accordion.Toggle>
                    </Card>
                </Accordion>
            );
        else
            return (
                <Accordion key="0" defaultActiveKey="0" className="mb-3">
                    <Card className="rounded-0 shadow">
                        <Accordion.Toggle style={{cursor: 'pointer'}} as={Card.Header} eventKey="0">
                            Expeditions Mentioned ({this.state.linked_expeditions.length})
                        </Accordion.Toggle>
                        <Accordion.Collapse eventKey="0" className="p-4">
                            <CardColumns style={{width: "100"}}>
                                {this.state.linked_expeditions.map(e => this.expeditionEntry(e))}
                            </CardColumns>
                        </Accordion.Collapse>
                    </Card>
                </Accordion>
            );   
    }



    // ----------
    // AGENCY TAB
    // ----------

    agencyEntry(agency) {
        return (
            <a href={String("/agencies/").concat(agency.id)} className="no-underline">
                <Card className="darken rounded-0 shadow-sm">
                    <Card.Header className="text-dark">
                        {agency.name}
                    </Card.Header>
                    <Card.Img className="p-3 mb-1 mt-1 rounded-0" variant="top" style={{objectFit: 'cover'}} src={agency.logo_url}/>
                    <ListGroup className="text-muted darken-child" variant="flush">
                        <ListGroup.Item className="darken-child">Country: {agency.country}</ListGroup.Item>
                        <ListGroup.Item className="darken-child">Type: {agency.type}</ListGroup.Item>
                    </ListGroup>
                </Card>
            </a>
        );
    }

    agencyTab() {
        if (this.state.linked_agencies.length === 0)
            return (
                <Accordion key="0" defaultActiveKey="0" className="mb-3">
                    <Card className="rounded-0 shadow">
                        <Accordion.Toggle as={Card.Header} eventKey="0">
                            Agencies Mentioned ({this.state.linked_agencies.length})
                        </Accordion.Toggle>
                    </Card>
                </Accordion>
            );
        else
            return (
                <Accordion key="0" defaultActiveKey="0" className="mb-3">
                    <Card className="rounded-0 shadow">
                        <Accordion.Toggle style={{cursor: 'pointer'}} as={Card.Header} eventKey="0">
                            Agencies Mentioned ({this.state.linked_agencies.length})
                        </Accordion.Toggle>
                        <Accordion.Collapse eventKey="0" className="p-4">
                            <CardColumns style={{width: "100%"}}>
                                {this.state.linked_agencies.map(a => this.agencyEntry(a))}
                            </CardColumns>
                        </Accordion.Collapse>
                    </Card>
                </Accordion>
            );

    }



    // --------
    // INFO TAB
    // --------

    infoTab() {
        return (
            <Accordion key="0" defaultActiveKey="0" className="mb-3">
                <Card className="rounded-0 shadow">
                    <Accordion.Toggle style={{cursor: 'pointer'}} as={Card.Header} eventKey="0">
                        Story Information
                    </Accordion.Toggle>
                    <Accordion.Collapse eventKey="0">
                        <Card.Body>
                            <Card.Text className="mb-0">
                                <CreateIcon fontSize="small"/>
                                <small className="my-auto">
                                    <span className="text-muted"> written by </span>{this.state.story.author}
                                </small>&emsp;&emsp;
                                <TimerIcon fontSize="small" style={{color: this.lengthDetails()[1]}}/>
                                <small className="my-auto">
                                    <span> {this.lengthDetails()[0]} article length </span><span className="text-muted">({this.state.story.word_count} words)</span>
                                </small>&emsp;&emsp;
                                <TwitterIcon fontSize="small" style={{color: "#1DA1F2", cursor: "pointer"}} onClick={this.openTwitter.bind(this)}/>
                                <small className="my-auto twitter-link" onClick={this.openTwitter.bind(this)}> @{this.state.story.site_twitter}</small>
                            </Card.Text>
                            <Divider className="my-2"/>
                            <Card.Text className="mt-2">
                                <span className="font-weight-bold">Summary: </span>{this.state.story.summary}
                            </Card.Text>
                            <Card.Text>
                                <small className="text-muted my-auto mx-1">TAGS:</small>
                                {this.state.story.tags.split(", ").map(t => this.tagBox(t))}
                            </Card.Text>                                                
                        </Card.Body>
                    </Accordion.Collapse>
                </Card>
            </Accordion>
        );
    }

    lengthDetails() {
        // return short, long, or medium depending on the word count
        var wc = this.state.story.word_count;
        if      (wc < 600 ) return ["short" , "#5cb85c"];
        else if (wc > 1300) return ["long"  , "#d9534f"];
        else                return ["medium", "#f0ad4e"];
    }

    tagBox = (t) => {
        // skip if empty string
        if (t === "")
            return;

        return (
            <Button variant="outline-secondary" className="rounded-0 m-1 py-0 px-1" style={{cursor: "default"}}>
                <small className="m-0 p-0">{t.trim().toUpperCase()}</small>
            </Button>
        );
    }



    // ---------------
    // RELATED STORIES
    // ---------------

    relatedStory(story) {
        return (
            <tr className="fill-width m-0 p-0">
                <td className="pr-0 pl-0">
                    <a href={String("/news/").concat(story.id)}>
                        <Image className="shadow" style={{objectFit: 'cover', width: '140px', height: '105px'}} src={story.image_url}/>
                    </a>
                </td>
                <td className="fill-width">
                    <a href={String("/news/").concat(story.id)} style={{cursor: 'pointer'}} className="no-underline">
                        <h6 className="m-0 pt-1 pl-0 pr-0 pb-0 text-limit-4 text-dark">
                            {story.title}
                        </h6>
                        <p className="m-0 p-0 text-muted small-text">
                            {story.site} | {Utils.dateToString(story.publish_date)}
                        </p>
                    </a>
                </td>
            </tr>
        );
    }



    // ------
    // RENDER
    // ------

    renderSuccess() {
        return (
            <div>
                <Row className="mt-0 mb-0">
                    <h2 className="ml-2 hover-underline" style={{cursor: 'pointer'}} onClick={this.openArticle.bind(this)}>{this.state.story.title}</h2>
                </Row>
                <Row className="ml-2 mb-3 mt-0" style={{height: 40}}>
                    <a href="#" onClick={this.openPubSite.bind(this)} style={{height: "100%"}}>
                        <Image src={this.state.story.site_logo_url} roundedCircle style={{height: "100%"}}/> 
                    </a>
                    <p style={{margin: "auto"}} className="ml-1">&emsp;
                        <span className="hover-underline" style={{cursor:'pointer'}} onClick={this.openPubSite.bind(this)}>{this.state.story.site}</span>&emsp;
                        <span className="text-muted">{Utils.dateToString(this.state.story.publish_date)}</span>
                    </p>
                </Row>
                <Row className="mt-1 mb-5 horizontalLine">
                    <Col sm="8" className="mt-0 mt-4 pr-5 verticalLine">
                        <Row className="mb-0">
                            <Image style={{width: "100%"}} className="shadow" src={this.state.story.image_url} fluid/>
                        </Row>
                        <Row className="mt-0 mb-4">
                            <Button className="mt-4 mb-4 rounded-0 shadow" variant="outline-dark" onClick={this.openArticle.bind(this)} block>
                                Go To Article
                            </Button>
                            <div className="fill-width">
                                {this.infoTab()}
                                {this.expeditionTab()}
                                {this.agencyTab()}
                            </div>
                        </Row>
                    </Col>
                    <Col sm="4" className="pl-4">
                        <div className="mt-4 shadow">
                            <TwitterTimelineEmbed
                                sourceType="profile"
                                screenName={this.state.story.site_twitter}
                                options={{height: 600}}
                                noFooter noScrollbar noBorders/>
                        </div>
                        <h5 className="mt-4 ml-2" style={{textAlign: "left"}}>
                            Related Stories
                        </h5>
                        <Table className="fill-width">
                            <tbody className="fill-width m-0 p-0">
                                {this.state.related_stories.map(s => (this.relatedStory(s)))}
                            </tbody>
                        </Table>
                        
                    </Col>
                </Row>
            </div>
        );
    }

    renderContent() {
        // switch, handle the status
        switch(this.state.status) {
            case Utils.starting : return Utils.renderSpinner();
            case Utils.success  : return this.renderSuccess();
            case Utils.error    : return Utils.renderMessage("404 Not Found");
        }
    }

    render() {
        return (
            <Container className="mt-5" style={{paddingLeft: "15%", paddingRight: "15%"}} fluid>
                {this.renderContent()}
            </Container>
        );
    }
}

export default StoryInstance;
