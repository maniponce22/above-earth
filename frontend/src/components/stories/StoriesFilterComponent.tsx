import React, { Component } from 'react';
import axios                from 'axios';

// CSS imports
import 'bootstrap/dist/css/bootstrap.min.css'
import './StoriesSearchComponent.css'

// import Compenents and interfaces
import { Filters } from './StoryInterface';
import FilterUtils from '../utils/FilterUtils'
import ChipDialog  from '../utils/ChipDialogComponent'
import DateDialog  from '../utils/DateDialogComponent'
import Utils       from '../utils/Utils'

// material-ui imports
import FormControl from '@material-ui/core/FormControl';

// react-bootstrap imports
import { Form, Accordion, Row, Col, InputGroup } from 'react-bootstrap';

// define the state
type StoriesFilterState = {
    date_key:         number;
    custom_publisher: boolean;
    custom_agency:    boolean;
    custom_tag:       boolean;

    filter_data: {
        publishers: string[];
        agencies:   string[];
        tags:       string[];
    };

    filters:  Filters;
    keywords: string[];
    sort_by:  string;
    order_by: string;
};

// CONSTANTS
const default_start_date = new Date("1600-01-01");
const default_end_date = new Date(
    String(new Date().getFullYear()) + "-" + 
    String(new Date().getMonth() + 1).padStart(2, '0') + "-" + 
    String(new Date().getDate()).padStart(2, '0'));

const sort_options = [
    { name: "Title",                 value: "title"                    },
    { name: "Hits",                  value: "hits"                     },
    { name: "Publisher",             value: "site"                     },
    { name: "Author",                value: "author"                   },
    { name: "Publish Date",          value: "publish_date"             },
    { name: "Article Length",        value: "word_count"               },
    { name: "Agencies Mentioned",    value: "related_agency_count"     },
    { name: "Expeditions Mentioned", value: "related_expedition_count" }
];

const filter_list_limit = 5;

class StoriesFilter extends Component<any, StoriesFilterState> {
    state: StoriesFilterState = {
        date_key:         0,
        custom_publisher: false,
        custom_agency:    false,
        custom_tag:       false,

        filter_data: {
            publishers: [],
            agencies:   [],
            tags:       [],
        },

        filters: {
            start_date: default_start_date,
            end_date:   default_end_date,
            publishers: [],
            agencies:   [],
            tags:       []
        },

        keywords: [],
        sort_by:  "publish_date",
        order_by: "desc",
    };

    // map for getting agency ids
    agency_map = {};

    // refs
    searchBarRef;
    dateDialogRef;
    publisherDialogRef;
    agencyDialogRef;
    tagDialogRef;
    
    constructor(props: any) {
        super(props);

        this.searchBarRef       = React.createRef();
        this.dateDialogRef      = React.createRef();
        this.publisherDialogRef = React.createRef();
        this.agencyDialogRef    = React.createRef();
        this.tagDialogRef       = React.createRef();
    }

    

    // -----------
    // AXIOS CALLS
    // -----------

    async getNewsData() {
        try {
            // getting news info
            await axios.get(
                `${process.env.REACT_APP_API_URL}/overview/news`,
            ).then(response => {
                var data = response.data;

                // count each publisher and tag
                var publishers = {}
                var tags = {}
                for (var s of data.stories) {
                    // count publishers
                    if (!(s.site in publishers))
                        publishers[s.site] = 0;
                    publishers[s.site] += 1;

                    // count tags
                    for (var t of s.tags.split(", ")) {
                        var tag = t.trim().toUpperCase()
                        if (tag === "")
                            continue
                        
                        if (!(tag in tags))
                            tags[tag] = 0;
                        tags[tag] += 1;
                    }
                }

                // Create and sort publisher array
                var publishers_arr = Object.keys(publishers).map(key => ({publisher: key, count: publishers[key]}));
                publishers_arr.sort((a, b) => (b.count - a.count));

                // Create and sort tag array
                var tags_arr = Object.keys(tags).map(key => ({tag: key, count: tags[key]}));
                tags_arr.sort((a, b) => (b.count - a.count));

                // update state
                this.setState({filter_data: {
                    ...this.state.filter_data,
                    publishers: publishers_arr.map(p => (p.publisher)),
                    tags:       tags_arr.map(e => (e.tag))}})
            }).catch(err => {})
        } catch (error) {}
    }

    async getAgencyData() {
        try {
            // getting agency info
            await axios.get(
                `${process.env.REACT_APP_API_URL}/overview/agencies`,
            ).then(response => {
                var data = response.data;

                // Create and sort array
                var agency_arr = data.agencies
                    .filter(a => (a.total_story_count > 0))
                    .map(a => ({name: a.name, id: a.id, count: a.total_story_count}));
                agency_arr.sort((a, b) => (b.count - a.count));

                // create agency_map
                agency_arr.map(a => {this.agency_map[a.name] = a.id;})

                // update state
                this.setState({filter_data: {
                    ...this.state.filter_data,
                    agencies: agency_arr.map(a => (a.name))
                }})
            }).catch(err => {});
        } catch (error) {}
    }

    componentDidMount() {
        this.getNewsData();
        this.getAgencyData();
    }

    componentDidUpdate(prevProps, prevState) {
        // send an onChange update if the filters have not changed
        if (prevState.order_by           === this.state.order_by           &&
            prevState.sort_by            === this.state.sort_by            &&
            prevState.filters.start_date === this.state.filters.start_date &&
            prevState.filters.end_date   === this.state.filters.end_date   &&
            Utils.arraysEqual(prevState.keywords,           this.state.keywords          ) &&
            Utils.arraysEqual(prevState.filters.publishers, this.state.filters.publishers) &&
            Utils.arraysEqual(prevState.filters.agencies,   this.state.filters.agencies  ) &&
            Utils.arraysEqual(prevState.filters.tags,       this.state.filters.tags      ))
            return;

        this.props.onChange(this.state);
    }



    // ------
    // SEARCH
    // ------

    handleSearchGo = () => {
        // prevent tsx warnings
        if (this.searchBarRef.current === null)
            return;

        // build keyword array and update state
        var kws = this.searchBarRef.current.value.toString().split(" ").filter(kw => (kw !== "" && kw !== undefined))
        this.setState({keywords: kws})
    }



    // ------------
    // Publish Date
    // ------------

    handleDateClick = (key) => {
        if (key === this.state.date_key) {
            // if getting rid of the filter...
            this.setState({
                date_key: 0,
                filters: {
                    ...this.state.filters,
                    start_date: default_start_date,
                    end_date: default_end_date
                }
            });
        } else {
            // if adding a filter...
            if (key === 3) {
                // Click CUSTOM, open dialog
                this.dateDialogRef.current.open();
            } else {
                // Click MONTH or YEAR
                this.setState({
                    date_key: key,
                    filters: {
                        ...this.state.filters,
                        start_date: (key === 1) ? FilterUtils.month_date : FilterUtils.year_date,
                        end_date: default_end_date
                    }
                })
            }
        }
    };



    // ---------
    // Publisher
    // ---------

    handleSetPublisher = (name) => {
        // create new pub arr
        var new_pub = [name]
        if (Utils.arraysEqual(this.state.filters.publishers, [name]) && !this.state.custom_publisher)
            new_pub = [];

        // update state
        this.setState({
            filters: { ...this.state.filters, publishers: new_pub }, 
            custom_publisher: false
        });
    }

    handlePublisherMoreClick = () => {
        if (this.state.custom_publisher) {
            // removing filter, deselect all
            this.setState({
                filters: { ...this.state.filters, publishers: [] },
                custom_publisher: false
            })
        } else {
            // open the dialog
            this.publisherDialogRef.current.open();
        }
    }



    // ------
    // Agency
    // ------

    handleSetAgency = (name) => {
        // create new agency arr
        var new_agencies = [this.agency_map[name]]
        if (Utils.arraysEqual(this.state.filters.agencies, new_agencies) && !this.state.custom_agency)
            new_agencies = [];

        // update state
        this.setState({
            filters: { ...this.state.filters, agencies: new_agencies }, 
            custom_agency: false
        });
    }

    handleAgencyMoreClick = () => {
        if (this.state.custom_agency) {
            // removing filter, deselect all
            this.setState({
                filters: { ...this.state.filters, agencies: [] },
                custom_publisher: false
            })
        } else {
            // open the dialog
            this.agencyDialogRef.current.open();
        }
    }

    

    // ---
    // Tag
    // ---

    handleSetTag = (t) => {
        // create new tag arr
        var new_tags = [t]
        if (Utils.arraysEqual(this.state.filters.tags, [t]) && !this.state.custom_tag)
            new_tags = [];

        // update state
        this.setState({
            filters: { ...this.state.filters, tags: new_tags },
            custom_tag: false
        });
    }

    handleTagMoreClick = () => {
        if (this.state.custom_tag)
            // removing filter, deselect all
            this.setState({
                filters: { ...this.state.filters, tags: [] },
                custom_publisher: false
            })
        else
            // open the dialog
            this.tagDialogRef.current.open();
    }



    // ------
    // Render
    // ------

    render() {
        return (
            <Accordion key="0" className="mb-4">
                <Form className="mx-auto mb-0">
                    <Form.Row>
                        <Col className="pr-0" style={{marginRight: "8px"}}>
                            <InputGroup>
                                {FilterUtils.filterButton(0)}
                                {FilterUtils.searchBar(
                                    "Search News",
                                    this.handleSearchGo.bind(this),
                                    "calc(100% - 136px)",
                                    this.searchBarRef
                                )}
                            </InputGroup>
                        </Col>
                        <Col className="pl-0" style={{marginLeft: "8px"}}>
                            <InputGroup>
                                <FormControl variant="outlined" style={{marginRight: "10px", width: "calc(100% - 65px)"}}>
                                    {FilterUtils.sortSelector(
                                        sort_options,
                                        "publish_date",
                                        (e) => {this.setState({sort_by: e.target.value});})}
                                </FormControl>
                                {FilterUtils.orderDirectionButton(
                                    this.state.order_by === "desc",
                                    () => {this.setState({order_by: this.state.order_by === "asc" ? "desc" : "asc"})})}
                            </InputGroup>
                        </Col>
                    </Form.Row>
                </Form>
                <Accordion.Collapse eventKey="0">
                    <Row className="mx-auto mt-3">
                        <Col sm="2">
                            <Row>{FilterUtils.filterColumnHeader("Publish Date")}</Row>
                            <Row>{FilterUtils.filterColumnUnderline()}</Row>
                            <Row>
                                {FilterUtils.filterEntry(
                                    "This month",
                                    this.state.date_key === 1,
                                    this.handleDateClick.bind(this), 1)}
                            </Row>
                            <Row>
                                {FilterUtils.filterEntry(
                                    "This year", 
                                    this.state.date_key === 2, 
                                    this.handleDateClick.bind(this), 2)}
                            </Row>
                            <Row>
                                {FilterUtils.filterEntry(
                                    "Custom", 
                                    this.state.date_key === 3, 
                                    this.handleDateClick.bind(this), 3)}
                                <DateDialog 
                                    ref={this.dateDialogRef}
                                    title="Custom Publish Date Range"
                                    onChange={dates => {
                                        this.setState({
                                            date_key: 3,
                                            filters: {
                                                ...this.state.filters,
                                                start_date: dates[0],
                                                end_date: dates[1]
                                            }
                                        })
                                    }}/>
                            </Row>
                        </Col>
                        <Col sm="3">
                            <Row>{FilterUtils.filterColumnHeader("Publisher")}</Row>
                            <Row>{FilterUtils.filterColumnUnderline()}</Row>
                            {this.state.filter_data.publishers.slice(0, filter_list_limit).map(p => 
                                <Row>
                                    {FilterUtils.filterEntry(
                                        p, 
                                        (Utils.arraysEqual(this.state.filters.publishers, [p]) 
                                            && !this.state.custom_publisher),
                                        this.handleSetPublisher.bind(this), p)}
                                </Row>)}
                            <Row>
                                {FilterUtils.filterEntry(
                                    "More", 
                                    this.state.custom_publisher, 
                                    this.handlePublisherMoreClick.bind(this))}
                                <ChipDialog 
                                    ref={this.publisherDialogRef}
                                    title="Select Publishers"
                                    entries={this.state.filter_data.publishers} 
                                    onChange={(selection) => {
                                        this.setState({
                                            filters: { ...this.state.filters, publishers: selection },
                                            custom_publisher: true
                                        })
                                    }}/>
                            </Row>
                        </Col>
                        <Col sm="4">
                            <Row>{FilterUtils.filterColumnHeader("Mentioned Agency")}</Row>
                            <Row>{FilterUtils.filterColumnUnderline()}</Row>
                            {this.state.filter_data.agencies.slice(0, filter_list_limit).map(a => 
                                <Row>
                                    {FilterUtils.filterEntry(
                                        a, 
                                        (Utils.arraysEqual(this.state.filters.agencies, [this.agency_map[a]]) 
                                            && !this.state.custom_agency),
                                        this.handleSetAgency.bind(this), a)}
                                </Row>)}
                            <Row>
                                {FilterUtils.filterEntry(
                                    "More",
                                    this.state.custom_agency,
                                    this.handleAgencyMoreClick.bind(this))}
                                <ChipDialog 
                                    ref={this.agencyDialogRef}
                                    title="Select Agencies"
                                    entries={this.state.filter_data.agencies} 
                                    onChange={(selection) => {
                                        this.setState({
                                            filters: {
                                                ...this.state.filters,
                                                agencies: selection.map(name => (this.agency_map[name]))
                                            },
                                            custom_agency: true
                                        })
                                    }}/>
                            </Row>
                        </Col>
                        <Col sm="3">
                            <Row>{FilterUtils.filterColumnHeader("Tags")}</Row>
                            <Row>{FilterUtils.filterColumnUnderline()}</Row>
                            {this.state.filter_data.tags.slice(0, filter_list_limit).map(t => 
                                <Row>
                                    {FilterUtils.filterEntry(
                                        t, 
                                        (Utils.arraysEqual(this.state.filters.tags, [t]) && 
                                            !this.state.custom_tag),
                                        this.handleSetTag.bind(this), t)}
                                </Row>)}
                            <Row>
                                {FilterUtils.filterEntry(
                                    "More", 
                                    this.state.custom_tag,
                                    this.handleTagMoreClick.bind(this))}
                                <ChipDialog 
                                    ref={this.tagDialogRef}
                                    title="Select Tags"
                                    entries={this.state.filter_data.tags} 
                                    onChange={(selection) => {
                                        this.setState({
                                            filters: { ...this.state.filters, tags: selection },
                                            custom_tag: true
                                        })
                                    }}/>
                            </Row>
                        </Col>
                    </Row>
                </Accordion.Collapse>
            </Accordion>
        );
    }
}

export default StoriesFilter;
