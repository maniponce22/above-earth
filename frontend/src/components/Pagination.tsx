import React, { useEffect, useState, useMemo } from "react";

// react-bootstrap imports
import Pagination from "react-bootstrap/Pagination";

const PaginationComponent = ({
    total = 0,
    itemsPerPage = 10,
    currentPage = 1,
    onPageChange}) => {
        
    const [totalPages, setTotalPages] = useState(0);

    useEffect(() => {
        if (total > 0 && itemsPerPage > 0)
            setTotalPages(Math.ceil(total / itemsPerPage));
    }, [total, itemsPerPage]);

    const paginationItems = useMemo(() => {
        const pages : JSX.Element[] = [];
        // small amount, display all pages
        if (totalPages <= 7) {
            for (let i = 1; i <= totalPages; ++i) {
                pages.push(
                <Pagination.Item
                    key={i}
                    active={i === currentPage}
                    onClick={() => onPageChange(i)}
                >
                    {i}
                </Pagination.Item>                    
                );
            }      
        }

        // starting window to display
        else if (currentPage < 5) {
            for (let i = 1; i <= 5; i++) {
                pages.push(
                <Pagination.Item
                    key={i}
                    active={i === currentPage}
                    onClick={() => onPageChange(i)}
                >
                    {i}
                </Pagination.Item>                    
                );
            }
            pages.push(<Pagination.Ellipsis key={-3} disabled/>);
            pages.push(
                <Pagination.Item
                    key={totalPages}
                    active={false}
                    onClick={() => onPageChange(totalPages)}
                >
                    {totalPages}
                </Pagination.Item>
            );
        }

        // end window to display
        else if (currentPage >= totalPages - 2) {
            pages.push(
                <Pagination.Item
                    key={1}
                    active={false}
                    onClick={() => onPageChange(1)}
                >
                    {1}
                </Pagination.Item>
            );            
            pages.push(<Pagination.Ellipsis key={-3} disabled/>);
            for (let i = totalPages - 4; i <= totalPages; i++) {
                pages.push(
                    <Pagination.Item
                        key={i}
                        active={i === currentPage}
                        onClick={() => onPageChange(i)}
                    >
                        {i}
                    </Pagination.Item>                    
                );
            }
        }

        // truncate both sides, intermediate window
        else {
            pages.push(
                <Pagination.Item
                    key={1}
                    active={false}
                    onClick={() => onPageChange(1)}
                >
                    {1}
                </Pagination.Item>
            );  
            pages.push(<Pagination.Ellipsis key={-3} disabled/>);
            for(let i = currentPage - 1; i <= currentPage + 1; i++) {
                pages.push(
                    <Pagination.Item
                        key={i}
                        active={i === currentPage}
                        onClick={() => onPageChange(i)}
                    >
                        {i}
                    </Pagination.Item>                    
                );      
            }
            pages.push(<Pagination.Ellipsis key={-4} disabled/>);
            pages.push(
                <Pagination.Item
                    key={totalPages}
                    active={false}
                    onClick={() => onPageChange(totalPages)}
                >
                    {totalPages}
                </Pagination.Item>
            );
        }

        return pages;
    }, [totalPages, currentPage]);

    if (totalPages === 0) return null;

    return (
        <Pagination className="my-auto">
            <Pagination.Prev
                key={-1}
                onClick={() => onPageChange(currentPage - 1)}
                disabled={currentPage === 1}
            />
            {paginationItems}
            <Pagination.Next
                key={-2}
                onClick={() => onPageChange(currentPage + 1)}
                disabled={currentPage === totalPages}
            />
        </Pagination>
    );
};

export default PaginationComponent;
