import React, { Component } from 'react';
import { LinkContainer }    from 'react-router-bootstrap';

// import images
import Logo from './splash/SplashScreenImages/spaceNav.png'

// react-bootstrap imports
import Navbar                             from 'react-bootstrap/Navbar';
import { Nav, Form, FormControl, Button } from 'react-bootstrap';

// define the state
type HeaderState = {
    searchVal: string;
};

class Header extends Component<any, HeaderState> {
    state: HeaderState = {
        searchVal: ""
    };

    getSearchURL() {
        var words = this.state.searchVal
        return String("/search/").concat(words)
    }

    handleSearchChange(event) {
        this.state.searchVal = event.target.value;
    }

    handleSearchGo = () => {
        window.location.href = this.getSearchURL();
    }

    handleSearchKeyPress(event) {
        if (event.charCode === 13) {
            event.preventDefault();
            this.handleSearchGo();
        }
    }

    render() { 
        return (
            <Navbar className="nav-style" variant="dark">
                <LinkContainer to="/">
                    <Navbar.Brand href="#">
                        <img
                            src={Logo}
                            width="30"
                            height="30"
                            padding-right="100"
                            alt="Above Earth Rocket" />
                        <b>ABOVE EARTH</b>
                    </Navbar.Brand>
                </LinkContainer>
                <Nav className="mr-auto">
                    <LinkContainer to="/about">
                        <Nav.Link>About</Nav.Link>
                    </LinkContainer>
                    <LinkContainer to="/agencies">
                        <Nav.Link>Agencies</Nav.Link>
                    </LinkContainer>
                    <LinkContainer to="/expeditions">
                        <Nav.Link>Expeditions</Nav.Link>
                    </LinkContainer>
                    <LinkContainer to="/news">
                        <Nav.Link>News Stories</Nav.Link>
                    </LinkContainer>
                    <LinkContainer to="/our_visualizations">
                        <Nav.Link>Our Visualizations</Nav.Link>
                    </LinkContainer>
                    <LinkContainer to="/provider_visualizations">
                        <Nav.Link>Provider Visualizations</Nav.Link>
                    </LinkContainer>
                </Nav>
                <Form inline >
                    <FormControl type="text" placeholder="Explore" className="mr-sm-2 rounded-0" name="data" onChange={this.handleSearchChange.bind(this)} onKeyPress={this.handleSearchKeyPress.bind(this)} />
                    <Button variant="outline-light" className='rounded-0' onClick={this.handleSearchGo}>Go</Button>
                </Form>
            </Navbar>
        );
    }
}

export default Header;