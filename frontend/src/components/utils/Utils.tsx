import spinner from '../splash/SplashScreenImages/rocket_loading.gif'
// import {Spinner} from 'react-bootstrap'
import dotsSpinner from '../splash/SplashScreenImages/dots_loading.gif'

export default class Utils {
    // CONSTANTS
    static starting   = 0;
    static loading    = 1;
    static success    = 2;
    static no_results = 3;
    static error      = 4; 

    static spinnerMargin = 800;

    // returns true if the arrays are equal
    static arraysEqual(a, b) {
        if (a === b) return true;
        if (a == null || b == null) return false;
        if (a.length !== b.length) return false;
      
        for (var i = 0; i < a.length; ++i)
            if (a[i] !== b[i]) return false;
        return true;
    }
    
    // checks if a string is in arr
    static stringInArray(arr, val) {
        for (var a of arr)
            if (a.toLowerCase() === val) return true;
        return false;
    }

    // converts a date object into a string mm/dd/yyyy
    static dateToString(date) {
        const thisDate = new Date(date)
        return (
            (thisDate.getMonth() + 1) + "/" + 
            thisDate.getDate() + "/" + 
            thisDate.getFullYear()
        );
    }

    static highlightText(t, keywords) {
        // make sure text is valid
        if (t === null) return;

        var text = t.toString()

        // trivial if no keywords
        if (keywords.length === 0) return text;

        // build regex and split
        var highlight_words = keywords.map(k => ("(" + k + ")"))
        const parts = text.split(new RegExp(highlight_words.join('|'), 'gi'));

        return (
            <span>
                {parts.map(part => {
                    if (part === undefined) return;

                    // highlight if in keywords
                    if (Utils.stringInArray(keywords, part.toLowerCase()))
                        return (
                            <span style={{backgroundColor: "#FFFF00"}}>{part}</span>
                        );

                    // just return the part otherwise
                    return (<span>{part}</span>);
                })}
            </span>
        );
    }

    static renderSpinner() {
        return (
            <div style={{marginBottom: this.spinnerMargin}}>
                <img className="mx-auto d-block mt-0 mb-5" src={spinner}/>
            </div>
        );
    }

    static renderSpinnerNoPad() {
        return (
            <div >
                <img className="mx-auto d-block mt-0 mb-5" src={spinner}/>
            </div>
        );
    }

    static renderMediumSpinner() {
        return (
                <img className="" width="45" src={dotsSpinner}/>
        );
    }

    static renderSmallSpinner() {
        return (
                <img className="mx-auto d-block mt-0" width="40" src={dotsSpinner}/>
        );
    }

    static renderMessage(content) {
        return (
            <h4 style={{marginBottom: this.spinnerMargin}}>
                {content}
            </h4>
        );
    }

    static groupByN = (n, data) => {
        let result: any[] = [];
        for (let i = 0; i < data.length; i += n) 
            result.push(data.slice(i, i + n));
        if (result.length > 0)
            while (result[result.length - 1].length !== n)
                result[result.length - 1].push(null)
        return result;
    };
}