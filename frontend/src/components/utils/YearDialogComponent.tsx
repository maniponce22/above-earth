import React, { Component } from 'react';

// CSS imports
import 'bootstrap/dist/css/bootstrap.min.css'

// import components
import FilterUtils from '../utils/FilterUtils'

// material-ui imports
import TextField     from '@material-ui/core/TextField';
import Button        from '@material-ui/core/Button';
import Dialog        from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle   from '@material-ui/core/DialogTitle';

// define the state
type DialogState = {
    isOpen:    boolean;
    startYear: number;
    endYear:   number;
};

// define the props
type DialogProps = {
    title:    string;
    onChange: any;
}

class YearDialog extends Component<DialogProps, DialogState> {
    state: DialogState = {
        isOpen:    false,
        startYear: FilterUtils.start_year,
        endYear:   FilterUtils.end_year
    };

    open = () => {
        // set visibility to true
        this.setState({isOpen: true})
    }

    handleToggle = () => {
        // toggle visibility
        this.setState({isOpen: !this.state.isOpen});
    }

    handleGo = () => {
        // send on change update, update state
        this.props.onChange([ this.state.startYear, this.state.endYear ]);
        this.setState({isOpen: false, startYear: FilterUtils.start_year, endYear: FilterUtils.end_year})
    }

    handleStartChange = (event) => {
        // update start year
        this.setState({startYear: event.target.value})
    }

    handleEndChange = (event) => {
        // update end year
        this.setState({endYear: event.target.value})
    }

    render() {
        return (
            <Dialog open={this.state.isOpen} onClose={this.handleToggle}>
                <DialogTitle>{this.props.title}</DialogTitle>
                <DialogContent>
                    <TextField
                        autoFocus
                        style={{minWidth: "150px"}}
                        variant="outlined"
                        id="start"
                        type="Number"
                        defaultValue={this.state.startYear}
                        label="Start Year"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        onChange={this.handleStartChange}
                    />
                    <TextField
                        className="ml-3"
                        style={{minWidth: "150px"}}
                        variant="outlined"
                        id="end"
                        type="Number"
                        defaultValue={this.state.endYear}
                        label="End Year"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        onChange={this.handleEndChange}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleToggle} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={this.handleGo} color="primary">
                        Go
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

export default YearDialog;
