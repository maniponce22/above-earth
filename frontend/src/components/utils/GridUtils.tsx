import {Row, Col} from 'react-bootstrap';

export default class GridUtils {
    // CONSTANTS
    static cards_per_row = 5;
    static time_limit    = 1200;

    static renderCol(instance, index, row, renderCard) {
        // specify the padding based on whether it is on the edge or not
        var card_padding = "m-0 p-2";
        if (index === 0)
            card_padding = "m-0 pl-0 pt-2 pr-2 pb-2";
        else if (index === this.cards_per_row - 1)
            card_padding = "m-0 pl-2 pt-2 pr-0 pb-2";

        // just return a column if null
        if (instance === null) return (<Col className={card_padding}/>);

        // set timeout val, maxed with time limit
        var timeout_val = (index + row) * 200
        if (timeout_val > this.time_limit)
            timeout_val = this.time_limit;

        return (
            <Col className={card_padding}>
                {renderCard(instance, timeout_val)}
            </Col>
        );
    }

    static renderRow(instances, row, renderCard) {
        return (
            <Row className="m-0 p-0">
                {instances.map((instance, index) => (
                    this.renderCol(instance, index, row, renderCard)))}
            </Row>
        )
    }

    static renderGrid(instances, renderCard) {
        // build an array of arrays length 5
        var rows: Array<Array<any>> = [[]]
        for (let i of instances) {
            // create new array if needed
            if (rows[rows.length - 1].length === this.cards_per_row)
                rows.push([]);

            // push into last array
            rows[rows.length - 1].push(i);
        }

        // fill excess with nulls
        while (rows[rows.length - 1].length !== this.cards_per_row)
            rows[rows.length - 1].push(null);

        return (
            <div className="m-0 p-0">
                {rows.map((rowInstances, index) => (
                    this.renderRow(rowInstances, index, renderCard)))}
            </div>
        );
    }
}