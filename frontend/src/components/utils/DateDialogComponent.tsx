import React, { Component } from 'react';

// CSS imports
import 'bootstrap/dist/css/bootstrap.min.css'

// material-ui imports
import TextField     from '@material-ui/core/TextField';
import Button        from '@material-ui/core/Button';
import Dialog        from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle   from '@material-ui/core/DialogTitle';

// define the state
type DialogState = {
    isOpen: boolean;
    startDate: string;
    endDate: string;
};

// define the props
type DialogProps = {
    title: string;
    onChange: any;
}

// CONSTANTS
const default_start_date = "1969-07-20";
const default_end_date =
    String(new Date().getFullYear()) + "-" + 
    String(new Date().getMonth() + 1).padStart(2, '0') + "-" + 
    String(new Date().getDate()).padStart(2, '0');

class DateDialog extends Component<DialogProps, DialogState> {
    state: DialogState = {
        isOpen: false,
        startDate: default_start_date,
        endDate: default_end_date
    };

    open = () => {
        // turn on visibility
        this.setState({isOpen: true})
    }

    handleToggle = () => {
        // toggle visibility
        this.setState({isOpen: !this.state.isOpen});
    }

    handleGo = () => {
        // send on change update, update state
        this.props.onChange([new Date(this.state.startDate), new Date(this.state.endDate)]);
        this.setState({isOpen: false, startDate: default_start_date, endDate: default_end_date})
    }

    handleStartChange = (event) => {
        // update start year
        this.setState({startDate: event.target.value})
    }

    handleEndChange = (event) => {
        // update end year
        this.setState({endDate: event.target.value})
    }

    render() {
        return (
            <Dialog open={this.state.isOpen} onClose={this.handleToggle}>
                <DialogTitle>{this.props.title}</DialogTitle>
                <DialogContent>
                    <TextField
                        autoFocus
                        style={{minWidth: "150px"}}
                        variant="outlined"
                        id="start"
                        type="date"
                        defaultValue={this.state.startDate}
                        label="Start Date"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        onChange={this.handleStartChange}
                    />
                    <TextField
                        className="ml-3"
                        style={{minWidth: "150px"}}
                        variant="outlined"
                        id="end"
                        type="date"
                        defaultValue={this.state.endDate}
                        label="End Date"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        onChange={this.handleEndChange}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleToggle} color="primary">
                        Cancel
                    </Button>
                    <Button
                        disabled={!(this.state.startDate!=="" && this.state.endDate!=="")}
                        onClick={this.handleGo} color="primary">
                        Go
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

export default DateDialog;
