import React, { Component } from 'react';

// CSS imports
import 'bootstrap/dist/css/bootstrap.min.css'

// material-ui imports
import MenuItem      from '@material-ui/core/MenuItem';
import FormControl   from '@material-ui/core/FormControl';
import Select        from '@material-ui/core/Select';
import Button        from '@material-ui/core/Button';
import Chip          from '@material-ui/core/Chip';
import Dialog        from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle   from '@material-ui/core/DialogTitle';

// define the state
type DialogState = {
    isOpen: boolean;
    selected: string[];
};

// define the props
type DialogProps = {
    title: string;
    entries: string[];
    onChange: any;
}

class ChipDialog extends Component<DialogProps, DialogState> {
    state: DialogState = {
        isOpen: false,
        selected: []
    };

    open = () => {
        // turn on visibility
        this.setState({isOpen: true})
    }

    handleToggle = () => {
        // toggle visibility
        this.setState({isOpen: !this.state.isOpen});
    }

    handleGo = () => {
        // send on change update, update state
        this.props.onChange(this.state.selected);
        this.setState({isOpen: false, selected: []})
    }

    handleChange = (event) => {
        // update selected list
        this.setState({selected: event.target.value})
    }

    render() {
        return (
            <Dialog
                fullWidth
                open={this.state.isOpen}
                onClose={this.handleToggle}>
                <DialogTitle>{this.props.title}</DialogTitle>
                <DialogContent>
                    <FormControl style={{width: "100%"}}>
                        <Select
                            multiple
                            value={this.state.selected}
                            onChange={this.handleChange}
                            renderValue={(selected: any) => (
                                <div className="p-1" style={{
                                    display: 'flex',
                                    flexWrap: 'wrap'}}>
                                    {selected.map((value) => (
                                        <Chip key={value} label={value} className="m-1"/>
                                    ))}
                                </div>
                            )}
                            MenuProps={{
                                PaperProps: {
                                    style: {
                                        maxHeight: 400,
                                    }
                                },
                                getContentAnchorEl: null
                            }}
                            >
                            {this.props.entries.map(entry => 
                                <MenuItem key={entry} value={entry}>
                                    {entry}
                                </MenuItem>)}
                        </Select>
                    </FormControl>
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleToggle} color="primary">
                        Cancel
                    </Button>
                    <Button
                        disabled={this.state.selected.length === 0}
                        onClick={this.handleGo} color="primary">
                        Go
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

export default ChipDialog;
