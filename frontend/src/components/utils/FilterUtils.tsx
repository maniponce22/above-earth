import React, { Fragment } from 'react';

// CSS imports
import 'bootstrap/dist/css/bootstrap.min.css'

// material-ui icon imports
import IconButton        from '@material-ui/core/IconButton';
import Close             from '@material-ui/icons/Close'
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowUpwardIcon   from '@material-ui/icons/ArrowUpward';

// material-ui imports
import Typography     from '@material-ui/core/Typography';
import Divider        from '@material-ui/core/Divider';
import Select         from '@material-ui/core/Select';
import InputLabel     from '@material-ui/core/InputLabel';
import MenuItem       from '@material-ui/core/MenuItem';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField      from '@material-ui/core/TextField';
import SearchIcon     from '@material-ui/icons/Search';
import Button         from '@material-ui/core/Button';
import Tune           from '@material-ui/icons/Tune';

// react-bootstrap imports
import {Col, Accordion} from 'react-bootstrap';

export default class FilterUtils {
    // Constants
    static month_string = 
        String(new Date().getFullYear()) + "-" + 
        String(new Date().getMonth() + 1).padStart(2, '0') + "-01";
    static year_string = String(new Date().getFullYear()) + "-01-01";

    static month_date = new Date(FilterUtils.month_string);
    static year_date  = new Date(FilterUtils.year_string);

    static start_year = 1717;
    static end_year = new Date().getFullYear();

    static filterEntry(text, selected, handler, ...handlerArgs) {
        return (
            <Typography
                className={
                    "ptr text-muted" +
                    (selected ? " font-weight-bold" : "")}
                variant="body2"
                onClick={() => handler.apply(this, handlerArgs)}>
                {text}
                <Close
                    style={{display:
                        ((selected) ? "" : "none")}}
                    className="mx-auto my-auto p-1"/>
            </Typography>
        );
    }

    static filterColumnHeader(text) {
        return (
            <Typography className="font-weight-bold text-dark" variant="button">
                {text}
            </Typography>
        );
    }

    static filterColumnUnderline() {
        return (
            <Col className="ml-0 mr-0 mt-2 mb-2 p-0 pl-0 pr-5">
                <Divider className="m-0"/>
            </Col>
        );
    }

    static orderDirectionButton(isDesc, handler) {
        if (isDesc)
            return (
                <IconButton
                    className="my-auto"
                    style={{float: "left", width: "50px", height: "50px"}}
                    onClick={handler}>
                    <ArrowDownwardIcon/>
                </IconButton>
            );
        else
            return (
                <IconButton
                    className="my-auto"
                    style={{float: "left", width: "50px", height: "50px"}}
                    onClick={handler}>
                    <ArrowUpwardIcon/>
                </IconButton>
            );
    }

    static sortSelector(sort_options, defaultVal, handler) {
        return (
            <Fragment>
                <InputLabel >Sort By</InputLabel>
                <Select
                    onChange={handler}
                    label="Sort By"
                    defaultValue={defaultVal}
                    className="shadow">

                    {sort_options.map(s => (
                        <MenuItem value={s.value}>{s.name}</MenuItem>
                    ))}
                </Select>
            </Fragment>
        );
    }

    static searchBar(title, handler, width="default", ref: any=null) {
        return (
            <TextField className="shadow"
                label={title}
                variant="outlined"
                style={{marginLeft: "16px", width: width}}
                inputRef={ref}
                onKeyPress={(event) => {
                    if (event.code === 'Enter') {
                        event.preventDefault()
                        handler();
                    }
                }}

                InputProps={{
                    endAdornment: (
                    <InputAdornment position='end'>
                        <IconButton
                        aria-label='search'
                        onClick={handler}>
                        <SearchIcon />
                        </IconButton>
                    </InputAdornment>
                    ),
                }}/>
        );
    }

    static filterButton(key) {
        return (
            <Accordion.Toggle 
                as={Button} 
                eventKey={String(key)} 
                className="font-weight-bold text-dark"
                style={{float: "left", width: "120px"}}
                startIcon={<Tune/>}>
                Filters
            </Accordion.Toggle>
        );
    }
}


