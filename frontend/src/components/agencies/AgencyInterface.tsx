export interface AgenciesSearchState {
    status:        number;
    numOfAgencies: number;
    agencies:      Agency[];
    total:         number;
    currentPage:   number;
    pageSize:      number;
    filters:       Filters;
    keywords:      string[];
    sort_by:       string;
    order_by:      string;
}

export interface Filters {
    founding_year_start: number;
    founding_year_end:   number;
    countries:           string[];
    types:               string[];
}

export interface FilterData {
    countries: string[];
    types:     string[];
}

export interface Agency {
    id:                 number,
    name:               string,
    type:               string,
    country:            string,
    abbrev:             string,
    founding_year:      string,
    total_launch_count: number,
    total_story_count:  number,
    logo_url:           string
}

export interface AgencyInstanceState {
    status: number
    agency: {
        id:                  number,
        name:                string,
        type:                string,
        country:             string,
        abbrev:              string,
        description:         string,
        administrator:       string,
        founding_year:       number,
        failed_launches:     number,
        successful_launches: number,
        pending_launches:    number,
        total_launch_count:  number,
        total_story_count:   number,
        image_url:           string,
        info_url:            string,
        logo_url:            string,
        wiki_url:            string,
        pads:                Pad[]
    },
    linked_expeditions: LinkedExpedition[],
    linked_stories:     LinkedStory[],
    related_agencies:   RelatedAgency[]
}

export interface RelatedAgency {
    id:   number,
    name: string
}

export interface Pad {
    name:      string,
    latitude:  number,
    longitude: number
}

export interface LinkedExpedition {
    id:   number,
    name: string
}

export interface LinkedStory {
    id:    number,
    title: string
}

