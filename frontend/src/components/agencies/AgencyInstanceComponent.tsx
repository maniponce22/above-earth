import React, { Component, Fragment } from 'react';
import { RouteComponentProps, Link } from 'react-router-dom';
import axios from 'axios';

// CSS imports
import './agencies.css';

// import components and interfaces
import {AgencyInstanceState} from './AgencyInterface'
import PadsMap               from './PadsMapComponent';
import Utils                 from '../utils/Utils'

// react-bootstrap imports
import { Media, Image, Row, Col, Container, Card, Badge, Table } from 'react-bootstrap';

class AgencyInstance extends Component<RouteComponentProps, AgencyInstanceState> {
    state: AgencyInstanceState = {
        status: Utils.starting,
        agency: {
            id:                  0,
            name:                "",
            type:                "",
            country:             "",
            abbrev:              "",
            description:         "",
            administrator:       "",
            founding_year:       0,
            failed_launches:     0,
            successful_launches: 0,
            pending_launches:    0,
            total_launch_count:  0,
            total_story_count:   0,
            image_url:           "",
            info_url:            "",
            logo_url:            "",
            wiki_url:            "",
            pads:                []
        },
        linked_expeditions: [],
        linked_stories:     [],
        related_agencies:   []
    }



    // ----------
    // AXIOS CALL
    // ----------

    async getAgency() {
        try {
            // send axios call
            await axios.get(
                `${process.env.REACT_APP_API_URL}/agencies/` + this.props.match.params['agencyId']
            ).then(response => {
                var data = response.data;
                // update state
                if ('error' in data)
                    this.setState({status: Utils.error})
                else
                    this.setState({
                        status: Utils.success,
                        agency: data.agency,
                        linked_expeditions: data.linked_expeditions,
                        linked_stories: data.linked_stories,
                        related_agencies: data.related_agencies
                    });
            }).catch(error => {
                this.setState({status: Utils.error})
            });
        } catch (error) {
            this.setState({status: Utils.error})
        }   
    }

    componentDidMount() {
        window.scrollTo(0,0);
        this.getAgency();
    }


    // ------
    // RENDER
    // ------

    renderSuccess() {
        return (
            <div>
                <Container>
                    <Row className="align-self-center mt-5">
                        <h1>{this.state.agency.name}</h1>
                    </Row>
                    <br />
                    <Row>
                        <Col>
                            <Media>
                                <Image src={this.state.agency.logo_url} className="agency-logo mr-3" />
                                <Media.Body>
                                    {this.state.agency.description}
                                </Media.Body>
                            </Media>
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Card>
                            <Card.Body>
                                <ul style={{ listStyleType: 'none' }}>
                                    <li><b>Abbreviation: </b>{this.state.agency.abbrev}</li>
                                    <li><b>Country: </b>{this.state.agency.country}</li>
                                    <li><b>Type: </b>{this.state.agency.type}</li>
                                    <li><b>Administrator: </b>{this.state.agency.administrator}</li>
                                    <li><b>Founding Year: </b>{this.state.agency.founding_year}</li>
                                </ul>
                                <div className="text-center">
                                    <Card.Link href={this.state.agency.info_url} target="_blank">{this.state.agency.abbrev} Website</Card.Link>{'  '}
                                    <Card.Link href={this.state.agency.wiki_url} target="_blank">Wikipedia</Card.Link>
                                </div>
                            </Card.Body>
                        </Card>
                        <Col>
                            <h4>Total Launches: {this.state.agency.total_launch_count}</h4>
                            <br />
                            <h4><Badge variant="success">Successful Launches:</Badge> {this.state.agency.successful_launches}</h4>
                            <h4><Badge variant="danger">Failed Launches:</Badge> {this.state.agency.failed_launches}</h4>
                            <h4><Badge variant="warning">Pending Launches:</Badge> {this.state.agency.pending_launches}</h4>
                        </Col>
                        <Col>
                            <Image src={this.state.agency.image_url} className="agency-img" />
                        </Col>
                    </Row>
                </Container>
                <hr />
                <Container>
                    <h4>Pad Locations</h4>
                    <PadsMap pads={this.state.agency.pads} />
                </Container>
                <hr />
                <Container>
                    <Row>
                        <Col>
                            <Table striped bordered hover>
                                <thead>
                                    <tr>
                                        <th>Related Expeditions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.linked_expeditions.map((expedition) => {
                                        return (
                                            <tr>
                                                <td>
                                                    <Link to={{ pathname: `/expeditions/${expedition.id}` }}>
                                                        {expedition.name}
                                                    </Link>
                                                </td>
                                            </tr>
                                        )
                                    })}
                                </tbody>
                            </Table>
                        </Col>
                        <Col>
                            <Table striped bordered hover>
                                <thead>
                                    <tr>
                                        <th>Related Stories</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.linked_stories.map((story) => {
                                        return (
                                            <tr>
                                                <td>
                                                    <Link to={{ pathname: `/news/${story.id}`}}>
                                                        {story.title}
                                                    </Link>
                                                </td>
                                            </tr>
                                        )
                                    })}
                                </tbody>
                            </Table>
                        </Col>
                        <Col>
                            <Table striped bordered hover>
                                <thead>
                                    <tr>
                                        <th>Related Agencies</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.related_agencies.map((agency) => {
                                        return (
                                            <tr>
                                                <td>
                                                    <a href={String("/agencies/").concat(String(agency.id))}>
                                                        {agency.name}
                                                    </a>
                                                </td>
                                            </tr>
                                        )
                                    })}
                                </tbody>
                            </Table>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }

    renderContent() {
        // render based on status
        switch(this.state.status) {
            case Utils.starting : return Utils.renderSpinner();
            case Utils.success  : return this.renderSuccess();
            case Utils.error    : return Utils.renderMessage("404 Not Found");
        }
    }

    render() {
        return (
            <Container className="mt-5" style={{paddingLeft: "15%", paddingRight: "15%"}} fluid>
                {this.renderContent()}
            </Container>
        );
    }
}

export default AgencyInstance;