import React, { Component, Fragment } from 'react';

// import leaflet
import 'leaflet/dist/leaflet.css';
import L          from 'leaflet';
import icon       from 'leaflet/dist/images/marker-icon.png';
import iconShadow from 'leaflet/dist/images/marker-shadow.png';
import { MapContainer, Marker, TileLayer, Popup } from 'react-leaflet';

let DefaultIcon = L.icon({
    iconUrl:   icon,
    shadowUrl: iconShadow
});

L.Marker.prototype.options.icon = DefaultIcon;

// input pads
interface Pad {
    name:      string,
    latitude:  number,
    longitude: number
}

// define the state
type MapProps = {
    pads: Pad[]
}

class PadsMap extends Component<MapProps> {
    render() {
        return (
            <MapContainer
                center={[0, 0]}
                zoom={3}
                style={{ height: "500px" }}>
                <TileLayer
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                {this.props.pads.map((pad) => 
                    <Marker position={[pad.latitude, pad.longitude]}>
                        <Popup>{pad.name}</Popup>
                    </Marker>)}
            </MapContainer>
        );
    }
}

export default PadsMap;
