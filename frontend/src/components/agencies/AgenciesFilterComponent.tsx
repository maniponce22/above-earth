import React, { Component } from 'react';
import axios from 'axios';

// CSS imports
import 'bootstrap/dist/css/bootstrap.min.css'
import './agencies.css';

// import components and interfaces
import { Filters, FilterData } from './AgencyInterface';
import Utils       from '../utils/Utils'
import FilterUtils from '../utils/FilterUtils'
import ChipDialog  from '../utils/ChipDialogComponent'
import YearDialog  from '../utils/YearDialogComponent'

// react-bootstrap imports
import {Form, Accordion, Row, Col, InputGroup} from 'react-bootstrap';

// define the state
type AgenciesFilterState = {
    year_key:       number;
    custom_country: boolean;
    filters:        Filters;
    filter_data:    FilterData;
    keywords:       string[];
}

// CONSTANTS
const filter_list_limit = 5;

class AgenciesFilter extends Component<any, AgenciesFilterState> {
    state: AgenciesFilterState = {
        year_key:       0,
        custom_country: false,
        keywords:       [],

        filters: {
            founding_year_start: FilterUtils.start_year,
            founding_year_end:   FilterUtils.end_year,
            countries:           [],
            types:               []
        },

        filter_data: {
            countries: [],
            types:     []
        }
    };

    // refs
    searchBarRef;
    yearDialogRef;
    countryDialogRef;

    constructor(props: any) {
        super(props);

        this.searchBarRef     = React.createRef();
        this.yearDialogRef    = React.createRef();
        this.countryDialogRef = React.createRef();
    }

    async getCountryTypeData() {
        try {
            // send axios call
            await axios.get(
                `${process.env.REACT_APP_API_URL}/overview/agencies`
            ).then(response => {
                // count number of countries and types
                var countries = {};
                var types = {};
                for (var a of response.data.agencies) { 
                    // count countries
                    a.country = a.country.replace(/,/g, ", ").replace(/  /g, " ")
                    for (var c of a.country.split(", ")) {
                        if (!(c in countries))
                            countries[c] = 0;
                        countries[c] += 1;
                    }

                    // count types
                    if (!(a.type in types))
                        types[a.type] = 0;
                    types[a.type] += 1;
                }

                // create and sort countries array
                var countries_arr = Object.keys(countries).map(key => ({country: key, count: countries[key]}));
                countries_arr.sort((a, b) => (b.count - a.count));

                // create and sort type array
                var types_arr = Object.keys(types).map(key => ({type: key, count: types[key]}));
                types_arr.sort((a, b) => (b.count - a.count));

                // update state
                this.setState({
                    filter_data: {
                        ...this.state.filter_data,
                        countries: countries_arr.map(c => (c.country)),
                        types:     types_arr.map(t => (t.type))
                    }
                })
            }).catch(error => {});
        } catch(error) {}
    }

    componentDidMount() {
        this.getCountryTypeData();
    }

    componentDidUpdate(prevProps, prevState) {
        // send an onChange update if the filters have not changed
        if (prevState.filters.founding_year_start !== this.state.filters.founding_year_start ||
            prevState.filters.founding_year_end   !== this.state.filters.founding_year_end   ||
            !Utils.arraysEqual(prevState.keywords,          this.state.keywords         ) ||
            !Utils.arraysEqual(prevState.filters.countries, this.state.filters.countries) ||
            !Utils.arraysEqual(prevState.filters.types,     this.state.filters.types    ))
            this.props.onChange(this.state);
    }



    // ------
    // SEARCH
    // ------

    handleSearchGo = () => {
        // prevent tsx warnings
        if (this.searchBarRef.current === null)
            return;
        
        // build keyword array and update state
        var kws = this.searchBarRef.current.value.toString().split(" ").filter(kw => (kw !== "" && kw !== undefined))
        this.setState({keywords: kws})
    }



    // --------------
    // Founding Year
    // --------------

    handleYearClick = (key) => {
        if (key === this.state.year_key) {
            // if getting rid of the filter...
            this.setState({
                year_key: 0,
                filters: {
                    ...this.state.filters,
                    founding_year_start: FilterUtils.start_year,
                    founding_year_end: FilterUtils.end_year,
                }
            });
        } else {
            // if adding a filter...
            if (key === 2)
                // Click CUSTOM, open dialog
                this.yearDialogRef.current.open();
            else
                // Click CENTURY
                this.setState({
                    year_key: 1,
                    filters: {
                        ...this.state.filters,
                        founding_year_start: 2000,
                        founding_year_end: FilterUtils.end_year,
                    }
                });
        }
    };



    // -------
    // Country
    // -------

    handleSetCountry = (name) => {
        // create new country array
        var new_countries = [name];
        if (Utils.arraysEqual(this.state.filters.countries, [name]) && !this.state.custom_country) 
            new_countries = [];

        // update state
        this.setState({
            filters: { ...this.state.filters, countries: new_countries }, 
            custom_country: false
        });
    }

    handleCountryMoreClick = () => {
        if (this.state.custom_country)
            // removing filter, deselect all
            this.setState({
                filters: { ...this.state.filters, countries: [] },
                custom_country: false
            })
        else
            // open the dialog
            this.countryDialogRef.current.open();
    }



    // ----
    // Type
    // ----

    handleSetType = (type) => {
        // create new type array
        var new_types = [type];
        if (Utils.arraysEqual(this.state.filters.types, [type]))
            new_types = [];

        // update state
        this.setState({filters: { ...this.state.filters, types: new_types }});
    }



    // ------
    // Render
    // ------

    render() {
        return (
            <Accordion key="0" className="mb-4">
                <Form className="mx-auto mb-0">
                    <Form.Row>
                        <InputGroup className="px-auto mx-1">
                            {FilterUtils.filterButton(0)}
                            {FilterUtils.searchBar(
                                "Search Agencies",
                                this.handleSearchGo.bind(this),
                                "calc(100% - 136px)",
                                this.searchBarRef
                            )}
                        </InputGroup>
                    </Form.Row>
                </Form>
                <Accordion.Collapse eventKey="0">
                    <Row className="mx-auto mt-3">
                        <Col sm="4">
                            <Row>{FilterUtils.filterColumnHeader("Founding Year")}</Row>
                            <Row>{FilterUtils.filterColumnUnderline()}</Row>
                            <Row>
                                {FilterUtils.filterEntry(
                                    "This century", 
                                    this.state.year_key === 1, 
                                    this.handleYearClick.bind(this), 1)}
                            </Row>
                            <Row>
                                {FilterUtils.filterEntry(
                                    "Custom", 
                                    this.state.year_key === 2, 
                                    this.handleYearClick.bind(this), 2)}
                                <YearDialog 
                                    ref={this.yearDialogRef}
                                    title="Custom Founding Year Range"
                                    onChange={years => {
                                        this.setState({
                                            year_key: 2,
                                            filters: {
                                                ...this.state.filters,
                                                founding_year_start: years[0],
                                                founding_year_end: years[1]
                                            }
                                        })
                                    }}/>
                            </Row>
                        </Col>
                        <Col sm="4">
                            <Row>{FilterUtils.filterColumnHeader("Country")}</Row>
                            <Row>{FilterUtils.filterColumnUnderline()}</Row>
                            {this.state.filter_data.countries.slice(0, filter_list_limit).map(c => 
                                <Row key={c}>
                                    {FilterUtils.filterEntry(
                                        c,
                                        (Utils.arraysEqual(this.state.filters.countries, [c]) 
                                            && !this.state.custom_country), 
                                        this.handleSetCountry.bind(this), c)}
                                </Row>)}
                            <Row>
                                {FilterUtils.filterEntry(
                                    "More", 
                                    this.state.custom_country, 
                                    this.handleCountryMoreClick.bind(this))}
                                <ChipDialog 
                                    ref={this.countryDialogRef}
                                    title="Specify Countries"
                                    entries={this.state.filter_data.countries} 
                                    onChange={(selection) => {
                                        this.setState({
                                            filters: { ...this.state.filters, countries: selection },
                                            custom_country: true
                                        })
                                    }}/>
                            </Row>
                        </Col>
                        <Col sm="4">
                            <Row>{FilterUtils.filterColumnHeader("Agency Type")}</Row>
                            <Row>{FilterUtils.filterColumnUnderline()}</Row>
                            {this.state.filter_data.types.slice(0, filter_list_limit).map(t => 
                                <Row key={t}>
                                    {FilterUtils.filterEntry(
                                        t,
                                        Utils.arraysEqual(this.state.filters.types, [t]),
                                        this.handleSetType.bind(this), t)}
                                </Row>)}
                        </Col>
                    </Row>
                </Accordion.Collapse>
            </Accordion>
        );
    }
}

export default AgenciesFilter;
