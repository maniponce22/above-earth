import React, { Component, Fragment } from 'react';
import { Link }                       from 'react-router-dom';
import axios                          from 'axios';

// CSS imports
import './agencies.css';

// import interfaces and components
import { AgenciesSearchState } from './AgencyInterface';
import PaginationComponent     from '../Pagination';
import AgenciesFilter          from './AgenciesFilterComponent';
import Utils                   from '../utils/Utils'

// react-bootstrap imports
import { Table, Container, Form } from 'react-bootstrap';

// material-ui imports
import Button            from '@material-ui/core/Button';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowUpwardIcon   from '@material-ui/icons/ArrowUpward';

const sort_options = [
    { name: "Name",               value: "name",               width: "20%" },
    { name: "Country",            value: "country",            width: "25%" },
    { name: "Founding Year",      value: "founding_year",      width: "15%"},
    { name: "Type",               value: "type",               width: "10%" },
    { name: "Total Launch Count", value: "total_launch_count", width: "15%" },
    { name: "Total Story Count",  value: "total_story_count",  width: "15%"}];

class AgenciesSearch extends Component<any, AgenciesSearchState> {
    state: AgenciesSearchState = {
        status:        Utils.starting,
        numOfAgencies: 0,
        agencies:      [],
        total:         -1,
        currentPage:   1,
        pageSize:      10,
        filters: {
            founding_year_start: 1600,
            founding_year_end:   3000,
            countries:           [],
            types:               []
        },
        keywords: [],
        sort_by:  "name",
        order_by: "asc"
    };

    // ----------
    // AXIOS CALL
    // ----------

    async getAgencies(page, pageSize) {
        // change status to loading
        if (this.state.status !== Utils.starting) {
            this.setState({ status: Utils.loading })
        }

        // build parameter string
        var parameters = [
            ["limit", this.state.pageSize],
            ["offset", (page - 1) * pageSize],
            ["founding_year_start", this.state.filters.founding_year_start],
            ["founding_year_end", this.state.filters.founding_year_end],
            ["sort_by", this.state.sort_by],
            ["order_by", this.state.order_by]];
        this.state.keywords         .map(k => {parameters.push(["keyword", k]);});
        this.state.filters.countries.map(c => {parameters.push(["country", c]);});
        this.state.filters.types    .map(t => {parameters.push(["type", t]);   });
        var joined_parameters = parameters.map(p => (p[0] + "=" + p[1])).join("&")

        try {
            await axios.get(
                `${process.env.REACT_APP_API_URL}/agencies?${joined_parameters}`,
            ).then(response => {
                var data = response.data;
                // update state
                if ('error' in data) {
                    this.setState({status: Utils.error})
                } else if (data.count === 0) {
                    this.setState({status: Utils.no_results})
                } else {
                    var agencies = data.agencies;
                    for (var a of agencies)
                        a.country = a.country.replace(/,/g, ", ").replace(/  /g, " ")

                    this.setState({
                        status: Utils.success,
                        numOfAgencies: agencies.length,
                        agencies: agencies,
                        total: data.count
                    });
                }
            }).catch(error => {
                this.setState({status: Utils.error});
            });
        } catch (error) {
            this.setState({status: Utils.error})
        }
    }

    componentDidMount() {
        window.scrollTo(0, 0);
        this.getAgencies(this.state.currentPage, this.state.pageSize);
    }

    componentDidUpdate(prevProps, prevState) {
        // Typical usage (don't forget to compare props):
        if (this.state.currentPage                 !== prevState.currentPage                 || 
            this.state.pageSize                    !== prevState.pageSize                    ||
            this.state.filters.founding_year_start !== prevState.filters.founding_year_start ||
            this.state.filters.founding_year_end   !== prevState.filters.founding_year_end   ||
            this.state.sort_by                     !== prevState.sort_by                     ||
            this.state.order_by                    !== prevState.order_by                    ||
            !Utils.arraysEqual(this.state.filters.countries, prevState.filters.countries) ||
            !Utils.arraysEqual(this.state.filters.types,     prevState.filters.types    ) ||
            !Utils.arraysEqual(this.state.keywords,          prevState.keywords         )) {
            window.scrollTo(0, 0);
            this.getAgencies(this.state.currentPage, this.state.pageSize);
        }
    }



    // -------
    // SORTING
    // -------

    handleOrderChange = (val) => {
        if (this.state.order_by === "asc")
            this.setState({sort_by: val, order_by: "desc"})
        else
            this.setState({sort_by: val, order_by: "asc"})
    }

    displayArrow = (val) => {
        if (this.state.sort_by === val) {
            if (this.state.order_by === "asc") return (<ArrowUpwardIcon/>)
            else return (<ArrowDownwardIcon/>)
        }
    }
    


    // ------
    // FILTER
    // ------

    handleFilter = (searchProps) => {
        this.setState({
            filters: searchProps.filters,
            keywords: searchProps.keywords,
            currentPage: 1
        });
    }



    // ------------
    // RENDER MODES
    // ------------

    renderSuccess() {
        return (
            <div>
                {this.renderFilter()}
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            {sort_options.map(col => {
                                return (
                                    <th key={col.value} style={{width: col.width}}>
                                        <Button
                                            color="primary"
                                            onClick={() => this.handleOrderChange(col.value)}>
                                            {col.name}
                                            {this.displayArrow(col.value)}
                                        </Button>
                                    </th>
                                );
                            })}
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.agencies.map((agency) => {
                            return (
                                <tr key={agency.name}>
                                    <td>
                                        <Link to={{ pathname: `/agencies/${agency.id}` }}>
                                            {Utils.highlightText(agency.name, this.state.keywords)}
                                            {/* <img src={agency.logo_url} height="200" width="auto" className="agency-logo" alt="" /> */}
                                        </Link>
                                    </td>
                                    <td>{Utils.highlightText(agency.country.replace(",", ", "), this.state.keywords)}</td>
                                    <td>{Utils.highlightText(agency.founding_year, this.state.keywords)}</td>
                                    <td>{Utils.highlightText(agency.type, this.state.keywords)}</td>
                                    <td>{Utils.highlightText(agency.total_launch_count, this.state.keywords)}</td>
                                    <td>{Utils.highlightText(agency.total_story_count, this.state.keywords)}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </Table>
                {this.renderPagination(true)}
            </div>
        );
    }

    renderNoResults() {
        return(
            <div>
                {this.renderFilter()}
                {Utils.renderMessage("Uh oh! No results were found")}
                {this.renderPagination()}
            </div>
        );
    }

    renderError() {
        return(
            <div>
                {this.renderFilter()}
                {Utils.renderMessage("Error retrieving data")}
                {this.renderPagination()}
            </div>
        );
    }

    renderContent() {
        switch(this.state.status) {
            case Utils.starting   : return Utils.renderSpinner();
            case Utils.loading    : return this.renderSuccess();
            case Utils.success    : return this.renderSuccess();
            case Utils.no_results : return this.renderNoResults();
            default               : return this.renderError();
        }
    }



    // ------
    // RENDER
    // ------

    renderFilter() {
        return (
            <AgenciesFilter onChange={this.handleFilter}/>
        );
    }

    renderPagination(visible=false) {
        return (
            <div className="pagination mb-3 mt-3" style={{display: (visible ? "" : "none")}}>
                <Form.Label className="my-auto mr-2">
                    Agencies per page:
                </Form.Label>
                <Form.Control className="my-auto" as="select" style={{width: 100}} onChange={(event) => {window.scrollTo(0,0); this.setState({currentPage: 1, pageSize: parseInt(event.target.value)})}} custom>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </Form.Control>

                <Form.Label className="my-auto mx-4">
                    {((this.state.currentPage - 1) * this.state.pageSize) + 1}-
                    {Math.min(this.state.total, this.state.currentPage * this.state.pageSize)} of {this.state.total}
                </Form.Label>
                <PaginationComponent
                    total={this.state.total}
                    itemsPerPage={this.state.pageSize}
                    currentPage={this.state.currentPage}
                    onPageChange={page => {window.scrollTo(0,0); this.setState({currentPage: page})}}
                />
            </div>
        );
    }

    render() {
        return (
            <Container className="mt-3" style={{paddingLeft: "5%", paddingRight: "5%"}} fluid>
                <h1>Agencies</h1>
                {this.renderContent()}
            </Container>
        );
    }
}

export default AgenciesSearch;
