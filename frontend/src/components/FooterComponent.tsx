import React from "react";

class Footer extends React.Component {
  render() {
    return (
        <section id="splashCreation">
            <div className="SplashCreationContent">
                <h2>Above Earth . Austin, TX</h2>
                <h3>est. 2021</h3>
            </div>
        </section>
    );
  }
}

export default Footer;
