import React, { Component, Fragment } from 'react';
import { Link }                       from 'react-router-dom';
import axios                          from 'axios';

// import images
import EarthHorizon from './SplashScreenImages/earthHorizon.jpg'
import ISS          from './SplashScreenImages/international-space-station.jpg'
import RocketLaunch from './SplashScreenImages/RockerLaunch.jpg'
import SpaceNews    from './SplashScreenImages/SpaceNews.jpg'

// React-bootstrap imports
import { Container, Row, Col, Media, Card, Button, CardDeck } from 'react-bootstrap';
import Carousel                                               from 'react-bootstrap/Carousel'

// interface returned by the api
interface Picture {
    day:         number,
    explanation: string,
    hdurl:       string,
    month:       number,
    title:       string, 
    url:         string
}

// define the state
type ApodState = {
    pictures: Picture[]
}

class SplashScreen extends Component <any, ApodState> {
    state: ApodState = {
        pictures: []
    }

    async getApodPictures() {
        try {
            // send axios call
            await axios.get(
                `${process.env.REACT_APP_API_URL}/pictures?limit=3`,
            ).then(response => {
                // update state with pictures
                var data = response.data;
                this.setState({pictures: data.pictures});
            }).catch(error => {});
        } catch (error) {}
    }    

    componentDidMount() {
        this.getApodPictures();
    }

    render() {
        window.scrollTo(0,0);
        return (
            <Fragment>
                <div className="MainSplashContent">
                    <div className="img-hover-zoom fade-in">
                        <img src={EarthHorizon} alt="Space Horizon" style={{minHeight: 600}} draggable="false"/>
                        <h1><span>ABOVE EARTH.</span></h1>
                        <h2><span>The technic-utopia is awakening. What is imagined?</span></h2>
                        <Link to="/about"><button>About </button></Link>
                    </div>
                </div>

                <div className="SplashInfo">
                    <h1><span>Tour Expeditions, News, and Agencies</span></h1>
                </div>
                <div className="SplashCards">
                    <CardDeck>
                        <Card className="text-center rounded-0 shadow" style={{ width: '18rem' }}>
                            <Card.Img variant="top" className="rounded-0" src={RocketLaunch} height="350" />
                            <Card.Body>
                                <Card.Title>Past, Present, and Future Expeditions</Card.Title>
                                <Card.Text style={{minHeight: 70}}>
                                    Space Expeditions, the investigation, by means of crewed and uncrewed spacecraft, of the reaches of the universe beyond Earth’s atmosphere.
                                </Card.Text>
                                <Link to="/expeditions"><Button variant="outline-dark" className="rounded-0"> Tour Expeditions</Button></Link>
                            </Card.Body>
                        </Card>
                        <Card className="text-center rounded-0 shadow" style={{ width: '18rem' }}>
                            <Card.Img variant="top" className="rounded-0" src={SpaceNews} height="350" />
                            <Card.Body>
                                <Card.Title>Breaking News</Card.Title>
                                <Card.Text style={{minHeight: 70}}>
                                    Read the breaking news about Space Expeditions taking place that are using the information gained to increase knowledge of the cosmos and benefit humanity.
                                </Card.Text>
                                <Link to="/news"><Button variant="outline-dark" className="rounded-0">Read the News</Button></Link>
                            </Card.Body>
                        </Card>
                        <Card className="text-center rounded-0 shadow" style={{ width: '18rem' }}>
                            <Card.Img variant="top" className="rounded-0" src={ISS} height="350" />
                            <Card.Body>
                                <Card.Title>Agencies</Card.Title>
                                <Card.Text style={{minHeight: 70}}>
                                    Take a look at the orginizations behind the Space Expeditions and the breaking news they are taking part of.
                                </Card.Text>
                                <Link to="/agencies"><Button variant="outline-dark" className="rounded-0">Tour Agencies</Button></Link>
                            </Card.Body>
                        </Card>
                    </CardDeck>
                </div>

                <div className="SplashInfo">
                    <h1><span>Astronomy Pictures of the Day</span></h1>
                </div>
                <div className="Apod">
                    <Carousel>
                        {this.state.pictures.map((picture) => 
                            <Carousel.Item>
                                <img className="cover" src={picture.hdurl} alt={picture.title}/>
                                <Carousel.Caption>
                                    <h3>{picture.title}</h3>
                                    <p>{picture.explanation}</p>
                                </Carousel.Caption>
                            </Carousel.Item>)}
                    </Carousel>
                </div>

                <div className="SplashMotto">
                    <h3>The technic-utopia is awakening. What is imagined?</h3>
                </div>
            </Fragment>
        );
    }
}

export default SplashScreen
