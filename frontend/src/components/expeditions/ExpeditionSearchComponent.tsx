import React, { Component } from 'react';
import axios                from 'axios';

// CSS imports
import 'bootstrap/dist/css/bootstrap.min.css'
import './ExpeditionSearch.css'

// import components and interfaces
import {ExpeditionSearchState} from './ExpeditionInterface';
import ExpeditionsFilter       from './ExpeditionsFilterComponent';
import PaginationComponent     from '../Pagination';
import Utils                   from '../utils/Utils'
import GridUtils               from '../utils/GridUtils'

// material ui imports
import Grow    from '@material-ui/core/Grow';
import Divider from '@material-ui/core/Divider';

// react-bootstrap imports
import {ListGroup, Container, Card, Form} from 'react-bootstrap'

class ExpeditionSearch extends Component<any, ExpeditionSearchState> {
    state: ExpeditionSearchState = {
        status:       Utils.starting,
        count:        0,
        expeditions:  [],
        page_size:    10,
        current_page: 1,

        filters: {
            start_date: new Date("1600-01-01"),
            end_date:   new Date("3000-01-01"),
            rockets:    [],
            agencies:   [],
            pads:       []
        },

        keywords: [],
        sort_by:  "date",
        order_by: "desc"
    }



    // ----------
    // AXIOS CALL
    // ----------
    
    async getExpeditions(page, pageSize) {
        // change status to loading
        if (this.state.status !== Utils.starting) {
            this.setState({status: Utils.loading})
        }

        // build parameter string
        var parameters = [
            ["limit",                 pageSize                                   ],
            ["offset",                pageSize*(page - 1)                        ],
            ["expedition_date_start", this.state.filters.start_date.toISOString()],
            ["expedition_date_end",   this.state.filters.end_date.toISOString()  ],
            ["sort_by",               this.state.sort_by                         ],
            ["order_by",              this.state.order_by                        ]];
        this.state.keywords        .map(k => {parameters.push(["keyword", k]);    });
        this.state.filters.rockets .map(r => {parameters.push(["rocket_name", r]);});
        this.state.filters.agencies.map(a => {parameters.push(["agency_id", a]);  });
        this.state.filters.pads    .map(p => {parameters.push(["pad_name", p]);   });
        var joined_parameters = parameters.map(p => (p[0] + "=" + p[1])).join("&");

        // send axios call
        try {
            await axios.get(
                `${process.env.REACT_APP_API_URL}/expeditions?${joined_parameters}`,
            ).then(response => {
                var data = response.data;
                // update stories, count, and status
                if ('error' in data)
                    this.setState({status: Utils.error})
                else if (data.count === 0)
                    this.setState({status: Utils.no_results})
                else
                    this.setState({
                        status: Utils.success,
                        count: data.count,
                        expeditions: data.expeditions,
                    })
            }).catch(error => {
                this.setState({status: Utils.error})
            });
        } catch (error) {
            this.setState({status: Utils.error})
        }
    }

    componentDidMount() {
        window.scrollTo(0,0);
        this.getExpeditions(this.state.current_page, this.state.page_size);
    }
    
    componentDidUpdate(prevProps, prevState) {
        if (prevState.current_page       === this.state.current_page       &&
            prevState.page_size          === this.state.page_size          &&
            prevState.filters.start_date === this.state.filters.start_date &&
            prevState.filters.end_date   === this.state.filters.end_date   &&
            prevState.sort_by            === this.state.sort_by            &&
            prevState.order_by           === this.state.order_by           &&
            Utils.arraysEqual(prevState.filters.pads,     this.state.filters.pads    ) &&
            Utils.arraysEqual(prevState.filters.agencies, this.state.filters.agencies) &&
            Utils.arraysEqual(prevState.filters.rockets,  this.state.filters.rockets ) &&
            Utils.arraysEqual(prevState.keywords,         this.state.keywords        ))
            return;

        window.scrollTo(0,0);
        this.getExpeditions(this.state.current_page, this.state.page_size);
    }



    // ------------
    // RENDER CARDS
    // ------------

    renderCard(expedition, timeout_val) {
        var keywords = this.state.keywords;
        return (
            <a href={String("/expeditions/").concat(expedition.id)} className="no-underline">
                <Grow in timeout={timeout_val}>
                    <Card className="m-0 p-0 shadow rounded-0 darken">
                        <Card.Img className="rounded-0" variant="top" style={{objectFit: 'cover', width: '100%', height: '160px'}} src={expedition.image}/>
                        <Card.Body style={{height: '80px'}}>
                            <Card.Subtitle className="mb-2 text-muted text-limit-one">
                                Agency: {Utils.highlightText(expedition.agency_name, keywords)}
                            </Card.Subtitle>
                            <Card.Title className="text-dark text-limit-one">
                                {Utils.highlightText(expedition.name, keywords)}
                            </Card.Title>
                        </Card.Body>
                        <ListGroup className="text-muted" variant="flush">
                            <ListGroup.Item className="darken-child" variant="light">
                                <text className="text-limit-one">
                                    Rocket: {Utils.highlightText(expedition.rocket_name, keywords)}
                                </text>
                            </ListGroup.Item>
                            <ListGroup.Item className="darken-child" variant="light">
                                <text className="text-limit-one">
                                    Launch Pad: {Utils.highlightText(expedition.pad_name, keywords)}
                                </text>
                            </ListGroup.Item>
                        </ListGroup>
                        <Card.Footer>
                            <small className="text-muted">
                                Launched on {Utils.highlightText(Utils.dateToString(expedition.date), keywords)}
                            </small>
                        </Card.Footer>
                    </Card>
                </Grow>
            </a>
        );
    }

    renderGrid() {
        return GridUtils.renderGrid(
            this.state.expeditions,
            this.renderCard.bind(this)
        );
    }



    // ------
    // FILTER
    // ------

    handleFilter = (searchProps) => {
        this.setState({
            current_page: 1,
            filters:      searchProps.filters,
            keywords:     searchProps.keywords,
            sort_by:      searchProps.sort_by,
            order_by:     searchProps.order_by
        })
    }



    // ------------
    // RENDER MODES
    // ------------

    renderLoading() {
        return (
            <div>
                {this.renderFilter()}
                {Utils.renderSpinner()}
                {this.renderPagination()}
            </div>
        );
    }

    renderSuccess() {
        return (
            <div>
                {this.renderFilter()}
                {this.renderGrid()}
                {this.renderPagination(true)}
            </div>
        );
    }

    renderNoResults() {
        return(
            <div>
                {this.renderFilter()}
                {Utils.renderMessage("Uh oh! No results were found")}
                {this.renderPagination()}
            </div>
        );
    }

    renderError() {
        return(
            <div>
                {this.renderFilter()}
                {Utils.renderMessage("Error retrieving data")}
                {this.renderPagination()}
            </div>
        );
    }

    renderContent() {
        // render based on status
        switch(this.state.status) {
            case Utils.starting   : return Utils.renderSpinner();
            case Utils.loading    : return this.renderLoading();
            case Utils.success    : return this.renderSuccess();
            case Utils.no_results : return this.renderNoResults();
            default               : return this.renderError();
        }
    }



    // ------
    // RENDER
    // ------

    renderFilter() {
        return (
            <div>
                <ExpeditionsFilter onChange={this.handleFilter}/>
                <Divider className="mb-3"/>
            </div>
        );
    }

    renderPagination(visible=false) {
        return (
            <div className="pagination mb-3 mt-3" style={{display: (visible ? "" : "none")}}>
                <Form.Label className="my-auto mr-2">
                    Expeditions per page:
                </Form.Label>

                <Form.Control className="my-auto" as="select" style={{width: 100}} onChange={(event) => {this.setState({current_page: 1, page_size: parseInt(event.target.value)})}} custom>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </Form.Control>

                <Form.Label className="my-auto mx-4">
                    {((this.state.current_page - 1) * this.state.page_size) + 1}-
                    {Math.min(this.state.count, this.state.current_page * this.state.page_size)} of {this.state.count}
                </Form.Label>

                <PaginationComponent
                    total={this.state.count}
                    itemsPerPage={this.state.page_size}
                    currentPage={this.state.current_page}
                    onPageChange={page => {window.scrollTo(0,0); this.setState({current_page: page})}}
                />
            </div>
        );
    }

    render() {
        return (
            <Container className="mt-3" style={{paddingLeft: "5%", paddingRight: "5%"}} fluid>
                <h1>Expeditions</h1>
                {this.renderContent()}
            </Container>
        );
    }
}

export default ExpeditionSearch;
