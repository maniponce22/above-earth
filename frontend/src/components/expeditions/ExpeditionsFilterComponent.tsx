import React, { Component } from 'react';
import axios from 'axios';

// CSS imports
import 'bootstrap/dist/css/bootstrap.min.css'

// import components and util libs
import Utils       from '../utils/Utils'
import FilterUtils from '../utils/FilterUtils'
import ChipDialog  from '../utils/ChipDialogComponent'
import DateDialog  from '../utils/DateDialogComponent'

// material-ui imports
import FormControl from '@material-ui/core/FormControl';

// react-bootstrap imports
import {Form, Accordion, Row, Col, InputGroup} from 'react-bootstrap';

// define the state
type ExpeditionsFilterState = {
    date_key:      number;
    custom_pad:    boolean;
    custom_rocket: boolean;
    custom_agency: boolean;

    filter_data: {
        pads:     string[];
        agencies: string[];
        rockets:  string[];
    };

    filters: {
        start_date: Date;
        end_date:   Date;
        rockets:    string[];
        agencies:   number[];
        pads:       string[];
    };

    keywords: string[];
    sort_by:  string;
    order_by: string;
};

// CONSTANTS
const default_start_date = new Date("1900-01-01");
const default_end_date   = new Date("2100-01-01");

const filter_list_limit = 5;

const sort_options = [
    { name: "Name",        value: "name"        },
    { name: "Hits",        value: "hits"        },
    { name: "Launch Date", value: "date"        },
    { name: "Rocket",      value: "rocket_name" },
    { name: "Launch Pad",  value: "pad_name"    },
    { name: "Agency Name", value: "agency_name" }
];

class ExpeditionsFilter extends Component<any, ExpeditionsFilterState> {
    state: ExpeditionsFilterState = {
        date_key:      0,
        custom_pad:    false,
        custom_rocket: false,
        custom_agency: false,

        filter_data: {
            pads:     [],
            agencies: [],
            rockets:  []
        },

        filters: {
            start_date: default_start_date,
            end_date:   default_end_date,
            rockets:    [],
            agencies:   [],
            pads:       []
        },

        keywords: [],
        sort_by:  "date",
        order_by: "desc"
    };

    // map for getting agency ids
    agency_map = {};

    // refs
    searchBarRef;
    dateDialogRef;
    padDialogRef;
    rocketDialogRef;
    agencyDialogRef;

    constructor(props: any) {
        super(props);

        this.searchBarRef    = React.createRef();
        this.dateDialogRef   = React.createRef();
        this.padDialogRef    = React.createRef();
        this.rocketDialogRef = React.createRef();
        this.agencyDialogRef = React.createRef();
    }



    // -----------
    // AXIOS CALLS
    // -----------

    async getPadRocketData() {
        try {
            // send axios call
            await axios.get(
                `${process.env.REACT_APP_API_URL}/overview/expeditions`,
            ).then(response => {
                var data = response.data;

                // count number of pads and rockets
                var pads = {}
                var rockets = {}
                for (var e of data.expeditions) {
                    if (!(e.pad_name in pads))
                        pads[e.pad_name] = 0;
                    pads[e.pad_name] += 1;

                    if (!(e.rocket_name in rockets))
                        rockets[e.rocket_name] = 0;
                    rockets[e.rocket_name] += 1;
                }
                
                // Create and sort pad array
                var pads_arr = Object.keys(pads).map(key => ({pad: key, count: pads[key]}));
                pads_arr.sort((a, b) => (b.count - a.count));

                // create and sort rocket array
                var rockets_arr = Object.keys(rockets).map(key => ({rocket: key, count: pads[key]}));
                rockets_arr.sort((a, b) => (b.count - a.count));

                // update state
                this.setState({filter_data: {
                    ...this.state.filter_data,
                    pads:    pads_arr.map(p => (p.pad)),
                    rockets: rockets_arr.map(r => (r.rocket))
                }});
            }).catch(error => {});
        } catch (error) {}
    }

    async getAgencyData() {
        try {
            // send axios call
            await axios.get(
                `${process.env.REACT_APP_API_URL}/overview/agencies`,
            ).then(response => {
                var data = response.data;

                // Create and sort array
                var agency_arr = data.agencies
                    .filter(a => (a.total_story_count > 0))
                    .map(a => ({name: a.name, id: a.id, count: a.total_launch_count}));
                agency_arr.sort((a, b) => (b.count - a.count));

                // create agency_map
                agency_arr.map(a => {this.agency_map[a.name] = a.id;})

                // update state
                this.setState({filter_data: {
                    ...this.state.filter_data,
                    agencies: agency_arr.map(a => (a.name))
                }})
            }).catch(error => {});
        } catch (error) {}
    }

    componentDidMount() {
        this.getPadRocketData();
        this.getAgencyData();
    }

    componentDidUpdate(prevProps, prevState) {
        // send an onChange update if the filters have not changed
        if (prevState.order_by           === this.state.order_by           &&
            prevState.sort_by            === this.state.sort_by            &&
            prevState.filters.start_date === this.state.filters.start_date &&
            prevState.filters.end_date   === this.state.filters.end_date   &&
            Utils.arraysEqual(prevState.keywords,         this.state.keywords        ) &&
            Utils.arraysEqual(prevState.filters.pads,     this.state.filters.pads    ) &&
            Utils.arraysEqual(prevState.filters.agencies, this.state.filters.agencies) &&
            Utils.arraysEqual(prevState.filters.rockets,  this.state.filters.rockets ))
            return;

        this.props.onChange(this.state);
    }



    // ------
    // SEARCH
    // ------

    handleSearchGo = () => {
        // prevent tsx warnings
        if (this.searchBarRef.current === null)
            return;

        // split keywords and update state
        var kws = this.searchBarRef.current.value.toString().split(" ").filter(kw => (kw !== "" && kw !== undefined))
        this.setState({keywords: kws})
    }



    // ------------
    // Launch Date
    // ------------

    handleDateClick = (key: number) => {
        if (key === this.state.date_key) {
            // if getting rid of the filter...
            this.setState({
                date_key: 0,
                filters: {
                    ...this.state.filters,
                    start_date: default_start_date,
                    end_date: default_end_date
                }
            });
        } else {
            // if adding a filter...
            if (key === 3)
                // Click CUSTOM, open dialog
                this.dateDialogRef.current.open();
            else
                // Click MONTH or YEAR
                this.setState({
                    date_key: key,
                    filters: {
                        ...this.state.filters,
                        start_date: (key === 1) ? FilterUtils.month_date : FilterUtils.year_date,
                        end_date: default_end_date
                    }
                })
        }
    };



    // ---
    // Pad
    // ---

    handleSetPad = (pad) => {
        // create new pad arr
        var new_pads = [pad];
        if (Utils.arraysEqual(this.state.filters.pads, [pad]) && !this.state.custom_pad)
            new_pads = [];

        // update state
        this.setState({
            filters: {...this.state.filters, pads: new_pads}, 
            custom_pad: false
        });
    }

    handlePadMoreClick = () => {
        if (this.state.custom_pad)
            // removing filter, deselect all
            this.setState({
                filters: { ...this.state.filters, pads: [] },
                custom_pad: false
            })
        else
            // open the dialog
            this.padDialogRef.current.open();
    }


    
    // ------
    // Rocket
    // ------

    handleSetRocket = (name) => {
        // create new rocket arr
        var new_rockets = [name]
        if (Utils.arraysEqual(this.state.filters.rockets, [name]) && !this.state.custom_rocket)
            new_rockets = [];

        // update state
        this.setState({
            filters: { ...this.state.filters, rockets: new_rockets }, 
            custom_rocket: false
        });
    }

    handleRocketMoreClick = () => {
        if (this.state.custom_rocket)
            // removing filter, deselect all
            this.setState({
                filters: { ...this.state.filters, rockets: [] },
                custom_rocket: false
            })
        else
            // open the dialog
            this.rocketDialogRef.current.open();
    }



    // ------
    // Agency
    // ------

    handleSetAgency = (name) => {
        // create new agency arr
        var new_agencies = [this.agency_map[name]]
        if (Utils.arraysEqual(this.state.filters.agencies, new_agencies) && !this.state.custom_agency)
            new_agencies = [];

        // update state
        this.setState({
            filters: { ...this.state.filters, agencies: new_agencies }, 
            custom_agency: false
        });
    }

    handleAgencyMoreClick = () => {
        if (this.state.custom_agency)
            // removing filter, deselect all
            this.setState({
                filters: { ...this.state.filters, agencies: [] },
                custom_agency: false
            })
        else
            // open the dialog
            this.agencyDialogRef.current.open();
    }



    // ------
    // Render
    // ------

    render() {
        return (
            <Accordion key="0" className="mb-4">
                <Form className="mx-auto mb-0">
                    <Form.Row>
                        <Col className="pr-0" style={{marginRight: "8px"}}>
                            <InputGroup>
                                {FilterUtils.filterButton(0)}
                                {FilterUtils.searchBar(
                                    "Search Expeditions",
                                    this.handleSearchGo.bind(this),
                                    "calc(100% - 136px)",
                                    this.searchBarRef
                                )}
                            </InputGroup>
                        </Col>
                        <Col className="pl-0" style={{marginLeft: "8px"}}>
                            <InputGroup>
                                <FormControl variant="outlined" style={{marginRight: "10px", width: "calc(100% - 65px)"}}>
                                    {FilterUtils.sortSelector(
                                        sort_options,
                                        "date",
                                        (e) => {this.setState({sort_by: e.target.value})})}
                                </FormControl>
                                {FilterUtils.orderDirectionButton(
                                    this.state.order_by === "desc",
                                    () => {this.setState({order_by: this.state.order_by === "asc" ? "desc" : "asc"})})}
                            </InputGroup>
                        </Col>
                    </Form.Row>
                </Form>
                <Accordion.Collapse eventKey="0">
                    <Row className="mx-auto mt-3">
                        <Col sm="2">
                            <Row>{FilterUtils.filterColumnHeader("Launch Date")}</Row>
                            <Row>{FilterUtils.filterColumnUnderline()}</Row>
                            <Row>
                                {FilterUtils.filterEntry(
                                    "This month",
                                    this.state.date_key === 1,
                                    this.handleDateClick.bind(this), 1)}
                            </Row>
                            <Row>
                                {FilterUtils.filterEntry(
                                    "This year",
                                    this.state.date_key === 2,
                                    this.handleDateClick.bind(this), 2)}
                            </Row>
                            <Row>
                                {FilterUtils.filterEntry(
                                    "Custom",
                                    this.state.date_key === 3,
                                    this.handleDateClick.bind(this), 3)}
                                <DateDialog 
                                    ref={this.dateDialogRef}
                                    title = "Custom Launch Date Range"
                                    onChange={dates => {
                                        this.setState({
                                            date_key: 3,
                                            filters: {
                                                ...this.state.filters,
                                                start_date: dates[0],
                                                end_date: dates[1]
                                            }
                                        })
                                    }}/>
                            </Row>
                        </Col>
                        <Col sm="3">
                            <Row>{FilterUtils.filterColumnHeader("Launch Pad")}</Row>
                            <Row>{FilterUtils.filterColumnUnderline()}</Row>
                            {this.state.filter_data.pads.slice(0, filter_list_limit).map(pad => 
                                <Row>
                                    {FilterUtils.filterEntry(
                                        pad, 
                                        (Utils.arraysEqual(this.state.filters.pads, [pad]) 
                                            && !this.state.custom_pad),
                                        this.handleSetPad.bind(this), pad)}
                                </Row>)}
                            <Row>
                                {FilterUtils.filterEntry(
                                    "More", 
                                    this.state.custom_pad, 
                                    this.handlePadMoreClick.bind(this))}
                                <ChipDialog 
                                    ref={this.padDialogRef}
                                    title="Specify Launch Pads"
                                    entries={this.state.filter_data.pads} 
                                    onChange={(selection) => {
                                        this.setState({
                                            filters: {
                                                ...this.state.filters,
                                                pads: selection
                                            },
                                            custom_pad: true
                                        })
                                    }}/>
                            </Row>
                        </Col>
                        <Col sm="3">
                            <Row>{FilterUtils.filterColumnHeader("Rocket")}</Row>
                            <Row>{FilterUtils.filterColumnUnderline()}</Row>
                            {this.state.filter_data.rockets.slice(0, filter_list_limit).map(rocket => 
                                <Row>
                                    {FilterUtils.filterEntry(
                                        rocket, 
                                        (Utils.arraysEqual(this.state.filters.rockets, [rocket]) 
                                            && !this.state.custom_rocket),
                                        this.handleSetRocket.bind(this), rocket)}
                                </Row>)}
                            <Row>
                                {FilterUtils.filterEntry(
                                    "More",
                                    this.state.custom_rocket,
                                    this.handleRocketMoreClick.bind(this))}
                                <ChipDialog 
                                    ref={this.rocketDialogRef}
                                    title="Select Rockets"
                                    entries={this.state.filter_data.rockets} 
                                    onChange={(selection) => {
                                        this.setState({
                                            filters: { ...this.state.filters, rockets: selection },
                                            custom_rocket: true
                                        })
                                    }}/>
                            </Row>
                        </Col>
                        <Col sm="4">
                            <Row>{FilterUtils.filterColumnHeader("Agency")}</Row>
                            <Row>{FilterUtils.filterColumnUnderline()}</Row>
                            {this.state.filter_data.agencies.slice(0, filter_list_limit).map(name => 
                                <Row>
                                    {FilterUtils.filterEntry(
                                        name, 
                                        (Utils.arraysEqual(this.state.filters.agencies, [this.agency_map[name]]) 
                                            && !this.state.custom_agency),
                                        this.handleSetAgency.bind(this), name)}
                                </Row>)}
                            <Row>
                                {FilterUtils.filterEntry(
                                    "More",
                                    this.state.custom_agency,
                                    this.handleAgencyMoreClick.bind(this))}
                                <ChipDialog 
                                    ref={this.agencyDialogRef}
                                    title="Select Agencies"
                                    entries={this.state.filter_data.agencies} 
                                    onChange={(selection) => {
                                        this.setState({
                                            filters: {
                                                ...this.state.filters,
                                                agencies: selection.map(name => (this.agency_map[name]))
                                            },
                                            custom_agency: true
                                        })
                                    }}/>
                            </Row>
                        </Col>
                    </Row>
                </Accordion.Collapse>
            </Accordion>
        );
    }
}

export default ExpeditionsFilter;
