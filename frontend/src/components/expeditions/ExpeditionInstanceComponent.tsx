import React, { Component }         from 'react';
import {RouteComponentProps, Link } from 'react-router-dom'
import axios                        from 'axios';

// CSS imports
import 'bootstrap/dist/css/bootstrap.min.css'

// import components and interfaces
import PadsMap                   from '../agencies/PadsMapComponent';
import {ExpeditionInstanceState} from './ExpeditionInterface';
import Utils                     from '../utils/Utils'

// react-bootstrap imports
import {Button, Image, Row, Col, Container, Table} from 'react-bootstrap'


class ExpeditionInstance extends Component<RouteComponentProps<{id}>, ExpeditionInstanceState> {
    state: ExpeditionInstanceState = {
        status: Utils.starting,

        expedition: {
            agency_abbrev:          "",
            agency_administrator:   "",
            agency_country:         "",
            agency_description:     "",
            agency_founding_year:   "",
            agency_id:              0,
            agency_logo_url:        "",
            agency_name:            "",
            agency_type:            "",
            date:                   new Date(),
            id:                     "",
            image:                  "",
            most_recent_news_story: new Date(),
            name:                   "",
            pad_image:              "",
            pad_latitude:           "",
            pad_longitude:          "",
            pad_name:               "",
            rocket_name:            "",
            total_story_count:      0,
        },

        linked_stories:      [],
        related_expeditions: []
    }    

    // ----------
    // AXIOS CALL
    // ----------

    async getExpedition() {
        // get id from url
        var id = this.props.match.params['expeditionId'];

        // send axios call
        try {
             await axios.get(
                `${process.env.REACT_APP_API_URL}/expeditions/` + id
            ).then(response => {
                // update state
                var data = response.data;
                if ('error' in data)
                    this.setState({status: Utils.error});
                else
                    this.setState({
                        status:              Utils.success,
                        expedition:          data.expedition,
                        linked_stories:      data.linked_stories,
                        related_expeditions: data.related_expeditions
                    });
            }).catch(error => {
                this.setState({status: Utils.error});
            });
        } catch (error) {
            this.setState({status: Utils.error});
        }
    }

    componentDidMount() {
        window.scrollTo(0,0);
        this.getExpedition();
    }



    // ------
    // RENDER
    // ------

    createDescriptionContent() {
        return (
            <p>
                <b>Agency:</b> {this.state.expedition.agency_name}<br></br>
                <b>Agency Country:</b> {this.state.expedition.agency_country}<br></br>
                <b>Agency Type:</b> {this.state.expedition.agency_type}<br></br>
                <b>Exact Time:</b> {this.state.expedition.date}<br></br>
                <b>Pad Name:</b> {this.state.expedition.pad_name}<br></br>
                <b>Pad Latitude:</b> {this.state.expedition.pad_latitude}<br></br>
                <b>Pad Longitude:</b> {this.state.expedition.pad_longitude}<br></br>
                <b>Rocket Name:</b> {this.state.expedition.rocket_name}<br></br>
                <b>Total Story Count:</b> {this.state.expedition.total_story_count}<br></br>
                <b>Most Recent News Story:</b> {Utils.dateToString(this.state.expedition.most_recent_news_story)}<br></br>
            </p>
        )
    }

    renderSuccess() {
        return (
            <div>
                <Row>
                    <h1>{this.state.expedition.name}</h1>
                </Row>
                <Row>
                    <div>
                        <a href={String("/agencies/").concat(this.state.expedition.agency_id.toString())} style={{height:100}}>
                            <Image src={this.state.expedition.agency_logo_url} style={{height:100}}/>
                        </a>
                        <p><Button variant="link" href={String("/agencies/").concat(this.state.expedition.agency_id.toString())}>{this.state.expedition.agency_abbrev}</Button>
                        <span className="text-muted">{Utils.dateToString(this.state.expedition.date)}</span></p> 
                    </div>
                    
                </Row>
                <Row className="mt 1 horizontalLine">
                    <Col sm="9" className="mt-0 mt-4 pr-5 verticalLine">
                        <Row>
                            <Image src={this.state.expedition.image} fluid/>
                        </Row>
                        <hr />
                        <h3>Launch Pad:</h3>
                        <div className="mb-4">
                            <PadsMap pads={[{
                                name: this.state.expedition.pad_name,
                                latitude: parseInt(this.state.expedition.pad_latitude),
                                longitude: parseInt(this.state.expedition.pad_longitude)
                            }]} />
                        </div>
                        
                    </Col>
                    <Col sm="3" className="pl-5">
                        <Row className="mt-4 mb-4">
                            {this.createDescriptionContent()}
                        </Row>
                        <Row style={{height: 50}}/>
                        <Row>
                            <Col>
                            <Table bordered hover>
                                <thead>
                                    <tr>
                                        <th>Related Stories</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.linked_stories.map((story) => {
                                        return (
                                            <tr>
                                                <td>
                                                    <Link to={{ pathname: `/news/${story.id}`}}>
                                                        {story.title}
                                                    </Link>
                                                </td>
                                            </tr>
                                        )
                                    })}
                                </tbody>

                            </Table>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </div>
        );
    }

    renderContent() {
        switch(this.state.status) {
            case Utils.starting : return Utils.renderSpinner();
            case Utils.success  : return this.renderSuccess();
            case Utils.error    : return Utils.renderMessage("404 Not Found");
        }
    }

    render() {
        return (
            <Container className="mt-5" style={{paddingLeft: "15%", paddingRight: "15%"}} fluid>
                {this.renderContent()}
            </Container>
        );
    }
}

export default ExpeditionInstance;
