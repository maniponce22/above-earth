
export interface Expedition {
    agency_abbrev:          string;
    agency_administrator:   string;
    agency_country:         string;
    agency_description:     string;
    agency_founding_year:   string;
    agency_id:              number;
    agency_logo_url:        string;
    agency_name:            string;
    agency_type:            string;
    date:                   Date;
    id:                     string;
    image:                  string;
    most_recent_news_story: Date;
    name:                   string;
    pad_image:              string;
    pad_latitude:           string;
    pad_longitude:          string;
    pad_name:               string;
    rocket_name:            string;
    total_story_count:      number;
}

export interface Filters {
    start_date: Date;
    end_date:   Date;
    rockets:    string[];
    agencies:   string[];
    pads:       string[];
}

export interface ExpeditionSearchState {
    status:       number;
    count:        number;
    expeditions:  Expedition[];
    page_size:    number;
    current_page: number;
    filters:      Filters;
    keywords:     String[];
    sort_by:      string;
    order_by:     string;
}


export interface ExpeditionInstanceState {
    status:              number;
    expedition:          Expedition;
    linked_stories:      LinkedStory[];
    related_expeditions: RelatedExpedition[];
}

export interface LinkedStory {
    id:                       string;
    image_url:                string;
    publish_date:             Date;
    related_agency_count:     number;
    related_expedition_count: number;
    site:                     string;
    site_logo_url:            string;
    site_url:                 string;
    summary:                  string;
    title:                    string;
    url:                      string;
}

export interface RelatedExpedition {
    agency_abbrev:          string;
    agency_country:         string;
    agency_id:              number;
    agency_name:            string;
    date:                   Date;
    id:                     string;
    image:                  string;
    most_recent_news_story: Date;
    name:                   string;
    pad_name:               string;
    rocket_name:            string;
    total_story_count:      number;
}
