import React, { Component }       from 'react';
import { Switch, Route, Redirect} from 'react-router-dom';
import { createBrowserHistory }   from "history";

// import header and footer
import Header from './HeaderComponent';
import Footer from './FooterComponent';

//import pages
import SplashScreen           from '../components/splash/SplashScreen';
import About                  from './about/AboutComponent';
import StoriesSearch          from './stories/StoriesSearchComponent';
import StoryInstance          from './stories/StoryInstanceComponent';
import ExpeditionSearch       from './expeditions/ExpeditionSearchComponent';
import ExpeditionInstance     from './expeditions/ExpeditionInstanceComponent';
import AgenciesSearch         from './agencies/AgenciesSearchComponent';
import AgencyInstance         from './agencies/AgencyInstanceComponent';
import SearchComponent        from './Searching';
import OurVisualizations      from './graphs/ourVis/OurVisComponent'
import ProviderVisualizations from './graphs/providerVis/ProviderVisComponent'

export const history = createBrowserHistory()
history.listen((location, action) => {
    window.scrollTo(0, 0)
})

const Main = () => (
    <div>
        <Header />
        <Switch>
            <Route exact path="/" component={SplashScreen} />
            <Route path="/about" component={About} />
            <Route exact path="/agencies" component={AgenciesSearch} />
            <Route path="/agencies/:agencyId" component={AgencyInstance} />
            <Route exact path="/expeditions" component={ExpeditionSearch} />
            <Route path="/expeditions/:expeditionId" component={ExpeditionInstance} />
            <Route exact path="/news" component={StoriesSearch} />
            <Route path="/news/:newsId" component={StoryInstance} />
            <Route path="/search/:query" component={SearchComponent} />
            <Route exact path="/our_visualizations" component={OurVisualizations} />
            <Route exact path="/provider_visualizations" component={ProviderVisualizations} />
            <Redirect to="/" />
        </Switch>
        <Footer />
    </div>
);

export default Main;
